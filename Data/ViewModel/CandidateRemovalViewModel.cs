﻿using System;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class CandidateRemovalViewModel
    {
        public string CandidateFirstName { get; set; }
        public string CandidateLastName { get; set; }
        public string CandidateEmail { get; set; }

        public string AdminId { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public string AdminEmail { get; set; }
        public string Reason { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
