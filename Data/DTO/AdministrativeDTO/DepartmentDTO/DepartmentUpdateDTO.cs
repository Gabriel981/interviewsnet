﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO.DepartmentDTO
{
    public class DepartmentUpdateDTO
    {
        [Required(ErrorMessage = "Department name is required")]
        [StringLength(30, ErrorMessage = "Department name length must be between 2 and 30", MinimumLength = 2)]
        [Display(Name = "Department name")]
        public string DepartmentName { get; set; }

        [Required(ErrorMessage = "Department description is required")]
        [StringLength(350, ErrorMessage = "Description length must be between 8 and 350 characters", MinimumLength = 8)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Department current status is required")]
        [Display(Name = "Department availability")]
        public bool Active { get; set; }
    }
}
