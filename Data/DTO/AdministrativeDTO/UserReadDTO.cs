﻿using InterviewsManagementNET.Models;
using System;

namespace InterviewsManagementNET.Data
{
    public class UserReadDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public Position Position { get; set; }

        public string Role { get; set; }

        public string Mentions { get; set; }
    }
}
