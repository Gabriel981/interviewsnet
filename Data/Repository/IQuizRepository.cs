﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public interface IQuizRepository
    {
        Task<IEnumerable<Quiz>> GetAllQuizesAsync();
        Task<Quiz> GetQuizByIdAsync(int id);
        Task<Quiz> GetQuizVariousParamsAsync(string createdById, string createdAt, int positionId);
        Task AddQuizAsync(Quiz quiz);
        void RemoveQuiz(Quiz quiz);
        void UpdateQuiz(Quiz quiz);
        Task<bool> SaveChangesAsync();
    }
}
