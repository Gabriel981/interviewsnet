﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InterviewsManagementNET.Models
{
    public class Quiz
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuizId { get; set; }
        public string Mentions { get; set; }
        public int ScoreResult { get; set; }

        public int PositionId { get; set; }
        public Position Position { get; set; }
        /*        public IEnumerable<Quiz> Questions { get; set; }*/

        public string CreatedAt { get; set; }

        public string CreatedById { get; set; }
        public User CreatedBy { get; set; }

        public ICollection<Interview> Interviews { get; set; }

        public ICollection<QuizQuestion> QuizQuestions { get; set; }
    }
}
