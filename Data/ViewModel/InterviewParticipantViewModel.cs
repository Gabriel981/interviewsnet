﻿using InterviewsManagementNET.Models;
using System;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class InterviewParticipantViewModel
    {
        public string ParticipantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int InterviewId { get; set; }
        public int PositionId { get; set; }
        public Position Position { get; set; }
        public DateTime AttendanceDate { get; set; }
    }
}
