﻿namespace InterviewsManagementNET.Models
{
    public class Notification
    {
        public string SenderId { get; set; }
        public string ReceviverId { get; set; }
        public string CandidateId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Timestamp { get; set; }
        public string PositionName { get; set; }
    }
}
