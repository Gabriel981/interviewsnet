﻿using System.Collections.Generic;

namespace InterviewsManagementNET.Models
{
    public class State
    {
        public int Id { get; set; }
        public ICollection<Sequence> Sequences { get; set; }
        public bool EmailAddress { get; set; }
        public bool Password { get; set; }
        public bool FirstName { get; set; }
        public bool LastName { get; set; }
        public bool Address { get; set; }
        public bool PhoneNumber { get; set; }
        public bool SecurityQuestion { get; set; }
        public bool SecurityQuestionAnswer { get; set; }
        public bool File { get; set; }
        public bool Position { get; set; }
    }
}
