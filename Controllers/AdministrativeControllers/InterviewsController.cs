﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers
{
    [ApiController]
    [Route("api/interviews")]
    public class InterviewsController : ControllerBase
    {
        private readonly IInterviewRepo _repository;
        private readonly IHiringRepo _hiringRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISubmissionRepo _submissionRepository;
        private readonly SubmissionContext _submissionContext;
        private readonly IPositionRepo _positionRepository;
        private readonly INotificationService _notificationService;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMailServices _mailServices;
        private IMapper _mapper;

        public const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public InterviewsController(
            IInterviewRepo repository,
            IHiringRepo hiringRepository,
            IUserRepository userRepository,
            ISubmissionRepo submissionRepository,
            SubmissionContext submissionContext,
            IPositionRepo positionRepository,
            INotificationService notificationService,
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            IMailServices mailServices,
            IMapper mapper)
        {
            _repository = repository;
            _hiringRepository = hiringRepository;
            _userRepository = userRepository;
            _submissionRepository = submissionRepository;
            _submissionContext = submissionContext;
            _positionRepository = positionRepository;
            _notificationService = notificationService;
            _userManager = userManager;
            _roleManager = roleManager;
            _mailServices = mailServices;
            _mapper = mapper;
        }

        [Authorize(Roles = "Recruiter, Administrator")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> GetAllInterviewsAsync()
        {
            var interviews = await _repository.GetAllInterviewsAsync();
            return Ok(_mapper.Map<IEnumerable<InterviewViewModel>>(interviews));
        }

        [HttpGet("{id}", Name = "GetInterviewById")]
        [Authorize(Roles = "Recruiter")]
        public async Task<ActionResult<InterviewReadDTO>> GetInterviewById(int id)
        {
            var InterviewFound = await _repository.GetInterviewByIdAsync(id);

            if (InterviewFound != null)
            {
                return Ok(_mapper.Map<InterviewReadDTO>(InterviewFound));
            }

            return NotFound();
        }


        [HttpPost]
        [Authorize(Roles = "Recruiter")]
        [Route("schedule")]
        public async System.Threading.Tasks.Task<ActionResult<InterviewReadDTO>> ScheduleInterviewAsync(InterviewWriteDTO interview)
        {
            if (interview != null)
            {
                HiringProcess hiringProcessFound = await _hiringRepository.GetHiringProcessById(interview.HiringId);
                if (hiringProcessFound == null)
                {
                    return NotFound("No hiring process found with id " + interview.HiringId);
                }

                string candidateCurrentStatus = hiringProcessFound.CandidateSituation.ToString();
                if (candidateCurrentStatus != CandidateSituation.ReadyForInterview.ToString())
                {
                    return BadRequest("No interview can be scheduled when the candidate current status is different than " + candidateCurrentStatus);
                }

                DateTime scheduledDate;
                var interviewModelConversion = _mapper.Map<Interview>(interview);

                if (interviewModelConversion.InterviewDate == null || interviewModelConversion.InterviewDate < DateTime.UtcNow)
                {
                    scheduledDate = DateTime.Now.Date.AddDays(7);
                }
                else
                {
                    scheduledDate = interview.InterviewDate;
                }

                var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                if (submissionFound == null)
                {
                    return NotFound("No submission found with id " + hiringProcessFound.SubmissionId);
                }

                User candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                if (candidateFound == null)
                {
                    return NotFound("No candidate found with id " + submissionFound.CandidateId);
                }

                var interviewsDB = await _repository.GetAllInterviewsAsync();
                foreach (var interviewDb in interviewsDB)
                {
                    if (interviewDb.HiringProcess.HiringId == hiringProcessFound.HiringId
                        && interviewDb.InterviewDate == interview.InterviewDate)
                    {
                        return BadRequest("No interview can be scheduled in the same day when the same candidate has already one interview");
                    }
                }

                var userCreatorFound = await _userRepository.GetUserById(interview.CreatedById);
                if (userCreatorFound == null)
                {
                    return NotFound("No user found with id " + interview.CreatedById);
                }

                interviewModelConversion.CreatedById = userCreatorFound.Id;

                /*interviewModelConversion.QuizId = 1;*/
                interviewModelConversion.QuizId = null;
                await _repository.AddInterviewAsync(interviewModelConversion);
                await _repository.SaveChangesAsync();

                Interview interviewFound = await _repository.GetInterviewByHiringProcessAsync(hiringProcessFound);

                hiringProcessFound.InterviewId = interviewFound.InterviewId;
                _hiringRepository.UpdateHiringProcess(hiringProcessFound);
                await _hiringRepository.SaveChangesAsync();

                var interviewReadDTO = _mapper.Map<InterviewReadDTO>(interviewModelConversion);

                return CreatedAtRoute(nameof(GetInterviewById), new { Id = interviewReadDTO.InterviewId }, interviewReadDTO);
            }

            return BadRequest();
        }

        [HttpPut]
        [Route("update/{id}")]
        [Authorize(Roles = "Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult> UpdateInterviewAsync(int id, [FromBody] InterviewUpdateDTO interviewUpdateDTO)
        {
            var interviewFromRepository = await _repository.GetInterviewByIdAsync(id);
            if (interviewFromRepository == null)
            {
                return NotFound();
            }

            interviewFromRepository.InterviewDate = interviewUpdateDTO.InterviewDate.AddDays(1);
            if (interviewUpdateDTO.QuizId == null || interviewUpdateDTO.QuizId == 0)
            {
                interviewFromRepository.QuizId = null;
            }
            else
            {
                interviewFromRepository.QuizId = interviewUpdateDTO.QuizId;
            }

            if (interviewUpdateDTO.CommunicationChannel != null)
            {
                interviewFromRepository.CommunicationChannel = interviewUpdateDTO.CommunicationChannel;
            }

            _repository.UpdateInterview(interviewFromRepository);
            _repository.SaveChangesAsync();

            return Ok("The interview details was sucesfully updated");
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult> RemoveInterviewAsync(int id)
        {
            var interviewFromRepository = await _repository.GetInterviewByIdAsync(id);
            if (interviewFromRepository == null)
                return NotFound("No interview was found in the database with id " + id);

            var participants = await _repository.GetAllInterviewParticipantsAsync(interviewFromRepository.InterviewId);
            if (participants == null)
            {
                await _repository.removeInterviewAsync(interviewFromRepository);
                await _repository.SaveChangesAsync();

                return Ok("The interview was sucesfully deleted from database");
            }

            foreach (var participant in participants)
            {
                this._repository.removeParticipantFromInterview(participant);
                await _repository.SaveChangesAsync();
            }

            await _repository.removeInterviewAsync(interviewFromRepository);
            await _repository.SaveChangesAsync();

            return Ok("The interview was sucesfully deleted from database");
        }

        [HttpGet]
        [Route("hirings")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<HiringViewModel>>> GetRequiredCandidatesAsync()
        {
            var taskHiringProcess = await _hiringRepository.GetAllHiringProcesses();
            var hiringProcessesFound = taskHiringProcess.Where(h => h.CandidateSituation == CandidateSituation.ReadyForInterview.ToString());

            if (hiringProcessesFound == null)
            {
                return NotFound("No hiring process with status ReadyForInterview found in the database");
            }

            return Ok(_mapper.Map<IEnumerable<HiringViewModel>>(hiringProcessesFound));
        }

        [HttpGet]
        [Route("participants/{id}")]
        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<InterviewParticipantViewModel>>> GetAllParticipantsByInterviewAsync(int id)
        {
            var participantsFound = await _repository.GetAllInterviewParticipantsAsync(id);
            if (participantsFound == null)
            {
                return NotFound("No participants found for interview with id " + id);
            }

            var interviewParticipantsViewModels = participantsFound.Select(p => new InterviewParticipantViewModel()
            {
                FirstName = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).FirstName,
                LastName = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).LastName,
                Email = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).Email,
                PhoneNumber = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).PhoneNumber,
                ParticipantId = p.EmployeeId,
                InterviewId = p.InterviewId,
                AttendanceDate = p.AttendedTime
            });

            return Ok(interviewParticipantsViewModels);
        }

        [HttpPost]
        [Route("addParticipant/{id}")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult<InterviewParticipantViewModel>> AddParticipantInterviewAsync(string id, EmployeeParticipantViewModel employeeParticipantViewModel)
        {
            var employeeFound = await _userRepository.GetUserByEmailAddress(employeeParticipantViewModel.Email);
            if (employeeFound == null)
            {
                return NotFound("No employee found in the database with email address " + employeeParticipantViewModel.Email);
            }

            var interviewFound = await _repository.GetInterviewByIdAsync(employeeParticipantViewModel.AttendedInterviewId);
            if (interviewFound == null)
            {
                return NotFound("No interview found with id " + employeeParticipantViewModel.AttendedInterviewId);
            }

            var senderFound = await _userRepository.GetUserById(id);
            if (senderFound == null)
            {
                return NotFound("No recruiter found in the database with id " + id);
            }

            var hiringFound = await _hiringRepository.GetHiringProcessById(interviewFound.HiringId);
            if (hiringFound == null)
            {
                return NotFound("No hiring process found in the database with id " + interviewFound.HiringId);
            }

            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringFound.SubmissionId);
            if (submissionFound == null)
            {
                return NotFound("No submission found in the database with id " + hiringFound.SubmissionId);
            }

            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
            {
                return NotFound("No candidate found in the database with id " + submissionFound.CandidateId);
            }

            var currentTimeAttendance = DateTime.UtcNow.AddHours(2);

            InterviewParticipant newlyCreatedInterviewParticipant = new InterviewParticipant()
            {
                InterviewId = interviewFound.InterviewId,
                EmployeeId = employeeFound.Id,
                User = employeeFound,
                Interview = interviewFound,
                AttendedTime = currentTimeAttendance
            };

            await _repository.AddInterviewParticipantAsync(newlyCreatedInterviewParticipant);
            await _repository.SaveChangesAsync();

            await _notificationService.PostAttendanceInterview(id, employeeFound.Id, interviewFound.InterviewId);
            this._mailServices.SendParticipantAttendanceInterview(senderFound, employeeFound, interviewFound, candidateFound);

            //return Ok(newlyCreatedInterviewParticipant);
            return Ok("The selected participant was succesfully allocated to the interview");
        }

        [HttpGet]
        [Route("employees")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult<EmployeeParticipantViewModel>> GetAllAvailableEmployeesAsync()
        {
            var employeeRole = await _roleManager.FindByNameAsync("Employee");
            var employees = await _userManager.GetUsersInRoleAsync(employeeRole.Name);

            if (employees == null) return BadRequest();

            IList<EmployeeParticipantViewModel> employeeParticipantViews = employees.Select(e => new EmployeeParticipantViewModel()
            {
                FirstName = e.FirstName,
                LastName = e.LastName,
                Address = e.Address,
                DateOfBirth = e.DateOfBirth,
                Email = e.Email,
                PhoneNumber = " ",
                PositionId = e.PositionId,
            }).ToList();

            return Ok(employeeParticipantViews);
        }

        [HttpDelete]
        [Route("participants/remove/{id}/{id2}")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult> RemoveParticipantFromInterviewAsync(int id, string id2)
        {
            var interviewParticipantFound = await _repository.GetInterviewParticipantByIdsAsync(id, id2);
            if (interviewParticipantFound == null)
            {
                return NotFound("No participant found for the interview with id " + id);
            }

            _repository.removeParticipantFromInterview(interviewParticipantFound);
            await _repository.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        [Route("export")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<IActionResult> ExportInterviewsDataAsync()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var interviewsAvailable = await _repository.GetAllInterviewsAsync();
            if (interviewsAvailable == null) return NotFound("No interviews found in the database");

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Interviews");

                int index = 1;

                worksheet.Cells["B1"].Value = "Candidate complete name";
                worksheet.Cells["B1"].Style.Font.Bold = true;
                worksheet.Cells["B1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["C1"].Value = "Candidate email address";
                worksheet.Cells["C1"].Style.Font.Bold = true;
                worksheet.Cells["C1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["D1"].Value = "Candidate phone number";
                worksheet.Cells["D1"].Style.Font.Bold = true;
                worksheet.Cells["D1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["D1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["E1"].Value = "Applied position";
                worksheet.Cells["E1"].Style.Font.Bold = true;
                worksheet.Cells["E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["F1"].Value = "Submission date";
                worksheet.Cells["F1"].Style.Font.Bold = true;
                worksheet.Cells["F1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["F1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["G1"].Value = "Scheduled date";
                worksheet.Cells["G1"].Style.Font.Bold = true;
                worksheet.Cells["G1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["G1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["H1"].Value = "Participants";
                worksheet.Cells["H1"].Style.Font.Bold = true;
                worksheet.Cells["H1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["H1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                foreach (var interview in interviewsAvailable)
                {
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                    if (hiringProcessFound == null) return NotFound("No hiring process found in the database with id " + interview.InterviewId);

                    var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                    if (submissionFound == null) return NotFound("No submission found in the database with id " + hiringProcessFound.SubmissionId);

                    var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                    if (positionFound == null) return NotFound("No position found in the database with id " + submissionFound.PositionId);

                    var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                    if (candidateFound == null) return NotFound("No candidate found in the database with id " + submissionFound.CandidateId);

                    worksheet.Cells["B" + (index + 1)].Value = candidateFound.FirstName + " " + candidateFound.LastName;
                    worksheet.Cells["C" + (index + 1)].Value = candidateFound.Email;
                    worksheet.Cells["D" + (index + 1)].Value = candidateFound.PhoneNumber;
                    worksheet.Cells["E" + (index + 1)].Value = positionFound.PositionName;
                    worksheet.Cells["F" + (index + 1)].Value = submissionFound.DateOfSubmission.ToString("dd/MM/yyyy HH:mm:ss");
                    worksheet.Cells["G" + (index + 1)].Value = interview.InterviewDate.ToString("dd/MM/yyyy HH:mm:ss");


                    var participantsFound = await _repository.GetAllInterviewParticipantsAsync(interview.InterviewId);
                    if (participantsFound == null) return NotFound("No participants found in the database for interview with id " + interview.InterviewId);

                    string participantString = "";
                    foreach (var participant in participantsFound)
                    {
                        var employeeFound = await _userRepository.GetUserById(participant.EmployeeId);
                        if (employeeFound == null) return NotFound("No employee found in the database with id " + participant.EmployeeId);

                        participantString += $"{employeeFound.FirstName} {employeeFound.LastName}; ";
                    }

                    worksheet.Cells["H" + (index + 1)].Value = participantString;

                    participantString = "";

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "AvailableInterviews.xlsx");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("search/interviews/candidate")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<InterviewViewModel>>> CustomSearchFilterCandidateNameAsync(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == null)
            {
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interview was found in the database");

                return Ok(interviews);
            }

            if (searchKeyDTO.key is string)
            {
                var searchKey = searchKeyDTO.key as string;
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                var interviewsFinalList = new List<InterviewViewModel>();
                foreach (var interview in interviews)
                {
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                    var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                    var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                    if (candidateFound.FirstName.ToLower().ToString().Contains(searchKey.ToLower()) || candidateFound.LastName.ToLower().ToString().Contains(searchKey.ToLower()))
                    {
                        interviewsFinalList.Add(new InterviewViewModel()
                        {
                            InterviewId = interview.InterviewId,
                            CandidateFirstName = candidateFound.FirstName,
                            CandidateLastName = candidateFound.LastName,
                            CommunicationChannel = interview.CommunicationChannel.ToString(),
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            InterviewDate = interview.InterviewDate,
                            PositionName = positionFound.PositionName,
                            CreatedBy = interview.CreatedBy,
                            LastModifiedBy = interview.LastModifiedBy,
                            QuizAvailable = interview.QuizAvailable
                        });
                    }
                }

                if (interviewsFinalList == null)
                    return NotFound("No interviews was found in the database");

                return Ok(interviewsFinalList);
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("search/interviews/status")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<InterviewViewModel>>> CustomSearchFilterStatusAsync(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == "")
            {
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                return Ok(interviews);
            }

            if(searchKeyDTO.key is string)
            {
                var searchKey = searchKeyDTO.key as string;
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                var interviewsFinalList = new List<InterviewViewModel>();

                foreach(var interview in interviews)
                {
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                    var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                    var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                    if(hiringProcessFound.CandidateSituation.ToString().ToLower().Contains(searchKey.ToLower()))
                    {
                        interviewsFinalList.Add(new InterviewViewModel()
                        {
                            InterviewId = interview.InterviewId,
                            CandidateFirstName = candidateFound.FirstName,
                            CandidateLastName = candidateFound.LastName,
                            CommunicationChannel = interview.CommunicationChannel.ToString(),
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            InterviewDate = interview.InterviewDate,
                            PositionName = positionFound.PositionName,
                            CreatedBy = interview.CreatedBy,
                            LastModifiedBy = interview.LastModifiedBy,
                            QuizAvailable = interview.QuizAvailable
                        });
                    }
                }

                if (interviewsFinalList == null)
                    return NotFound("No interviews was found in the database");

                return Ok(interviewsFinalList);
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("search/interviews/date")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<InterviewViewModel>>> CustomSearchFilterDateAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate == "")
            {
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                return Ok(interviews);
            }

            if (searchKeyDTO.startDate is string && searchKeyDTO.lastDate is string)
            {
                var interviews = await _repository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                var interviewsFinal = new List<InterviewViewModel>();

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1, d2;

                    if (DateTime.TryParse(searchKeyDTO.startDate, out d1) && DateTime.TryParse(searchKeyDTO.lastDate, out d2))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                            if (interview.InterviewDate.Date >= d1 && interview.InterviewDate.Date <= d2)
                            {
                                interviewsFinal.Add(new InterviewViewModel()
                                {
                                    InterviewId = interview.InterviewId,
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    PositionName = positionFound.PositionName,
                                    CreatedBy = interview.CreatedBy,
                                    LastModifiedBy = interview.LastModifiedBy,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }
                        return Ok(interviewsFinal);
                    }
                    return Ok(interviewsFinal);
                }

                if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1;
                    if (DateTime.TryParse(searchKeyDTO.lastDate, out d1))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                            if (interview.InterviewDate.Date <= d1)
                            {
                                interviewsFinal.Add(new InterviewViewModel()
                                {
                                    InterviewId = interview.InterviewId,
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    PositionName = positionFound.PositionName,
                                    CreatedBy = interview.CreatedBy,
                                    LastModifiedBy = interview.LastModifiedBy,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }

                        return Ok(interviewsFinal);
                    }
                }

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate == "")
                {
                    DateTime d2;
                    if (DateTime.TryParse(searchKeyDTO.startDate, out d2))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                            if (interview.InterviewDate.Date >= d2)
                            {
                                interviewsFinal.Add(new InterviewViewModel()
                                {
                                    InterviewId = interview.InterviewId,
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    PositionName = positionFound.PositionName,
                                    CreatedBy = interview.CreatedBy,
                                    LastModifiedBy = interview.LastModifiedBy,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }
                        return Ok(interviewsFinal);
                    }
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }
    }
}
