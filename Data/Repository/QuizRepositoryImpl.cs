﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public class QuizRepositoryImpl : IQuizRepository
    {
        private readonly SubmissionContext _submissionContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public QuizRepositoryImpl(SubmissionContext submissionContext, IHttpContextAccessor httpContextAccessor)
        {
            _submissionContext = submissionContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task AddQuizAsync(Quiz quiz)
        {
            await _submissionContext.Quizzes.AddAsync(quiz);
        }

        public async Task<IEnumerable<Quiz>> GetAllQuizesAsync()
        {
            return await _submissionContext.Quizzes.ToListAsync();
        }

        public async Task<Quiz> GetQuizByIdAsync(int id)
        {
            return await _submissionContext.Quizzes.FirstOrDefaultAsync(q => q.QuizId == id);
        }

        public async Task<Quiz> GetQuizVariousParamsAsync(string createdById, string createdAt, int positionId)
        {
            return await _submissionContext.Quizzes.Where(q => q.CreatedById == createdById && q.CreatedAt == createdAt && q.PositionId == positionId).FirstOrDefaultAsync();
        }

        public void RemoveQuiz(Quiz quiz)
        {
            _submissionContext.Quizzes.Remove(quiz);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _submissionContext.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }

        public void UpdateQuiz(Quiz quiz)
        {

        }
    }
}
