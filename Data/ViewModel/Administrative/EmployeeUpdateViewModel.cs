﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class EmployeeUpdateViewModel
    {
        [Required]
        [MaxLength(30, ErrorMessage = "First name cannot be more than 30 characters")]
        [MinLength(2, ErrorMessage = "First name cannot be less than 2 characters")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(30, ErrorMessage = "Last name cannot be more than 30 characters")]
        [MinLength(2, ErrorMessage = "Last name cannot be less than 2 characters")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Email adress provided does not respect standards")]
        public string EmailAddress { get; set; }
        [Required]
        [MinLength(7, ErrorMessage = "Address cannot be less than 7 characters")]
        [MaxLength(50, ErrorMessage = "Address cannot be more than 50 characters")]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth{ get; set; }
        [Required]
        [Phone(ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }
        [Required]
        public int PositionId { get; set; }
        [Required]
        public string SecurityQuestion { get; set; }
        [Required]
        public string SecurityQuestionAnswer { get; set; }
    }
}
