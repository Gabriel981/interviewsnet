﻿using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class AuthResponseDto
    {
        public bool isAuthSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public string Token { get; set; }
        public User AppUser { get; set; }
        public string refreshToken { get; set; }
        public string Role { get; set; }
        public bool Is2StepVerificationRequired { get; set; }
        public string Provider { get; set; }
        public string Method { get; set; }
    }
}
