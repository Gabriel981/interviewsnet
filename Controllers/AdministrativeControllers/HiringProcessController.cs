﻿using AutoMapper;
using InterviewsManagementNET.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace InterviewsManagementNET.Controllers
{
    [Route("api/hirings")]
    [ApiController]
    public class HiringProcessController : Controller
    {
        private readonly IHiringRepo _repository;
        private readonly IMapper _mapper;

        public HiringProcessController(IHiringRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = "Recruiter")]
        public ActionResult<IEnumerable<HiringViewModel>> GetAllHiringProcesses()
        {
            var hirings = _repository.GetAllHiringProcesses();
            return Ok(_mapper.Map<IEnumerable<HiringViewModel>>(hirings));
        }

        [HttpGet("{id}")]
        public ActionResult<HiringViewModel> GetHiringProcessById(int Id)
        {
            var hiringProcessFound = _repository.GetHiringProcessByIdDetailed(Id);

            if (hiringProcessFound != null)
            {
                return Ok(_mapper.Map<HiringViewModel>(hiringProcessFound));
            }

            return NotFound();
        }
    }
}
