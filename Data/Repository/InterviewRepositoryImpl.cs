﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class InterviewRepositoryImpl : IInterviewRepo
    {

        private readonly SubmissionContext _context;
        private readonly IHiringRepo _hiringRepository;
        private readonly ISubmissionRepo _submissionRepository;
        private readonly IPositionRepo _positionRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InterviewRepositoryImpl(
            SubmissionContext context, 
            IHiringRepo hiringRepository, 
            ISubmissionRepo submissionRepository,
            IPositionRepo positionRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _hiringRepository = hiringRepository;
            _submissionRepository = submissionRepository;
            _positionRepository = positionRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async System.Threading.Tasks.Task AddInterviewAsync(Interview interview)
        {
            if (interview == null)
            {
                throw new ArgumentNullException(nameof(interview));
            }

            HiringProcess HiringFound = _context.Hirings.FirstOrDefault(h => h.HiringId == interview.HiringId);

            if (HiringFound.InterviewId != null)
            {
                Interview OldInterview = await this.GetInterviewByIdAsync((int)HiringFound.InterviewId);
                if (OldInterview == null)
                {
                    _context.Interviews.Add(interview);
                }
                else
                {
                    _context.Interviews.Remove(OldInterview);
                    HiringFound.InterviewId = null;
                    _hiringRepository.UpdateHiringProcess(HiringFound);
                    await _hiringRepository.SaveChangesAsync();
                    _context.Interviews.Add(interview);
                }
            }
            else
            {
                _context.Interviews.Add(interview);
            }
        }

        public async Task AddInterviewParticipantAsync(InterviewParticipant interviewParticipant)
        {
            await _context.InterviewParticipant.AddAsync(interviewParticipant);
        }

        public async Task<IEnumerable<InterviewParticipant>> GetAllInterviewParticipantsAsync(int id)
        {
            var interviews = await _context.InterviewParticipant.ToListAsync();
            return interviews.Where(ip => ip.InterviewId == id);
        }

        public async Task<IEnumerable<InterviewViewModel>> GetAllInterviewsAsync()
        {
            var interviews = await _context.Interviews.Include(h => h.HiringProcess).Include(c => c.HiringProcess.Submission.User)
                   .Include(c => c.HiringProcess.Submission).ToListAsync();

            var interviewsViewModels = new List<InterviewViewModel>();

            foreach(var interview in interviews)
            {
                var candidate = await _context.Users.FirstOrDefaultAsync(c => c.Id == interview.HiringProcess.Submission.CandidateId);
                var quiz = await _context.Quizzes.FirstOrDefaultAsync(q => q.QuizId == interview.QuizId);
                var hiringProcess = await _context.Hirings.FirstOrDefaultAsync(h => h.InterviewId == interview.InterviewId);
                var submission = await _context.Submissions.FirstOrDefaultAsync(s => s.SubmissionId == hiringProcess.SubmissionId);
                var position = await _context.Positions.FirstOrDefaultAsync(p => p.PositionId == submission.PositionId);
                var createdBy = await _context.Users.FirstOrDefaultAsync(u => u.Id == interview.CreatedById);
                var lastModifiedBy = await _context.Users.FirstOrDefaultAsync(u => u.Id == interview.LastModifiedById);

                interviewsViewModels.Add(new InterviewViewModel
                {
                    InterviewId = interview.InterviewId,
                    InterviewDate = interview.InterviewDate,
                    CommunicationChannel = interview.CommunicationChannel,
                    CandidateSituation = hiringProcess.CandidateSituation.ToString(),
                    CandidateFirstName = candidate.FirstName,
                    CandidateLastName = candidate.LastName,
                    PositionName = position.PositionName,
                    Submission = interview.HiringProcess.Submission,
                    HiringProcess = interview.HiringProcess,
                    CreatedBy = createdBy,
                    LastModifiedBy = lastModifiedBy,
                    Quiz = quiz
                });
            }

            return interviewsViewModels;

           /* return _context.Interviews.Include(h => h.HiringProcess)
                .Include(c => c.HiringProcess.Submission.User)
                .Include(c => c.HiringProcess.Submission).Select(i => new InterviewViewModel
                {
                    InterviewId = i.InterviewId,
                    InterviewDate = i.InterviewDate,
                    CommunicationChannel = i.CommunicationChannel,
                    CandidateSituation = i.HiringProcess.CandidateSituation.ToString(),
                    CandidateFirstName = _context.Users.FirstOrDefault(c => c.Id == i.HiringProcess.Submission.CandidateId).FirstName,
                    CandidateLastName = _context.Users.FirstOrDefault(c => c.Id == i.HiringProcess.Submission.CandidateId).LastName,
                    PositionName = i.HiringProcess.Submission.AppliedPosition.PositionName,
                    Submission = i.HiringProcess.Submission,
                    HiringProcess = i.HiringProcess,
                    CreatedBy = _context.Users.FirstOrDefault(u => u.Id == i.CreatedById),
                    LastModifiedBy = _context.Users.FirstOrDefault(u => u.Id == i.LastModifiedById),
                    Quiz = _context.Quizzes.FirstOrDefault(q => q.QuizId == i.QuizId)
                }).ToList();*/
        }

        public async Task<IEnumerable<Interview>> GetAllInterviewsByDateAsync(DateTime date)
        {
            IEnumerable<Interview> AInterviews = await _context.Interviews.ToListAsync();

            IEnumerable<Interview> InterviewsFound = new LinkedList<Interview>();

            foreach (Interview i in AInterviews)
            {
                if (i.InterviewDate == date)
                {
                    InterviewsFound.Append(i);
                }
            }

            return InterviewsFound;
        }

        public async Task<IEnumerable<InterviewParticipant>> GetAllInterviewsParticipantsAsync()
        {
            return await _context.InterviewParticipant.ToListAsync();
        }

        public async Task<Interview> GetInterviewByHiringProcessAsync(HiringProcess hiringProcess)
        {
            return await _context.Interviews.Where(i => i.HiringId == hiringProcess.HiringId && i.InterviewDate == hiringProcess.Interview.InterviewDate).FirstOrDefaultAsync();
        }

        public async Task<Interview> GetInterviewByIdAsync(int id)
        {
            return await _context.Interviews.FirstOrDefaultAsync(i => i.InterviewId == id);
        }

        public async Task<InterviewParticipant> GetInterviewParticipantByIdsAsync(int interviewId, string participantId)
        {
            return await _context.InterviewParticipant.FirstOrDefaultAsync(ip => ip.InterviewId == interviewId && ip.EmployeeId == participantId);
        }

        public async System.Threading.Tasks.Task removeInterviewAsync(Interview interview)
        {
            HiringProcess HiringToBeUpdated = await _hiringRepository.GetHiringProcessById(interview.HiringId);
            HiringToBeUpdated.InterviewId = null;
            _hiringRepository.UpdateHiringProcess(HiringToBeUpdated);
            await _hiringRepository.SaveChangesAsync();
            _context.Interviews.Remove(interview);
        }

        public void removeParticipantFromInterview(InterviewParticipant interviewParticipant)
        {
            if (interviewParticipant == null)
            {
                throw new ArgumentNullException(nameof(interviewParticipant));
            }

            _context.InterviewParticipant.Remove(interviewParticipant);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }

        public Task<IEnumerable<Interview>> ShowAllInterviewsAsync()
        {
            throw new NotImplementedException();
        }

        public void UpdateInterview(Interview interview)
        {
            // Implemented by default
        }
    }
}
