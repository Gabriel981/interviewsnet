﻿using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class QuizReadDTO
    {
        public string Mentions { get; set; }
        public int ScoreResult { get; set; }
        public Position position { get; set; }
        public string CreatedAt { get; set; }
        public User CreatedBy { get; set; }
    }
}
