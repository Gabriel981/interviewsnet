﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO.SubmissionsDTO
{
    public class StatusViewModel
    {
        public int SubmissionId { get; set; }
        public string CandidateSituation { get; set; }
    }
}
