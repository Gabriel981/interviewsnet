﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class FilterViewModel
    {
        public string FilterKey { get; set; }
        public string FilterValue { get; set; }
        public bool DefaultFilter { get; set; }
    }
}
