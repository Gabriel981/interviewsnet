﻿using InterviewsManagementNET.Data.ViewModel.Account;
using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Account
{
    public interface IAccountRepository
    {
        void UpdateUserSettings(User user);
        Task<bool> SaveChangesAsync();
    }
}
