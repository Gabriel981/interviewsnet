﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Models
{
    public class Answer
    {
        [Key]
        public int AnswerId { get; set; }
        public string AnswerBody { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public string CreatedAt { get; set; }
    }
}
