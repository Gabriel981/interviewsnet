﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class UserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public int PositionId { get; set; }

    }
}
