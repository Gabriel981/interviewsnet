﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InterviewsManagementNET.Models
{
    public class Interview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InterviewId { get; set; }
        public DateTime InterviewDate { get; set; }
        public string CommunicationChannel { get; set; }
        public int? QuizId { get; set; }
        public Quiz Quiz { get; set; }

        public ICollection<InterviewParticipant> InterviewParticipants { get; set; }

        public int HiringId { get; set; }
        public HiringProcess HiringProcess { get; set; }

        public string CreatedById { get; set; }
        public User CreatedBy { get; set; }

        public string LastModifiedById { get; set; }
        public User LastModifiedBy { get; set; }
    }
}
