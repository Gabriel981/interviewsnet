﻿using Microsoft.AspNetCore.Http;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class FileUploadViewModel
    {
        public string Email { get; set; }
        public IFormFile File { get; set; }
    }
}
