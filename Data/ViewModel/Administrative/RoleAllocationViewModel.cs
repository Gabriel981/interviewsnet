﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class RoleAllocationViewModel
    {
        public string UserId { get; set; }
        public int PositionId { get; set; }
    }
}
