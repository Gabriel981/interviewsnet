﻿using InterviewsManagementNET.Models;
using System;
using System.Net;
using System.Net.Mail;

namespace InterviewsManagementNET.Services
{
    public class MailSender
    {
        public void SendEmailSubmission(Submission submission)
        {
            MailAddress toCandidate = new MailAddress(submission.User.Email);
            MailAddress from = new MailAddress("admin@mail.com");

            MailMessage messageToBeSend = new MailMessage(from, toCandidate);
            messageToBeSend.Subject = "Application for our vacant position: ' + submission.AppliedPosition.PositionName";
            messageToBeSend.Body = "We are grad that you've applied for our vacant position " + submission.AppliedPosition.PositionName
                    + "Please check further if are any changes in your submission process / hiring process!";

            SmtpClient smtpClient = new SmtpClient("smtp.mailtrap.io", 2525)
            {

                Credentials = new NetworkCredential("c66c7a99de9d79", "39da7e6360b986"),
                EnableSsl = true
            };

            try
            {
                smtpClient.Send(messageToBeSend);
            }
            catch (SmtpException smtpException)
            {
                Console.Write(smtpException.ToString());
            }
        }
    }
}
