﻿using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserViewModel>> GetAllUsers();
        Task<User> GetUserById(string Id);
        Task<User> GetUserByEmailAddress(string EmailAddress);
        Task<User> GetUserByName(string Username);
        User GetUserByRole(string Role);
        void UpdateUserPosition(User user);
        void UpdateUserDetails(User user);
        void RemoveUser(User user);
        Task<bool> SaveChangesAsync();
    }
}
