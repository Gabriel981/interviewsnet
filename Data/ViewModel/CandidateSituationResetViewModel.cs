﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class CandidateSituationResetViewModel
    {
        public string SenderId { get; set; }
        public string Email { get; set; }
        public string Reason { get; set; }
    }
}
