﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        public string Email { get; set; }
        [Required]
        [MinLength(4, ErrorMessage = "The reason cannot be less than 4 characters")]
        [MaxLength(50, ErrorMessage = "The reason cannot be more than 50 characters")]
        public string Reason { get; set; }
    }
}
