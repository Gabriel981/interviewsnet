﻿using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Administrative.Implementations
{
    public class RejectAllocationImpl : IRejectAllocationRepository
    {
        private readonly SubmissionContext _context;

        public RejectAllocationImpl(SubmissionContext context)
        {
            _context = context;
        }

        public async Task AddRejectionAsync(RejectedAllocation rejectedAllocation)
        {
            await _context.RejectedAllocation.AddAsync(rejectedAllocation);
        }

        public async Task<RejectedAllocation> GetRejectionByIdAsync(string id)
        {
            return await _context.RejectedAllocation.FirstOrDefaultAsync(ra => ra.RejectionId == id);
        }

        public async Task<IEnumerable<RejectedAllocation>> GetRejectionsAsync()
        {
            return await _context.RejectedAllocation.ToListAsync();
        }

        public void RemoveRejection(RejectedAllocation rejectedAllocation)
        {
            _context.RejectedAllocation.Remove(rejectedAllocation);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
