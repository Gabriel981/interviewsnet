﻿using AutoMapper;
using Firebase.Storage;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO.OthersDTO;
using InterviewsManagementNET.Data.DTO.ClientDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers
{
    [ApiController]
    [Route("api/candidate/available")]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly ISubmissionRepo _submissionRepository;
        private readonly IHiringRepo _hiringRepository;
        private readonly IStateSeqRepository _iStateSeqRepository;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<User> _userManager;
        private readonly INotificationService _notificationService;
        private readonly IMapper _mapper;

        private static string APIKey = "AIzaSyA7PHmbFTE1VOfaltAa0UsaNgm4PempF30";
        private static string Bucket = "interviewsnetproject.appspot.com";


        public ApplicationController(
            IApplicationRepository repository,
            IMapper mapper, IUserRepository userRepository,
            ISubmissionRepo submissionRepository,
            IHiringRepo hiringRepository,
            IStateSeqRepository iStateSeqRepository,
            IWebHostEnvironment hostingEnvironment,
            UserManager<User> userManager,
            INotificationService notificationService)
        {
            _repository = repository;
            _userRepository = userRepository;
            _submissionRepository = submissionRepository;
            _hiringRepository = hiringRepository;
            _iStateSeqRepository = iStateSeqRepository;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _notificationService = notificationService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("positions")]
        public ActionResult<IEnumerable<ApplicationPositionReadDTO>> GetAllAvailablePositions()
        {
            var positions = _repository.GetAllAvailablePositions();
            return Ok(_mapper.Map<IEnumerable<ApplicationPositionReadDTO>>(positions));
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<ApplicationPositionReadDTO> GetPositionDetailsById(int id)
        {
            var positionFound = _repository.GetPositionById(id);
            if (positionFound == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ApplicationPositionReadDTO>(positionFound));
        }

        [HttpGet]
        [Route("position/name")]
        public ActionResult<ApplicationPositionReadDTO> GetPositionDetailsByName([FromBody] string name)
        {
            var positionFound = _repository.GetPositionByName(name);
            if (positionFound == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ApplicationPositionReadDTO>(positionFound));
        }

        [HttpGet]
        [Route("submissions/{id}")]
        [Authorize(Roles = "Candidate registered")]
        public ActionResult<IEnumerable<SubmissionViewModel>> GetCandidateSubmissions(string id)
        {
            IEnumerable<SubmissionViewModel> submissionsFound = this._repository.GetUserSubmissions(id);
            if (submissionsFound == null)
            {
                return NotFound();
            }

            return Ok(submissionsFound);
        }

        [HttpDelete]
        [Route("withdraw/{id}")]
        [Authorize(Roles = "Candidate registered")]
        public async Task<ActionResult<Task>> WithdrawSubmissionAsync(int id)
        {
            Submission submissionFound = await _submissionRepository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
            {
                return NotFound();
            }

            this._submissionRepository.RemoveSubmission(submissionFound);
            this._hiringRepository.RemoveHiringProcess(submissionFound.HiringProcess);

            return Ok("The operation of withdrawal was a success!");
        }


        [HttpPost]
        [Route("upload")]
        public async Task<ActionResult<string>> UploadCVAsync([FromForm] FileUploadViewModel file)
        {
            var fileUpload = file.File;

            var fileLink = "";

            if (fileUpload.Length > 0)
            {
                using (var memoryStream = new MemoryStream())
                {
                    fileUpload.CopyTo(memoryStream);

                    var cancellation = new CancellationTokenSource();

                    Console.WriteLine(memoryStream.Length.ToString());

                    var task = new FirebaseStorage(Bucket)
                        .Child("Uploads")
                        .Child(file.Email)
                        .Child($"{fileUpload.FileName}")
                        .PutAsync(fileUpload.OpenReadStream(), cancellation.Token);

                    try
                    {
                        fileLink = await task;
                        var candidateFound = await _userRepository.GetUserByEmailAddress(file.Email);
                        if (candidateFound == null)
                        {
                            return NotFound("No candidate found in the database with email address " + file.Email);
                        }

                        var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(candidateFound.Id);
                        if (submissionFound == null)
                        {
                            return NotFound("No submission found for the candidate with id " + candidateFound.Id);
                        }

                        submissionFound.CV = fileLink;
                        _submissionRepository.SubmissionUpdate(submissionFound);
                        await _submissionRepository.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception was thrown: {0}", ex);
                    }
                }
            }

            return Ok(fileLink);
        }

        [HttpGet]
        [Route("currentFields")]
        public async Task<ActionResult<IEnumerable<string>>> GetAllAvailableSubmissionFieldsAsync()
        {
            var stateField = await _iStateSeqRepository.GetAllFieldsStateAsync();
            var lastStateField = stateField.OrderByDescending(x => x.Id).FirstOrDefault();

            if (lastStateField == null)
                return NotFound("No last saved state fields found in the database");

            var availableFieldsList = new List<string>();

            var stateType = typeof(State);

            var lastStateProperties = lastStateField.GetType().GetProperties();

            foreach (var property in lastStateProperties)
            {
                if (property.GetValue(lastStateField, null) != null)
                {
                    if (property.GetValue(lastStateField, null).ToString() == "True")
                    {
                        availableFieldsList.Add(property.Name);
                    }
                }
            }

            var sequenceField = await _iStateSeqRepository.GetAllFieldsSequencesAsync();
            var lastSequenceField = sequenceField.OrderByDescending(x => x.Id).FirstOrDefault();

            if (lastSequenceField == null)
                return NotFound("No last sequence fields found in the database");

            var sequenceSortedFields = new SortedList();

            var sequenceType = typeof(Sequence);

            var sequenceFieldProperties = lastSequenceField.GetType().GetProperties();

            foreach (var property in sequenceFieldProperties)
            {
                foreach (var availableField in availableFieldsList)
                {
                    if (property.Name == availableField)
                    {
                        if (int.Parse(property.GetValue(lastSequenceField, null).ToString()) > 0)
                        {
                            sequenceSortedFields.Add(property.GetValue(lastSequenceField, null), property.Name);
                        }
                    }
                }

            }

            if (sequenceSortedFields == null)
                return NotFound();

            return Ok(sequenceSortedFields);
        }

        [HttpPost("removal")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        public async Task<ActionResult> ProposeCandidateRemovalAsync(ProposalViewModel proposalViewModel)
        {
            var sender = _userRepository.GetUserByEmailAddress(proposalViewModel.EmailSender).Result;
            if (sender == null) return NotFound();

            var candidate = _userRepository.GetUserByEmailAddress(proposalViewModel.EmailCandidate).Result;
            if (candidate == null) return NotFound();

            var admins = _userManager.GetUsersInRoleAsync("Administrator");
            foreach(var admin in admins.Result)
            {
                await _notificationService.PostRequestRemoval(admin.Id, candidate.Id, sender.Id);
            }

            return Ok("Succesfully sent the request to administrators");
        }
    }
}
