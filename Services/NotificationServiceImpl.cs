﻿using Firebase.Database;
using Firebase.Database.Query;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Services
{
    public class NotificationServiceImpl : INotificationService
    {
        public readonly IUserRepository _userRepository;
        public readonly IHiringRepo _hiringRepository;
        public readonly ISubmissionRepo _submissionRepository;
        public readonly IInterviewRepo _interviewRepository;
        public readonly IPositionRepo _positionRepository;

        public readonly string FIREBASE_CLIENT_LINK = "https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/";

        public NotificationServiceImpl(
            IUserRepository userRepository,
            IHiringRepo hiringRepository,
            ISubmissionRepo submissionRepository,
            IInterviewRepo interviewRepository,
            IPositionRepo positionRepository)
        {
            this._userRepository = userRepository;
            this._hiringRepository = hiringRepository;
            this._submissionRepository = submissionRepository;
            this._interviewRepository = interviewRepository;
            this._positionRepository = positionRepository;
        }

        public async Task<IReadOnlyCollection<FirebaseObject<Notification>>> GetData(string id)
        {
            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            var dbNotifications = await firebaseClient
                .Child("Notifications")
                .Child(id)
                .Child("ReceivedMessages")
                .OnceAsync<Notification>();

            return dbNotifications;
        }

        public async Task<IReadOnlyCollection<FirebaseObject<Notification>>> GetNotificationData(string notificationId)
        {
            /*            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
                        var dbNotificationFound = await firebaseClient
                            .Child("Notifications")
                            .Child(managerId)
                            .Child("ReceivedMessages")
                            .Child(notificationId)
                            .OnceAsync<Notification>();

                        return dbNotificationFound;*/

            return null;
        }

        public async void PostData(string receiverId, Notification notificationMessage)
        {
            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            await firebaseClient
                .Child("Notifications")
                .Child(receiverId)
                .Child("ReceivedMessages")
                .PostAsync(notificationMessage);
        }

        public async Task<bool> PostAcceptedNotificationData(string receiverId, string senderId, string positionName)
        {
            User candidateFound = await _userRepository.GetUserById(receiverId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            User managerFound = await _userRepository.GetUserById(senderId);
            if (managerFound == null)
            {
                throw new ArgumentNullException(nameof(managerFound));
            }

            string message = $"Hello {candidateFound.FirstName} {candidateFound.LastName}, we're glad to announce you that you've been accepted and we'll provide you" +
                $"  future informations about what you need to do in order to complete the hiring process!" +
                $" {managerFound.FirstName} {managerFound.LastName} - Manager at --Company name here--";

            string title = "New message about your hiring process";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification acceptedNotification = new Notification()
            {
                ReceviverId = receiverId,
                SenderId = senderId,
                PositionName = positionName,
                Timestamp = currentNotificationTime,
                Message = message,
                Title = title
            };

            if (candidateFound.EnableNotifications == true)
            {
                var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
                await firebaseClient
                    .Child("Notifications")
                    .Child(receiverId)
                    .Child("ReceivedMessages")
                    .PostAsync(acceptedNotification);
            }

            return true;
        }

        public async void RemoveNotification(NotificationDeleteViewModel notificationDeleteViewModel)
        {
            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            await firebaseClient
                .Child("Notifications")
                .Child(notificationDeleteViewModel.UserId)
                .Child("ReceivedMessages")
                .Child(notificationDeleteViewModel.NotificationId)
                .DeleteAsync();
        }

        public async Task<bool> PostRejectedNotificationData(string receiverId, string senderId, string positionName)
        {
            User candidateFound = await _userRepository.GetUserById(receiverId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            User managerFound = await _userRepository.GetUserById(senderId);
            if (managerFound == null)
            {
                throw new ArgumentNullException(nameof(managerFound));
            }

            string message = $"Hello {candidateFound.FirstName} {candidateFound.LastName}, after analysing your results on interview and compare them with the others, " +
                $" we're sorry to announce you that you've been rejected for position {positionName}, but " +
                $" your data will remain valid and we'll contact you if another similar position is available and we need to recruit again!" +
                $" Thank you for patience! " +
                $"{managerFound.FirstName} {managerFound.LastName} - Manager at --Company name here--";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification acceptedNotification = new Notification()
            {
                ReceviverId = receiverId,
                SenderId = senderId,
                PositionName = positionName,
                Timestamp = currentNotificationTime,
                Message = message
            };

            if (candidateFound.EnableNotifications == true)
            {
                var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
                await firebaseClient
                    .Child("Notifications")
                    .Child(receiverId)
                    .Child("ReceivedMessages")
                    .PostAsync(acceptedNotification);
            }

            return true;
        }

        public async Task<bool> PostRequestData(string receiverId, string candidateId, string senderId)
        {
            var candidateFound = await _userRepository.GetUserById(candidateId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            var adminFound = await _userRepository.GetUserById(receiverId);
            if (adminFound == null)
            {
                throw new ArgumentNullException(nameof(adminFound));
            }

            var managerFound = await _userRepository.GetUserById(senderId);
            if (managerFound == null)
            {
                throw new ArgumentNullException(nameof(managerFound));
            }

            string message = $"Hello {adminFound.FirstName} {adminFound.LastName}. I am requesting a change of status" +
                $" for candidate with the name {candidateFound.FirstName} {candidateFound.LastName} and id {candidateFound.Id}" +
                $" from Rejected or Accepted to default status." +
                $" Thank you! {managerFound.FirstName} {managerFound.LastName} - Manager of -- Company name --";

            string title = "A new change status request was raised";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification requestNotificationChange = new Notification()
            {
                ReceviverId = receiverId,
                CandidateId = candidateId,
                SenderId = senderId,
                Timestamp = currentNotificationTime,
                Title = title,
                Message = message
            };

            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            await firebaseClient
                .Child("Notifications")
                .Child(receiverId)
                .Child("ReceivedMessages")
                .PostAsync(requestNotificationChange);

            return true;
        }

        public async Task<bool> PostResetNotificationToManager(string senderId, string candidateId, string positionName)
        {
            var candidateFound = await _userRepository.GetUserById(candidateId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            var receiverFound = _userRepository.GetUserByRole("Manager departament resurse umane");
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. I've analysed your request to change the status of the candidate with id {candidateFound.Id} - {candidateFound.FirstName} {candidateFound.LastName}" +
                $" and now it's status is UnderReview (default status)." +
                $" {senderFound.FirstName} {senderFound.LastName} - Administrator at -- Company name --";

            var currentTimeNotification = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification resetConfirmationNotification = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = candidateFound.Id,
                PositionName = " ",
                Title = "Candidate status request confirmed",
                Message = message,
                Timestamp = currentTimeNotification
            };

            if (receiverFound.EnableNotifications == true)
            {
                var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
                await firebaseClient
                    .Child("Notifications")
                    .Child(receiverFound.Id)
                    .Child("ReceivedMessages")
                    .PostAsync(resetConfirmationNotification);
            }

            return true;
        }

        public async Task<bool> PostRequestRemoval(string receiverId, string candidateId, string senderId)
        {
            var candidateFound = await _userRepository.GetUserById(candidateId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            var message = $"Hello {receiverFound.FirstName} {receiverFound.LastName}." +
                $" I am requesting a removal of candidate with name {candidateFound.FirstName} {candidateFound.LastName} - id: {candidateFound.Id}," +
                $" because it exceeds the normal duration of candidature (30 days)." +
                $" {senderFound.FirstName} {senderFound.LastName} - Manager at -- Company name --";

            var notificationTitle = "A new request for candidate removal is raised";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification requestRemovalNotification = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = candidateFound.Id,
                PositionName = " ",
                Title = notificationTitle,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(requestRemovalNotification);


            return true;
        }

        public async Task<bool> PostRemovalSuccessNotification(string senderId)
        {
            var receiverFound = this._userRepository.GetUserByRole("Manager departament resurse umane");
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. After analysing your request, I've decided to remove the candidate that was mentioned by you." +
                $" This notification and the email that comes with this, it's just to notify you about the changes that was made in the database." +
                $" {senderFound.FirstName} {senderFound.LastName} - Administrator at -- Company name --";

            var currentTimeNotification = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            string notificationTitle = "The request of candidate removal was accepted";

            Notification removalNotification = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = "",
                PositionName = "",
                Title = notificationTitle,
                Message = message,
                Timestamp = currentTimeNotification
            };

            if (receiverFound.EnableNotifications == true)
            {
                var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
                await firebaseClient
                    .Child("Notifications")
                    .Child(receiverFound.Id)
                    .Child("ReceivedMessages")
                    .PostAsync(removalNotification);
            }

            return true;
        }

        public async Task<bool> PostInterviewRequestChange(string senderId, string receiverId, Interview interview, string reason)
        {
            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. After analysing the interview with id {interview.InterviewId} details, scheduled on {interview.InterviewDate}" +
                $" I've been considering that the interview needs some changes." +
                $" Reason: {reason}." +
                $" {senderFound.FirstName} {senderFound.LastName} - Manager at -- Company name --";

            string notificationTitle = "Request for changing the interview details";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification changeInterviewDetailsRequest = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = "",
                PositionName = "",
                Title = notificationTitle,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(changeInterviewDetailsRequest);

            return true;
        }

        public async Task<bool> PostInterviewRemovalRequest(string senderId, string receiverId, Interview interview, string reason)
        {
            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. After analysing the interview with id {interview.InterviewId}, scheduled on {interview.InterviewDate}," +
                $" I'm aware that it the needs to be removed from our database." +
                $" Reason: {reason}" +
                $" With regards, {senderFound.FirstName} {senderFound.LastName} - Manager at -- Company name --";

            string title = "Interview removal request";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification interviewRemovalRequest = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = "",
                PositionName = "",
                Title = title,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(interviewRemovalRequest);

            return true;
        }

        public async Task<bool> PostInterviewSpecialRemovalRequest(string senderId, string receiverId, Interview interview)
        {
            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. After analysing the interview with id {interview.InterviewId}, scheduled on {interview.InterviewDate}," +
               $" I'm aware that it the needs to be specialy removed from our database because it remains as a sign of an error." +
               $" With regards, {senderFound.FirstName} {senderFound.LastName} - Manager at -- Company name --";

            string title = "Interview removal request";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification interviewRemovalRequestAdmin = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = "",
                PositionName = "",
                Title = title,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(interviewRemovalRequestAdmin);

            return true;
        }

        public async Task<bool> PostAttendanceInterview(string senderId, string receiverId, int interviewId)
        {
            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
            {
                throw new ArgumentNullException(nameof(senderFound));
            }

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
            {
                throw new ArgumentNullException(nameof(receiverFound));
            }

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewId);
            if (interviewFound == null)
            {
                throw new ArgumentNullException(nameof(interviewId));
            }

            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(interviewFound.HiringId);
            if (hiringProcessFound == null)
            {
                throw new ArgumentNullException(nameof(hiringProcessFound));
            }

            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
            if (submissionFound == null)
            {
                throw new ArgumentNullException(nameof(submissionFound));
            }

            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
            {
                throw new ArgumentNullException(nameof(candidateFound));
            }

            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
            if (positionFound == null)
            {
                throw new ArgumentNullException(nameof(positionFound));
            }

            string message = $"Hello, {receiverFound.FirstName} {receiverFound.LastName}. You've been allocated to the interview " +
                $" with id {interviewFound.InterviewId}, scheduled on {interviewFound.InterviewDate}, for candidate {candidateFound.FirstName} {candidateFound.LastName}" +
                $" that applied for position: {positionFound.PositionName}, by {senderFound.FirstName} {senderFound.LastName}";

            string title = "Selection for interview participation";

            var currentTimeNotification = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification interviewAttendanceNotification = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = candidateFound.Id,
                PositionName = " ",
                Title = title,
                Message = message,
                Timestamp = currentTimeNotification
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(interviewAttendanceNotification);

            return true;
        }

        public async Task<bool> PostNotificationCreatedQuiz(string senderId, string receiverId, int interviewId)
        {
            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
                throw new ArgumentNullException(nameof(senderFound));

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
                throw new ArgumentNullException(nameof(receiverFound));

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewId);
            if (interviewFound == null)
                throw new ArgumentNullException(nameof(interviewFound));

            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(interviewFound.HiringId);
            if (hiringProcessFound == null)
                throw new ArgumentNullException(nameof(hiringProcessFound));

            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
            if (submissionFound == null)
                throw new ArgumentNullException(nameof(submissionFound));

            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
                throw new ArgumentNullException(nameof(candidateFound));

            var senderPositionFound = await _positionRepository.GetPositionByIdAsync(senderFound.PositionId);
            if (senderPositionFound == null)
                throw new ArgumentNullException(nameof(senderPositionFound));

            string message = $"A new quiz was created and succesfully included in interview scheduled on {interviewFound.InterviewDate} " +
                $"for candidate {candidateFound.FirstName} {candidateFound.LastName} by {senderFound.FirstName} {senderFound.LastName}";

            string title = "A new quiz was created";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification notification = new Notification()
            {
                CandidateId = candidateFound.Id,
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                PositionName = senderPositionFound.PositionName,
                Title = title,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(notification);

            return true;
        }

        public async Task<bool> PostNotificationRemovedQuiz(string senderId, string receiverId, int interviewId)
        {
            var senderFound = await _userRepository.GetUserById(senderId);
            if (senderFound == null)
                throw new ArgumentNullException(nameof(senderFound));

            var receiverFound = await _userRepository.GetUserById(receiverId);
            if (receiverFound == null)
                throw new ArgumentNullException(nameof(receiverFound));

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewId);
            if (interviewFound == null)
                throw new ArgumentNullException(nameof(interviewFound));

            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(interviewFound.HiringId);
            if (hiringProcessFound == null)
                throw new ArgumentNullException(nameof(hiringProcessFound));

            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
            if (submissionFound == null)
                throw new ArgumentNullException(nameof(submissionFound));

            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
                throw new ArgumentNullException(nameof(candidateFound));

            var senderPositionFound = await _positionRepository.GetPositionByIdAsync(senderFound.PositionId);
            if (senderPositionFound == null)
                throw new ArgumentNullException(nameof(senderPositionFound));

            string message = $"The quiz that was allocated to the interview scheduled on {interviewFound.InterviewDate}" +
                $" for candidate {candidateFound.FirstName} {candidateFound.LastName}, was succesfully removed from database by" +
                $" {senderFound.FirstName} {senderFound.LastName}";

            string title = "Quiz removal";

            var currentNotificationTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            Notification notification = new Notification()
            {
                SenderId = senderFound.Id,
                ReceviverId = receiverFound.Id,
                CandidateId = candidateFound.Id,
                PositionName = senderPositionFound.PositionName,
                Title = title,
                Message = message,
                Timestamp = currentNotificationTime
            };

            var firebaseClient = new FirebaseClient(FIREBASE_CLIENT_LINK);
            await firebaseClient
                .Child("Notifications")
                .Child(receiverFound.Id)
                .Child("ReceivedMessages")
                .PostAsync(notification);

            return true;
        }
    }
}
