﻿using System;

namespace InterviewsManagementNET.Data
{
    public class InterviewReadDTO
    {
        public int InterviewId { get; set; }
        public DateTime InterviewDate { get; set; }
        public string CommunicationChannel { get; set; }
    }
}
