﻿
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class UserRepositoryImpl : IUserRepository
    {
        private readonly SubmissionContext _context;

        public UserRepositoryImpl(SubmissionContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UserViewModel>> GetAllUsers()
        {
            return await _context.Users.Select(u => new UserViewModel
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                DateOfBirth = u.DateOfBirth,
                Address = u.Address,
                Email = u.Email,
                Position = _context.Positions.FirstOrDefault(p => p.PositionId == u.PositionId),
                Role = u.Role,
                Mentions = u.Mentions
            }).ToListAsync();
        }

        public async Task<User> GetUserByEmailAddress(string emailAddress)
        {
            return await _context.Users.Where(x => x.Email == emailAddress).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserById(string Id)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == Id);
        }

        public async Task<User> GetUserByName(string Username)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.UserName == Username);
        }

        public User GetUserByRole(string Role)
        {
            return _context.Users.FirstOrDefault(u => u.Role == Role);
        }

        public void RemoveUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            _context.Users.Remove(user);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void UpdateUserDetails(User user)
        {
            //
        }

        public void UpdateUserPosition(User user)
        {
            //
        }
    }
}
