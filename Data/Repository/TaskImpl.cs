﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace InterviewsManagementNET.Data.Repository
{
    public class TaskImpl : ITaskRepository
    {
        private readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TaskImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async System.Threading.Tasks.Task AddTaskAsync(AutoTask task)
        {
            await _context.Tasks.AddAsync(task);
        }

        public async System.Threading.Tasks.Task<IEnumerable<AutoTask>> GetAllRemovalTasksAsync()
        {
            return await _context.Tasks.Where(t => t.Task == Tasks.Delete).ToListAsync();
        }

        public async System.Threading.Tasks.Task<AutoTask> GetRemovalTaskByUserIdAsync(string id)
        {
            return await _context.Tasks.FirstOrDefaultAsync(t => t.UserId == id);
        }

        public void RemoveTask(AutoTask task)
        {
            _context.Tasks.Remove(task);
        }

        public async System.Threading.Tasks.Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }
    }
}
