﻿using Firebase.Database;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Services
{
    public interface INotificationService
    {
        void PostData(string receiverId, Notification notificationMessage);

        Task<bool> PostRequestData(string receiverId, string candidateId, string senderId);

        Task<bool> PostRequestRemoval(string receiverId, string candidateId, string senderId);

        Task<bool> PostAcceptedNotificationData(string receiverId, string senderId, string positionName);

        Task<bool> PostRejectedNotificationData(string receiverId, string senderId, string positionName);

        Task<bool> PostResetNotificationToManager(string senderId, string candidateId, string positionName);

        Task<bool> PostRemovalSuccessNotification(string senderId);

        Task<bool> PostInterviewRequestChange(string senderId, string receiverId, Interview interview, string reason);

        Task<bool> PostInterviewRemovalRequest(string senderId, string receiverId, Interview interview, string reason);

        Task<bool> PostInterviewSpecialRemovalRequest(string senderId, string receiverId, Interview interview);

        Task<bool> PostAttendanceInterview(string senderId, string receiverId, int interviewId);

        Task<bool> PostNotificationCreatedQuiz(string senderId, string receiverId, int interviewId);

        Task<bool> PostNotificationRemovedQuiz(string senderId, string receiverId, int interviewId);

        Task<IReadOnlyCollection<FirebaseObject<Notification>>> GetData(string id);

        Task<IReadOnlyCollection<FirebaseObject<Notification>>> GetNotificationData(string notificationId);

        void RemoveNotification(NotificationDeleteViewModel notificationDeleteViewModel);
    }
}
