﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class NotificationDeleteViewModel
    {
        public string NotificationId { get; set; }
        public string UserId { get; set; }
    }
}
