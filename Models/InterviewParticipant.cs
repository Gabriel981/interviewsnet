﻿using System;

namespace InterviewsManagementNET.Models
{
    public class InterviewParticipant
    {
        public int InterviewId { get; set; }
        public Interview Interview { get; set; }
        public string EmployeeId { get; set; }
        public User User { get; set; }
        public DateTime AttendedTime { get; set; }
    }
}
