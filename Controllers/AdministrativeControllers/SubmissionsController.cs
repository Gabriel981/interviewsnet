﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO.SubmissionsDTO;
using InterviewsManagementNET.Data.ViewModel.Administrative.Charts;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers
{
    [Route("submissions")]
    [ApiController]
    public class SubmissionsController : ControllerBase
    {
        private readonly ISubmissionRepo _repository;
        private readonly IPositionRepo _positionRepository;
        private readonly IHiringRepo _hiringRepository;
        private readonly IUserRepository _userRepository;
        private readonly IInterviewRepo interviewRepository;
        private readonly IMapper _mapper;
        private IMailServices _mail;

        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public SubmissionsController(
            ISubmissionRepo repository,
            IMapper mapper,
            IHiringRepo hiringRepository,
            IPositionRepo positionRepository,
            IUserRepository userRepository,
            IInterviewRepo interviewRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _hiringRepository = hiringRepository;
            _positionRepository = positionRepository;
            _userRepository = userRepository;
            this.interviewRepository = interviewRepository;
        }

        //Works
        [HttpGet]
        [Authorize(Roles = "Administrator, Candidate registered, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<SubmissionViewModel>>> GetAllSubmissionsAsync()
        {
            var submissions = await _repository.GetAllSubmissionsAsync();
            return Ok(_mapper.Map<IEnumerable<SubmissionViewModel>>(submissions));
        }

        //Works
        [HttpGet("{id}", Name = "GetSubmissionById")]
        public async System.Threading.Tasks.Task<ActionResult<SubmissionViewModel>> GetSubmissionByIdAsync(int id)
        {
            var submissionFound = await _repository.GetSubmissionByIdAsync(id);

            if (submissionFound != null)
            {
                return Ok(_mapper.Map<SubmissionViewModel>(submissionFound));
            }

            return NotFound();
        }

        [HttpPost]
        [Route("upload")]
        public async System.Threading.Tasks.Task<ActionResult<SubmissionReadDTO>> UploadSubmissionAsync(SubmissionWriteDTO submissionWriteDTO)
        {
            var submissionModelConversion = _mapper.Map<Submission>(submissionWriteDTO);
            DateTime submissionDate = DateTime.Now.Date;
            submissionModelConversion.DateOfSubmission = submissionDate;
            /*User candidateFound = this._userRepository.GetUserByEmailAddress(submissionWriteDTO.EmailAddress);*/
            User candidateFound = await _userRepository.GetUserByName(submissionWriteDTO.EmailAddress);
            if (candidateFound == null)
            {
                return NotFound();
            }
            else
            {
                submissionModelConversion.CandidateId = candidateFound.Id;
                submissionModelConversion.PositionId = submissionWriteDTO.PositionId;
            }

            await _repository.AddSubmissionAsync(submissionModelConversion);
            await _repository.SaveChangesAsync();

            var lastSubmission = await _repository.GetLastSubmission();

            if (lastSubmission == null) return BadRequest();


            HiringProcess hiringProcessAllocated = new HiringProcess();
            hiringProcessAllocated.CandidateSituation = CandidateSituation.Waiting;
            hiringProcessAllocated.SubmissionId = lastSubmission.SubmissionId;

            await _hiringRepository.addHiringProcess(hiringProcessAllocated);
            await _hiringRepository.SaveChangesRegisterAsync();

            HiringProcess hiringFound = await _hiringRepository.GetHiringProcessBySubmissionId(lastSubmission.SubmissionId);
            lastSubmission.HiringId = hiringFound.HiringId;
            _repository.SubmissionUpdate(lastSubmission);
            await _repository.SaveChangesAsync();

            Position position = await _positionRepository.GetPositionByIdAsync(lastSubmission.PositionId);

            lastSubmission.User = candidateFound;
            lastSubmission.AppliedPosition = position;

            _mail = new MailServicesImpl();
            _mail.SendEmailSubmission(lastSubmission);


            return Ok("The submission was succesfully registered in database");
            /*var submissionReadDTO = _mapper.Map<SubmissionReadDTO>(submissionModelConversion);

            return CreatedAtRoute(nameof(GetSubmissionById), new { Id = submissionReadDTO.SubmissionId }, submissionReadDTO);*/
        }

        //Works
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<ActionResult> UpdateSubmissionAsync(int id, SubmissionUpdateDTO submission)
        {
            var submissionFromRepository = await _repository.GetSubmissionByIdAsync(id);
            if (submissionFromRepository == null)
            {
                return NotFound();
            }

            _mapper.Map(submission, submissionFromRepository);
            _repository.SubmissionUpdate(submissionFromRepository);
            await _repository.SaveChangesAsync();

            return Ok("Succesfully updated submissions details");
        }

        //Works
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<ActionResult> RemoveSubmissionAsync(int id)
        {
            var submissionFound = await _repository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
                return NotFound("No submission was found in the database with id " + id);

            var hiringFound = await _hiringRepository.GetHiringProcessById((int)submissionFound.HiringId);
            if (hiringFound == null)
                return NotFound($"No hiring process was found in the database for submission with id {submissionFound.SubmissionId}");

            _hiringRepository.RemoveHiringProcess(hiringFound);

            _repository.RemoveSubmission(submissionFound);
            await _repository.SaveChangesAsync();

            return Ok("The submission was sucesfully removed from the database");
        }

        [HttpGet]
        [Route("cv/{id}")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult<SubmissionViewModel>> GetSelectedCandidateCVAsync(int id)
        {
            var submissionFound = await _repository.GetSubmissionByIdAsync(id);

            if (submissionFound == null) return NotFound("No submission found in the database with id " + id);

            return Ok(_mapper.Map<SubmissionViewModel>(submissionFound));
        }

        [HttpGet]
        [Route("export")]
        [Authorize(Roles = "Administrator, Recruiter")]
        public async System.Threading.Tasks.Task<IActionResult> ExportSubmissionsDataAsync()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var submissionsAvailable = await _repository.GetAllSubmissionsAsync();
            if (submissionsAvailable == null) return NotFound("No submissions found in the database");

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Submissions");

                int index = 1;

                worksheet.Cells["B1"].Value = "Candidate complete name";
                worksheet.Cells["B1"].Style.Font.Bold = true;
                worksheet.Cells["B1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["C1"].Value = "Candidate address";
                worksheet.Cells["C1"].Style.Font.Bold = true;
                worksheet.Cells["C1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["D1"].Value = "Candidate email address";
                worksheet.Cells["D1"].Style.Font.Bold = true;
                worksheet.Cells["D1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["D1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["E1"].Value = "Candidate phone no.";
                worksheet.Cells["E1"].Style.Font.Bold = true;
                worksheet.Cells["E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["F1"].Value = "Candidate date of birth";
                worksheet.Cells["F1"].Style.Font.Bold = true;
                worksheet.Cells["F1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["F1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["G1"].Value = "Applied position";
                worksheet.Cells["G1"].Style.Font.Bold = true;
                worksheet.Cells["G1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["G1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["H1"].Value = "Submission date";
                worksheet.Cells["H1"].Style.Font.Bold = true;
                worksheet.Cells["H1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["H1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["I1"].Value = "Candidate current status";
                worksheet.Cells["I1"].Style.Font.Bold = true;
                worksheet.Cells["I1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["I1"].Style.Font.Color.SetColor(System.Drawing.Color.White);


                foreach (var submission in submissionsAvailable)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    if (candidateFound == null) return NotFound("No candidate found in the database with id " + submission.CandidateId);

                    var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                    if (positionFound == null) return NotFound("No position found in the database with id " + submission.PositionId);

                    var hiringProcessFound = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                    if (hiringProcessFound == null) return NotFound("No hiring process found in the database with submission id " + submission.SubmissionId);

                    worksheet.Cells["B" + (index + 1)].Value = candidateFound.FirstName + " " + candidateFound.LastName;
                    worksheet.Cells["C" + (index + 1)].Value = candidateFound.Address;
                    worksheet.Cells["D" + (index + 1)].Value = candidateFound.Email;
                    worksheet.Cells["E" + (index + 1)].Value = candidateFound.PhoneNumber;
                    worksheet.Cells["F" + (index + 1)].Value = candidateFound.DateOfBirth.ToString("dd/MM/yyyy");
                    worksheet.Cells["G" + (index + 1)].Value = positionFound.PositionName;
                    worksheet.Cells["H" + (index + 1)].Value = submission.DateOfSubmission.ToString("dd/MM/yyyy HH:mm:ss");
                    worksheet.Cells["I" + (index + 1)].Value = hiringProcessFound.CandidateSituation.ToString();

                    switch (hiringProcessFound.CandidateSituation)
                    {
                        case CandidateSituation.Accepted:
                            worksheet.Cells["I" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["I" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                            break;
                        case CandidateSituation.Rejected:
                            worksheet.Cells["I" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["I" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                            worksheet.Cells["I" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                        case CandidateSituation.UnderReview:
                            worksheet.Cells["I" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["I" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Aqua);
                            worksheet.Cells["I" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                        case CandidateSituation.Waiting:
                            worksheet.Cells["I" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["I" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                            worksheet.Cells["I" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                            break;
                        case CandidateSituation.ReadyForInterview:
                            worksheet.Cells["I" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["I" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Teal);
                            worksheet.Cells["I" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                    }


                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "Submissions.xlsx");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPost("schedule")]
        public async System.Threading.Tasks.Task<ActionResult> ScheduleFastInterviewAsync([FromBody] FastInterviewWriteDTO fastInterviewWriteDTO)
        {
            var submissionFound = await _repository.GetSubmissionByIdAsync(fastInterviewWriteDTO.SubmissionId);
            if (submissionFound == null)
                return NotFound("No submission was found in the database with id " + fastInterviewWriteDTO.SubmissionId);

            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submissionFound.HiringId);
            if (hiringProcessFound == null)
                return NotFound("No hiring process was found in the database for submission with id " + submissionFound.SubmissionId);

            if (hiringProcessFound.InterviewId != null)
                return BadRequest("This submission has already an interview scheduled");

            var creatorFound = await _userRepository.GetUserById(fastInterviewWriteDTO.CreatedById);
            if (creatorFound == null)
                return NotFound("No user was found in the database with id " + fastInterviewWriteDTO.CreatedById);

            var currentDate = DateTime.UtcNow.AddHours(2);
            var interviewDate = currentDate.AddDays(3);

            Interview interview = new Interview()
            {
                CommunicationChannel = "ONLINE",
                HiringId = hiringProcessFound.HiringId,
                InterviewDate = interviewDate,
                CreatedById = creatorFound.Id
            };

            await interviewRepository.AddInterviewAsync(interview);
            await interviewRepository.SaveChangesAsync();

            var interviewFound = await interviewRepository.GetInterviewByHiringProcessAsync(hiringProcessFound);
            if (interviewFound == null)
                return NotFound("No interview found in database for hiring process with id " + hiringProcessFound.HiringId);

            hiringProcessFound.InterviewId = interviewFound.InterviewId;
            _hiringRepository.UpdateHiringProcess(hiringProcessFound);
            await _hiringRepository.SaveChangesAsync();

            return Ok("The interview was sucesfully created and added in the database");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/submissions/date")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<SubmissionViewModel>>> CustomFilterBySubmissionDateAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate == "")
            {
                var submissions = await _repository.GetAllSubmissionsAsync();
                return Ok(submissions);
            }

            if (searchKeyDTO.startDate is string && searchKeyDTO.lastDate is string)
            {
                var submissions = await _repository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1, d2;

                    if (DateTime.TryParse(searchKeyDTO.startDate, out d1) && DateTime.TryParse(searchKeyDTO.lastDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            if (submission.DateOfSubmission >= d1 && submission.DateOfSubmission.Date <= d2)
                            {
                                var hiringProcess = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                                var user = await _userRepository.GetUserById(submission.CandidateId);

                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    SubmissionId = submission.SubmissionId,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    AppliedPosition = await _positionRepository.GetPositionByIdAsync(submission.PositionId),
                                    User = user,
                                    Mentions = submission.Mentions,
                                    Status = hiringProcess.CandidateSituation.ToString(),
                                    CV = submission.CV,
                                });
                            }
                        }
                        return Ok(submissionsFinalList);
                    }
                }

                if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1;
                    if (DateTime.TryParse(searchKeyDTO.lastDate, out d1))
                    {
                        foreach (var submission in submissions)
                        {
                            if (submission.DateOfSubmission.Date <= d1)
                            {
                                var hiringProcess = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                                var user = await _userRepository.GetUserById(submission.CandidateId);

                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    SubmissionId = submission.SubmissionId,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    AppliedPosition = await _positionRepository.GetPositionByIdAsync(submission.PositionId),
                                    User = user,
                                    Mentions = submission.Mentions,
                                    Status = hiringProcess.CandidateSituation.ToString(),
                                    CV = submission.CV,
                                });
                            }
                        }

                        return Ok(submissionsFinalList);
                    }
                }

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate == "")
                {
                    DateTime d2;
                    if (DateTime.TryParse(searchKeyDTO.startDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            if (submission.DateOfSubmission.Date >= d2)
                            {
                                var hiringProcess = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                                var user = await _userRepository.GetUserById(submission.CandidateId);

                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    SubmissionId = submission.SubmissionId,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    AppliedPosition = await _positionRepository.GetPositionByIdAsync(submission.PositionId),
                                    User = user,
                                    Mentions = submission.Mentions,
                                    Status = hiringProcess.CandidateSituation.ToString(),
                                    CV = submission.CV,
                                });
                            }
                        }

                        return Ok(submissionsFinalList);
                    }
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/submissions/candidate")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<SubmissionViewModel>>> CustomFiltersByCandidateAsync(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == "")
            {
                var submissions = await _repository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submissions was found in the database");

                return Ok(submissions);
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var submissions = await _repository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submissions found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();
                
                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var submission in submissions)
                    {
                        var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                        if (candidateFound.FirstName.ToLower().ToString().Contains(searchKey.ToLower()) || candidateFound.LastName.ToLower().ToString().Contains(searchKey.ToLower()))
                        {
                            var hiringProcess = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                            var user = await _userRepository.GetUserById(submission.CandidateId);

                            submissionsFinalList.Add(new SubmissionViewModel()
                            {
                                SubmissionId = submission.SubmissionId,
                                DateOfSubmission = submission.DateOfSubmission,
                                AppliedPosition = await _positionRepository.GetPositionByIdAsync(submission.PositionId),
                                User = user,
                                Mentions = submission.Mentions,
                                Status = hiringProcess.CandidateSituation.ToString(),
                                CV = submission.CV,
                            });
                        }
                    }

                    return Ok(submissionsFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [HttpPost("status/change/{id}")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        public async Task<ActionResult> ChangeCandidateReviewStatus(int id)
        {
            var submissionFound = await _repository.GetSubmissionByIdAsync(id);
            if (submissionFound == null) return NotFound("No submission was found in the database");

            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submissionFound.HiringId);
            if (hiringProcessFound == null) return NotFound("No hiring process was found in the database");

            CandidateSituation newCandidateStatus;
            Enum.TryParse("UnderReview", out newCandidateStatus);


            hiringProcessFound.CandidateSituation = newCandidateStatus;

            _hiringRepository.UpdateHiringProcess(hiringProcessFound);
            await _hiringRepository.SaveChangesAsync();

            return Ok("Succesfully updated candidate status");
        }

        [HttpGet("chart")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetSubmissionsDataChartAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            var submissions = await _repository.GetAllSubmissionsOfficialAsync();
            if (submissions == null) return BadRequest();

            var crtDate = DateTime.Now;
            var crtDateMinus7Days = DateTime.Now.AddDays(-7);

            var submissionsLastSevenDays = submissions.Where(s => s.DateOfSubmission >= crtDateMinus7Days && s.DateOfSubmission < crtDate);

            List<SubmissionsChartViewModel> submissionsChart = new List<SubmissionsChartViewModel>();

            Dictionary<string, int> submissionsDictionary = new Dictionary<string, int>();
            bool exists = false;

            foreach(var submissionDate in submissionsLastSevenDays)
            {
                var date = submissionDate.DateOfSubmission.ToShortDateString();
                if(submissionsDictionary.ContainsKey(date))
                {
                    exists = true;
                    submissionsDictionary[date] += 1;
                } else
                {
                    exists = false;
                    submissionsDictionary.Add(date, 1);
                }
            }

            foreach(KeyValuePair<string, int> keyValue in submissionsDictionary)
            {
                submissionsChart.Add(new SubmissionsChartViewModel
                {
                    Date = keyValue.Key,
                    NoOfSubmissions = keyValue.Value
                });
            }

            return Ok(submissionsChart);
        }

        public static bool IsAlphaNumeric(string keyToCheck)
        {
            return keyToCheck.All(char.IsLetterOrDigit);
        }

    }
}
