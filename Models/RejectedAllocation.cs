﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Models
{
    public class RejectedAllocation
    {
        public string RejectionId { get; set; }

        public string CompleteName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNo { get; set; }

        public string Reason { get; set; }

        public string RejectedOn { get; set; }

        public string LastModifiedOn { get; set; }
    }
}
