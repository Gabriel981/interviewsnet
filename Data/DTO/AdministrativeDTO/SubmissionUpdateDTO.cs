﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class SubmissionUpdateDTO
    {
        [MaxLength(60, ErrorMessage = "The mentions can't be more than 200 characters")]
        [MinLength(0)]
        public string Mentions { get; set; }
    }
}
