﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/questions")]
    public class QuestionController : ControllerBase
    {
        private readonly IQuestionRepository questionRepository;
        private readonly IUserRepository userRepository;
        private readonly IPositionRepo positionRepository;
        private readonly IAnswerRepository answerRepository;
        private readonly IMapper mapper;

        public QuestionController(
            IQuestionRepository questionRepository,
            IUserRepository userRepository,
            IPositionRepo positionRepository,
            IAnswerRepository answerRepository,
            IMapper mapper)
        {
            this.questionRepository = questionRepository;
            this.userRepository = userRepository;
            this.positionRepository = positionRepository;
            this.answerRepository = answerRepository;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<QuestionReadDTO>>> GetAllQuestionsAsync()
        {
            var questionsList = await questionRepository.GetAllQuestionsAsync();
            if (questionsList == null)
                return NotFound("No questions are available in the database");

            var finalList = new List<QuestionReadDTO>();

            foreach (var question in questionsList)
            {
                var createdByFound = await userRepository.GetUserById(question.CreatedById);
                if (createdByFound == null)
                    return NotFound("No user found in the database with id " + question.CreatedById);

                var lastModifiedBy = await userRepository .GetUserById(question.LastModifiedById);

                var questionReadDTO = mapper.Map<QuestionReadDTO>(question);
                questionReadDTO.CreatedBy = createdByFound.FirstName + " " + createdByFound.LastName;
                questionReadDTO.LastModifiedBy = lastModifiedBy.FirstName + " " + lastModifiedBy.LastName;

                var answersFound = await questionRepository.GetAnswersByQuestionIdAsync(question.QuestionId);
                if (answersFound == null)
                    return NotFound("No answers found in the database for question with id " + question.QuestionId);

                questionReadDTO.NoOfAnswers = answersFound.Count();

                var positionFound = await positionRepository.GetPositionByIdAsync(question.PositionId);
                if (positionFound == null)
                    return NotFound("No position found in the database with id " + question.PositionId);

                questionReadDTO.Position = positionFound.PositionName;

                finalList.Add(questionReadDTO);
            }

            return Ok(mapper.Map<IEnumerable<QuestionReadDTO>>(finalList));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<ActionResult<QuestionReadDTO>> GetQuestionByIdAsync(int id)
        {
            var questionFound = await questionRepository.GetQuestionByIdAsync(id);
            if (questionFound == null)
                return NotFound("No question was found in the database with id " + id);

            return Ok(mapper.Map<QuestionReadDTO>(questionFound));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("answers/{id}")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<AnswerReadDTO>>> GetQuestionAnswersAsync(int id)
        {
            var questionFound = await questionRepository.GetQuestionByIdAsync(id);
            if (questionFound == null)
                return NotFound("No question was found in the database with id " + id);

/*            var questionAnswersFound = this.questionRepository.GetAnswersByQuestionId(questionFound.QuestionId).Select(s => new AnswerReadDTO
            {
                AnswerBody = s.AnswerBody,
                AnswerId = s.AnswerId,
                CreatedBy = this.userRepository.GetUserById(s.CreatedById).FirstName + " " + this.userRepository.GetUserById(s.CreatedById).LastName,
                CreatedAt = s.CreatedAt,
                QuestionId = s.QuestionId
            });*/

            var questions = await questionRepository.GetAnswersByQuestionIdAsync(questionFound.QuestionId);
            List<AnswerReadDTO> answers = new List<AnswerReadDTO>();

            foreach (var question in questions)
            {
                User createdBy = await userRepository.GetUserById(question.CreatedById);

                answers.Add(new AnswerReadDTO
                {
                    AnswerBody = question.AnswerBody,
                    AnswerId = question.AnswerId,
                    CreatedBy = createdBy.FirstName + " " + createdBy.LastName,
                    CreatedAt = question.CreatedAt,
                    QuestionId = question.QuestionId
                });
            }

            if (answers == null)
                return NotFound("No answers found in the database for question with id " + questionFound.QuestionId);

            return Ok(answers);
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task<ActionResult<string>> UpdateQuestionAsync(int id, [FromBody] QuestionWriteDTO questionWriteDTO)
        {
            var questionFound = await questionRepository.GetQuestionByIdAsync(id);
            if (questionFound == null)
                return NotFound("No question was found in the database with id " + id);

            questionFound.QuestionBody = questionWriteDTO.QuestionBody;
            questionFound.LastModifiedById = questionWriteDTO.LastModifiedById;

            var currentTime = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm:ss");
            questionFound.LastModifiedAt = currentTime;

            this.questionRepository.UpdateQuestion(questionFound);
            await questionRepository.SaveChangesAsync();

            return Ok("The question details was succesfully updated in database");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<ActionResult<string>> RemoveQuestionAsync(int id)
        {
            var questionFound = await questionRepository.GetQuestionByIdAsync(id);
            if (questionFound == null)
                return NotFound("No question was found in the database with id " + id);

            var questionQuizes = await questionRepository.GetAllOccurencesByQuestionIdAsync(questionFound.QuestionId);
            foreach (var questionQuiz in questionQuizes)
            {
                this.questionRepository.RemoveQuestionFromQuiz(questionQuiz);
                await questionRepository.SaveChangesAsync();
            }

            var answersFound = await questionRepository.GetAnswersByQuestionIdAsync(questionFound.QuestionId);
            foreach (var answerFound in answersFound)
            {
                this.answerRepository.RemoveAnswer(answerFound);
                this.answerRepository.SaveChanges();
            }

            this.questionRepository.RemoveQuestion(questionFound);
            await questionRepository.SaveChangesAsync();

            return Ok("The question was succesfully removed from the database");
        }
    }
}
