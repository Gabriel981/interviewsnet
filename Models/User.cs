﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Models
{
    public class User : IdentityUser
    {
        [Required]
        [MinLength(2, ErrorMessage = "The first name cannot be less than 2 characters")]
        [MaxLength(30, ErrorMessage = "The first name cannot be more than 30 characters")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "The last name cannot be less than 2 characters")]
        [MaxLength(30, ErrorMessage = "The last name cannot be more than 30 characters")]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "The address details cannot be less than 5 characters")]
        [MaxLength(100, ErrorMessage = "The address cannot be more than 100 characters")]
        public string Address { get; set; }

        public int PositionId { get; set; }

        public Position Position { get; set; }

        public ICollection<InterviewParticipant> InterviewParticipants { get; set; }

        public ICollection<Quiz> CreatedQuizes { get; set; }

        public string Role { get; set; }

        public string Mentions { get; set; }

        public string JwtToken { get; set; }

        public string RefreshToken { get; set; }

        [Required]
        public string SecurityQuestion { get; set; }

        [Required]
        public string SecurityQuestionAnswer { get; set; }

        public string ProfilePhotoURL { get; set; }

        public bool EnableNotifications { get; set; }

        public bool EnabledTwoFactorGoogle { get; set; }
    }
}
