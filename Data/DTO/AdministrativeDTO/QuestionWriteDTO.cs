﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class QuestionWriteDTO
    {
        public string QuestionBody { get; set; }
        public string CreatedById { get; set; }
        public int PositionId { get; set; }
        public int QuizId { get; set; }
        public string LastModifiedById { get; set; }
    }
}
