﻿namespace InterviewsManagementNET.Models
{
    public class Sequence
    {
        public int Id { get; set; }
        public int StateId { get; set; }
        public State State { get; set; }
        public int EmailAddress { get; set; }
        public int Password { get; set; }
        public int FirstName { get; set; }
        public int LastName { get; set; }
        public int Address { get; set; }
        public int PhoneNumber { get; set; }
        public int SecurityQuestion { get; set; }
        public int SecurityQuestionAnswer { get; set; }
        public int File { get; set; }
        public int Position { get; set; }
    }
}
