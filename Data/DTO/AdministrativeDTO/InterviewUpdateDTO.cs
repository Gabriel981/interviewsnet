﻿using System;

namespace InterviewsManagementNET.Data
{
    public class InterviewUpdateDTO
    {
        public int InterviewId { get; set; }
        public DateTime InterviewDate { get; set; }
        public string CommunicationChannel { get; set; }
        public int QuizId { get; set; }
        public int HiringId { get; set; }
    }
}
