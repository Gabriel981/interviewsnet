﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class FastInterviewWriteDTO
    {
        public string CreatedById { get; set; }
        public int SubmissionId { get; set; }
    }
}
