﻿using InterviewsManagementNET.Data.Repository.Account;
using InterviewsManagementNET.Data.ViewModel.Account;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(IAccountRepository accountRepository, UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _accountRepository = accountRepository;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost("settings")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter, Employee, Registered candidate")]
        public async Task<ActionResult> ChangeAccountSettings(AccountViewModel accountView)
        {
            var user = await _userManager.FindByIdAsync(accountView.UserId);
            if (user == null) return BadRequest();

            user.TwoFactorEnabled = accountView.IsTwoFactorEnabled;
            user.EnableNotifications = accountView.IsNotificationEnabled;
            user.EnabledTwoFactorGoogle = accountView.IsTwoFactorGoogleEnabled;
            _accountRepository.UpdateUserSettings(user);
            bool success = await _accountRepository.SaveChangesAsync();

            return Ok(success);
        }

        [HttpGet("refresh")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter, Employee, Registered candidate")]
        public async Task<ActionResult> RefreshUserProfileAsync()
        {
            var user = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userFound = await _userManager.FindByEmailAsync(user);
            if (userFound == null) return BadRequest();

            return Ok(userFound);
        }
    }
}
