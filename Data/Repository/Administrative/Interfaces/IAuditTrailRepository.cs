﻿using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Administrative.Interfaces
{
    public interface IAuditTrailRepository
    {
        Task<IEnumerable<Audit>> GetAllAuditTrailsAsync();
        Task<Audit> GetAuditTrailByIdAsync(int id);
        Task<IEnumerable<Audit>> GetAllAuditTrailsByUserIdAsync(string userId);
        void RemoveAuditTrail(Audit audit);
        Task<bool> SaveChangesAsync();
    }
}
