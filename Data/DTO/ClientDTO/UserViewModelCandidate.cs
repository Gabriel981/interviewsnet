﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data.DTO.ClientDTO
{
    public class UserViewModelCandidate
    {
        [Required]
        [MinLength(2, ErrorMessage = "You cannot insert a first name that is less than 2 characters")]
        [MaxLength(20, ErrorMessage = "You cannot insert a first name that has more than 20 characters")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "You cannot insert a last name that is less than 2 characters")]
        [MaxLength(20, ErrorMessage = "You cannot insert a last name that has more than 20 characters")]
        public string LastName { get; set; }

        [Required]
        [MinLength(10, ErrorMessage = "Cannot insert an address that has less than 10 characters")]
        [MaxLength(50, ErrorMessage = "Cannot insert an address that has more than 50 characters")]
        public string Address { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

    }
}
