﻿using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Data
{
    public class HiringViewModel
    {
        public int HiringId { get; set; }
        public int SubmissionId { get; set; }
        public string CandidateSituation { get; set; }
        public Submission Submission { get; set; }
        public User Candidate { get; set; }
        public Position AppliedPosition { get; set; }
        public Interview Interview { get; set; }
    }
}
