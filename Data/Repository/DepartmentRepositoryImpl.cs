﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public class DepartmentRepositoryImpl : IDepartmentRepository
    {
        private readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DepartmentRepositoryImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public void AddDepartment(Department department)
        {
            _context.Departments.AddAsync(department);
        }

        public async Task<Department> GetDepartment(string name)
        {
            return await _context.Departments.FirstOrDefaultAsync(x => x.DepartmentName == name);
        }

        public async Task<Department> GetDepartmentById(int id)
        {
            return await _context.Departments.FirstOrDefaultAsync(x => x.DepartmentId == id);
        }

        public async Task<IEnumerable<Department>> GetDepartmentsAsync()
        {
            return await _context.Departments.ToListAsync();
        }

        public void RemoveDepartment(Department department)
        {
            _context.Remove(department);
        }

        public async Task<bool> SaveAllChangesAsync()
        {
            return  await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }

        public void UpdateDepartment(Department department)
        {
            _context.Update(department);
        }
    }
}