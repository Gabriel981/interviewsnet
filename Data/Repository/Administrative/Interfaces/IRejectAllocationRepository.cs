﻿using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Administrative
{
    public interface IRejectAllocationRepository
    {
        Task<RejectedAllocation> GetRejectionByIdAsync(string id);
        Task<IEnumerable<RejectedAllocation>> GetRejectionsAsync();
        Task AddRejectionAsync(RejectedAllocation rejectedAllocation);
        void RemoveRejection(RejectedAllocation rejectedAllocation);
        Task<bool> SaveChangesAsync();
    }
}
