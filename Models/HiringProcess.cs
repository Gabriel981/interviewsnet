﻿using System.ComponentModel;

namespace InterviewsManagementNET.Models
{
    public class HiringProcess
    {
        public int HiringId { get; set; }
        public int? InterviewId { get; set; }
        public Interview Interview { get; set; }
        public int SubmissionId { get; set; }
        public Submission Submission { get; set; }

        [DefaultValue("Waiting")]
        public CandidateSituation CandidateSituation { get; set; }
        public string Mentions { get; set; }

    }

    public enum CandidateSituation : int
    {
        Waiting = 1,
        UnderReview = 2,
        ReadyForInterview = 3,
        ScheduledInterview = 4,
        ScheduledEvaluation = 5,
        Accepted = 6,
        Rejected = 7,
        WaitingForFinalResponse = 8
    }
}
