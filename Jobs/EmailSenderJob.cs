﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Jobs
{
    public class EmailSenderJob : IJob
    {
        private readonly IServiceProvider _provider;

        public EmailSenderJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {

            using (var scope = _provider.CreateScope())
            {
                var _logger = scope.ServiceProvider.GetService<ILogger<EmailSenderJob>>();
                var service = scope.ServiceProvider.GetService<IUserRepository>();
                var mailService = scope.ServiceProvider.GetService<IMailServices>();
                var submissionService = scope.ServiceProvider.GetService<ISubmissionRepo>();
                var taskService = scope.ServiceProvider.GetService<ITaskRepository>();
                var hiringService = scope.ServiceProvider.GetService<IHiringRepo>();
                var interviewService = scope.ServiceProvider.GetService<IInterviewRepo>();

                var removalTasks = await taskService.GetAllRemovalTasksAsync();

                var userFound = new User();
                var submissionsFound = new List<Submission>();
                var hiringProcessFound = new HiringProcess();
                var taskFound = new AutoTask();

                foreach (var removalTask in removalTasks)
                {
                    userFound = await service.GetUserById(removalTask.UserId);

                    var currentTime = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm");
                    _logger.LogInformation("Current time" + currentTime);
                    _logger.LogInformation("Scheduled operation" + removalTask.ScheduledOperation);

                    if (removalTask.ScheduledOperation.Equals(currentTime))
                    {
                        var allSubmissions = await submissionService.GetAllSubmissionsOfficialAsync();
                        submissionsFound = submissionsFound.Where(s => s.CandidateId == userFound.Id).ToList();

                        foreach (var submissionFound in submissionsFound)
                        {
                            hiringProcessFound = await hiringService.GetHiringProcessById(submissionFound.HiringId);

                            mailService.SendCandidateDeleteConfirmation(userFound);
                            hiringService.RemoveHiringProcess(hiringProcessFound);
                            await hiringService.SaveChangesAsync();

                            submissionService.RemoveSubmission(submissionFound);
                            await submissionService.SaveChangesAsync();

                            service.RemoveUser(userFound);
                            await service.SaveChangesAsync();

                            taskService.RemoveTask(removalTask);
                            await taskService.SaveChangesAsync();
                        }
                    }
                }
            }
        }

    }
}
