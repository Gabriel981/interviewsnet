﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface IHiringRepo
    {
        Task<IEnumerable<HiringViewModel>> GetAllHiringProcesses();

        Task<HiringViewModel> GetHiringProcessByIdDetailed(int id);

        Task<HiringProcess> GetHiringProcessById(int? id);

        Task<HiringProcess> GetHiringProcessBySubmissionId(int id);

        Task<HiringProcess> GetHiringProcessByInterviewId(int id);

        Task addHiringProcess(HiringProcess hiringProcess);

        void UpdateHiringProcess(HiringProcess hiringProcess);

        void RemoveHiringProcess(HiringProcess hiringProcess);

        Task<bool> SaveChangesAsync();
        Task<bool> SaveChangesRegisterAsync();
    }
}
