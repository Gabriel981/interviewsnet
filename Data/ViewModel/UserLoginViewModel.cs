﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class UserLoginViewModel
    {
        [Required]
        [EmailAddress]
        [MinLength(8)]
        [MaxLength(30)]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Role { get; set; }
    }
}
