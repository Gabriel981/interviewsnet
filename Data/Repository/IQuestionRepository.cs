﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public interface IQuestionRepository
    {
        Task<IEnumerable<Question>> GetAllQuestionsAsync();
        Task<Question> GetQuestionByIdAsync(int id);
        Task<Question> GetQuestionByParamsAsync(string createdById, string createdAt, int positionId);
        Task<IEnumerable<Question>> GetQuestionsByQuizAsync(int quizId);
        Task<Question> GetLastQuestionAsync();
        Task<IEnumerable<Answer>> GetAnswersByQuestionIdAsync(int questionId);
        Task<IEnumerable<QuizQuestion>> GetAllOccurencesByQuestionIdAsync(int questionId);
        Task AddQuestionAsync(Question question);
        Task AddQuestionToQuizAsync(QuizQuestion quizQuestion);
        void UpdateQuestion(Question question);
        void RemoveQuestionFromQuiz(QuizQuestion quizQuestion);
        void RemoveQuestion(Question question);
        Task<bool> SaveChangesAsync();
    }
}
