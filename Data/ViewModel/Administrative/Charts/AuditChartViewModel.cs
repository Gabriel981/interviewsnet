﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class AuditChartViewModel
    {
        public string Date { get; set; }
        public int NoOfActions { get; set; }
    }
}
