﻿using System;

namespace InterviewsManagementNET.Data
{
    public class InterviewWriteDTO
    {
        public DateTime InterviewDate { get; set; }
        public string CommunicationChannel { get; set; }
        public int QuizId { get; set; }
        public int HiringId { get; set; }
        public string CreatedById { get; set; }
    }
}
