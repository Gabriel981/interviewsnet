﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;

namespace InterviewsManagementNET.Data
{
    public interface IApplicationRepository
    {
        IEnumerable<Position> GetAllAvailablePositions();
        Position GetPositionById(int id);
        Position GetPositionByName(string positionName);
        IEnumerable<SubmissionViewModel> GetUserSubmissions(string id);
    }
}
