﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.ViewModel.Roles;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/roles")]
    public class RoleController : ControllerBase
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly IPositionRepo _positionRepository;

        public RoleController(
            RoleManager<IdentityRole> roleManager, 
            UserManager<User> userManager,
            IPositionRepo positionRepository)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _positionRepository = positionRepository;
        }

        [HttpGet]
        [Route("all")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllRolesAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            var roles = await _roleManager.Roles.ToListAsync();
            if (roles == null) return BadRequest();

            return Ok(roles);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllUsersByRoleIdAsync(string id)
        {
            if (!ModelState.IsValid) return BadRequest();

            var role = await _roleManager.FindByIdAsync(id);
            if (role == null) return BadRequest();

            var users = await _userManager.GetUsersInRoleAsync(role.Name);
            if (users == null) return NotFound();

            return Ok(users);
        }

        [HttpPost("add/{role}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> CreateNewRoleAsync(string role)
        {
            if (!ModelState.IsValid) return BadRequest();

            var roleExists = await _roleManager.RoleExistsAsync(role);
            if (roleExists) return BadRequest("The specified role already exists in database. Try with a new one");

            await _roleManager.CreateAsync(new IdentityRole(role));

            return Ok("A new role was succesfully added in database");
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveRoleAsync(string id)
        {
            var roleExists = await _roleManager.FindByIdAsync(id);
            if (roleExists == null) return BadRequest();

            var hasUsers = await _userManager.GetUsersInRoleAsync(roleExists.Name);
            if(hasUsers == null || hasUsers.Count == 0)
            {
                await _roleManager.DeleteAsync(roleExists);
                return Ok("The role was succesfully removed from database");
            }
            
            if(hasUsers.Count > 0)
            {
                foreach(var user in hasUsers)
                {
                    await _userManager.RemoveFromRoleAsync(user, roleExists.Name);
                    var temporaryRole = await _roleManager.FindByNameAsync("Not retrieved");

                    if(temporaryRole == null)
                    {
                       await _roleManager.CreateAsync(new IdentityRole("Not retrieved"));
                       temporaryRole = await _roleManager.FindByNameAsync("Not retrieved");
                    }

                    await _userManager.AddToRoleAsync(user, temporaryRole.Name);
                }

                return Ok("The role was succesfully removed from database");
            }

            return BadRequest("Cannot do this operation. An error occured");
        }

        [HttpPost("assign")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> AssignToRoleAsync([FromBody] AssignRoleViewModel assignViewModel)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await _userManager.FindByIdAsync(assignViewModel.UserId);
            if (user == null) return BadRequest();

            var role = await _roleManager.FindByIdAsync(assignViewModel.RoleId);
            if (role == null) return BadRequest();

            var crtRoles = await _userManager.GetRolesAsync(user);

            if(crtRoles.Count > 0 && crtRoles.Count < 2)
            {
                string roleName = crtRoles.FirstOrDefault();
                if (roleName == role.Name) return BadRequest("Cannot assign a user to the same role");

                await _userManager.RemoveFromRoleAsync(user, roleName);
                await _userManager.AddToRoleAsync(user, role.Name);

                return Ok($"The user was succesfully assigned to the {role.Name} role");
            }

            if(crtRoles.Count >= 2)
            {
                foreach (var crtRole in crtRoles)
                {
                    if (crtRole == role.Name)
                    {
                        return BadRequest("Cannot assign a user to the same role");
                    }
                }

                await _userManager.AddToRoleAsync(user, role.Name);
                return Ok($"The user was succesfully assigned to the {role.Name} role");
            }

            return BadRequest("Something wrong occured during execution. Try again");
        }

        [HttpGet("positions")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetPositionsAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            return Ok(await _positionRepository.GetAllPositionsAsync());
        }

        [HttpDelete("remove/{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveUserInRoleAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return BadRequest();

            var userCrtRole = await _userManager.GetRolesAsync(user);
            if (userCrtRole == null) return BadRequest();

            if(userCrtRole.Count > 0 && userCrtRole.Count < 2)
            {
                var crtRole = userCrtRole.FirstOrDefault();
                await _userManager.RemoveFromRoleAsync(user, crtRole);

                var unretrievedRole = await _roleManager.FindByNameAsync("Not retrieved");

                await _userManager.AddToRoleAsync(user, unretrievedRole.Name);

                return Ok($"The user was succesfully removed from the role {crtRole}");
            }

            if(userCrtRole.Count >= 2)
            {
                foreach(var role in userCrtRole)
                {
                    await _userManager.RemoveFromRoleAsync(user, role);
                }

                var unretrievedRole = await _roleManager.FindByNameAsync("Not retrieved");
                await _userManager.AddToRoleAsync(user, unretrievedRole.Name);

                return Ok($"The user was succesfully removed from the roles");
            }

            return BadRequest("Something wrong occured during execution. Try again");
        }
    }
}
