﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class RejectionAllocationViewModel
    {
        public string UserId { get; set; }
        
        [Required(ErrorMessage = "Cannot insert a new row into database without reason")]
        public string Reason { get; set; }
    }
}
