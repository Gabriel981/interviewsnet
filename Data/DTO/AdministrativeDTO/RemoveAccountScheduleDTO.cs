﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class RemoveAccountScheduleDTO
    {
        public string UserId { get; set; }
        public string Reason { get; set; }
    }
}
