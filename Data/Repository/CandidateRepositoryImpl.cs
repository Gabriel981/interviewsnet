﻿using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class CandidateRepositoryImpl : ICandidateRepository
    {
        private readonly SubmissionContext _context;

        public CandidateRepositoryImpl(SubmissionContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllCandidatesAsync()
        {
            IEnumerable<User> candidates = await _context.Users.Where(u => u.Role == "RegisteredCandidate")
                                                .ToListAsync();
            return candidates;
        }

        public async Task<User> GetCandidateByEmailAddressAsync(string EmailAddress)
        {
            if (EmailAddress == null)
            {
                throw new ArgumentNullException(nameof(EmailAddress));
            }
            else
            {
                User CandidateFound = await _context.Users.Where(u => u.Role == "RegisteredCandidate").FirstOrDefaultAsync(u => u.Email == EmailAddress);
                if (CandidateFound != null)
                {
                    return CandidateFound;
                }
            }

            return null;
        }

        public async Task<User> GetCandidateByIdAsync(string Id)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == Id);
        }
    }
}
