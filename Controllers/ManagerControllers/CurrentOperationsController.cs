﻿using AutoMapper;
using Firebase.Database;
using Firebase.Database.Query;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.ManagerControllers
{
    [ApiController]
    [Route("api/manager")]
    public class CurrentOperationsController : ControllerBase
    {
        public readonly IUserRepository _userRepository;
        public readonly INotificationService _notificationService;
        public readonly IPositionRepo _positionRepository;
        public readonly ISubmissionRepo _submissionRepository;
        public readonly IHiringRepo _hiringRepository;
        public readonly IMailServices _mailServices;
        public readonly IInterviewRepo _interviewRepository;
        public readonly SubmissionContext _submissionContext;
        public readonly IMapper _mapper;

        private static string Bucket = "interviewsnetproject.appspot.com";
        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public CurrentOperationsController(
            IUserRepository userRepository,
            INotificationService notificationService,
            IPositionRepo positionRepository,
            ISubmissionRepo submissionRepository,
            IHiringRepo hiringRepository,
            IMailServices mailServices,
            IInterviewRepo interviewRepository,
            SubmissionContext submissionContext,
            IMapper mapper
        )
        {
            this._userRepository = userRepository;
            this._notificationService = notificationService;
            this._positionRepository = positionRepository;
            this._submissionRepository = submissionRepository;
            this._hiringRepository = hiringRepository;
            this._mailServices = mailServices;
            this._interviewRepository = interviewRepository;
            this._submissionContext = submissionContext;
            this._mapper = mapper;
        }

        [HttpGet]
        [Route("proposal/{id}")]
        [Authorize(Roles = "Manager departament resurse umane, Administrator")]
        public async Task<ActionResult<CandidateRemovalDetailedViewModel>> GetRemovalProposalDataAsync(string id)
        {
            User manager = this._userRepository.GetUserByRole("Manager departament resurse umane");
            var managerId = manager.Id;

            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            var dbNotifications = await firebaseClient
                .Child("Notifications")
                .Child(managerId)
                .Child("ReceivedMessages")
                .OnceAsync<Notification>();

            Notification notificationFound = null;

            foreach (var notification in dbNotifications)
            {
                if (notification.Key == id)
                {
                    notificationFound = notification.Object;
                }
            }

            var adminFound = await _userRepository.GetUserById(notificationFound.SenderId);

            var adminCurrentPosition = await _positionRepository.GetPositionByIdAsync(adminFound.PositionId);

            adminFound.Position = adminCurrentPosition;

            var candidateFound = await _userRepository.GetUserById(notificationFound.CandidateId);

            var candidateRemovalDetails = new CandidateRemovalDetailedViewModel()
            {
                Admin = adminFound,
                Candidate = candidateFound,
                Message = notificationFound.Message,
                Timestamp = notificationFound.Timestamp
            };

            return Ok(candidateRemovalDetails);
        }

        [HttpGet]
        [Route("submissions")]
        [Authorize(Roles = "Manager departament resurse umane, Administrator")]
        public async Task<ActionResult<IEnumerable<SubmissionViewModel>>> GetCurrentCandidatesAsync()
        {
            var availableSubmissions = await _submissionRepository.GetAllSubmissionsAsync();
            if (availableSubmissions == null)
            {
                return NotFound("No submissions available yet!");
            }

            return Ok(availableSubmissions);
        }

        [HttpPost]
        [Route("approve/{id}/{id2}")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<SubmissionViewModel>> ApproveCandidateAsync(int id, string id2)
        {
            Submission submissionFound = await _submissionRepository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
            {
                return NotFound("Submission not found!");
            }

            HiringProcess hiringFound = await _hiringRepository.GetHiringProcessById(submissionFound.HiringId);
            if (hiringFound == null)
            {
                return NotFound("Hiring process not found!");
            }

            User candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
            {
                return NotFound("No candidate found for the submission with id " + submissionFound.SubmissionId);
            }

            User managerFound = this._userRepository.GetUserByRole("Manager departament resurse umane");
            if (managerFound == null)
            {
                return NotFound("No manager found in the database");
            }

            hiringFound.CandidateSituation = CandidateSituation.Accepted;

            this._hiringRepository.UpdateHiringProcess(hiringFound);
            await _hiringRepository.SaveChangesAsync();

            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
            if (positionFound == null)
            {
                return NotFound("No position found for id " + positionFound.PositionId);
            }

            await _notificationService.PostAcceptedNotificationData(submissionFound.CandidateId, id2, positionFound.PositionName);

            this._mailServices.SendCandidateApproval(candidateFound, managerFound, positionFound.PositionName);

            positionFound.NoVacantPositions -= 1;

            this._positionRepository.PositionUpdate(positionFound);

            await _positionRepository.SaveChangesAsync();

            var submissionViewModel = _mapper.Map<SubmissionViewModel>(submissionFound);

            return Ok(submissionViewModel);
        }

        [HttpPost]
        [Route("reject/{id}/{id2}")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<SubmissionViewModel>> RejectCandidateAsync(int id, string id2)
        {
            Submission submissionFound = await _submissionRepository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
            {
                return NotFound("Submission not found!");
            }

            HiringProcess hiringFound = await _hiringRepository.GetHiringProcessById(submissionFound.HiringId);
            if (hiringFound == null)
            {
                return NotFound("Hiring process not found!");
            }

            hiringFound.CandidateSituation = CandidateSituation.Rejected;

            User candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
            if (candidateFound == null)
            {
                return NotFound("No candidate found with id " + submissionFound.CandidateId);
            }


            User managerFound = this._userRepository.GetUserByRole("Manager departament resurse umane");
            if (managerFound == null)
            {
                return NotFound("No manager found in the database");
            }

            this._hiringRepository.UpdateHiringProcess(hiringFound);
            await _hiringRepository.SaveChangesAsync();

            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

            await _notificationService.PostRejectedNotificationData(submissionFound.CandidateId, id2, positionFound.PositionName);

            this._mailServices.SendCandidateRejection(candidateFound, managerFound, positionFound.PositionName);

            var submissionViewModel = _mapper.Map<SubmissionViewModel>(submissionFound);

            return Ok(submissionViewModel);
        }

        [HttpPost]
        [Route("requestChange/{id}/{id2}")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult> RequestStatusChangeAsync(int id, string id2)
        {
            Submission submissionFound = await _submissionRepository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
            {
                return NotFound("Submission not found!");
            }

            var adminPosition = await _positionRepository.GetPositionByNameAsync("Administrator");
            if (adminPosition == null)
            {
                return NotFound("No Administrator position found in database!");
            }

            var adminList = this._userRepository.GetAllUsers().Result.ToList().Where(u => u.Position.PositionId == adminPosition.PositionId);

            if (adminList == null)
            {
                return NotFound("No Administrator users found in database!");
            }

            foreach (var admin in adminList)
            {
               await _notificationService.PostRequestData(admin.Id, submissionFound.CandidateId, id2);
            }

            return Ok();
        }

        [HttpPost]
        [Route("requestRemoval/{id}/{id2}")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult> RequestCandidateRemovalAsync(int id, string id2)
        {
            Submission submissionFound = await _submissionRepository.GetSubmissionByIdAsync(id);
            if (submissionFound == null)
            {
                return NotFound("Submission not found!");
            }

            var adminPosition = await _positionRepository.GetPositionByNameAsync("Administrator");
            if (adminPosition == null)
            {
                return NotFound("No Administrator position found in database!");
            }

            var adminList = this._userRepository.GetAllUsers().Result.ToList().Where(u => u.Position.PositionId == adminPosition.PositionId);
            if (adminList == null)
            {
                return NotFound("No Administrator users found in database!");
            }

            foreach (var admin in adminList)
            {
                await _notificationService.PostRequestRemoval(admin.Id, submissionFound.CandidateId, id2);
            }

            return Ok();
        }

        [HttpGet]
        [Route("availableCandidates")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<Data.DTO.AdministrativeDTO.UserViewModel>> GetAllAvailableCandidatesAsync()
        {
            var submissions = await _submissionRepository.GetAllSubmissionsAsync();

            var candidatesFound = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

            var users = await _userRepository.GetAllUsers();

            foreach (var submission in submissions)
            {
                foreach (var user in users)
                {
                    if (submission.CandidateId == user.Id)
                    {
                        candidatesFound.Add(user);
                    }
                }
            }

            if (candidatesFound == null || candidatesFound.Count == 0)
            {
                return NotFound("No candidates found in the database");
            }

            return Ok(candidatesFound);
        }

        [HttpGet]
        [Route("availableInterviews")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane")]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> GetAvailableInterviewsAsync()
        {
            var availableInterviews = await _interviewRepository.GetAllInterviewsAsync();
            if (availableInterviews == null)
            {
                return NotFound("No interviews found in the database");
            }

            return Ok(availableInterviews);
        }

        [HttpPost]
        [Route("requestInterviewChange")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<InterviewRequestViewModel>> RequestChangeDetailsInterviewAsync(InterviewRequestViewModel interviewRequestViewModel)
        {
            var requesterFound = await _userRepository.GetUserById(interviewRequestViewModel.RequesterId);
            if (requesterFound == null)
            {
                return NotFound("No user found with id " + interviewRequestViewModel.RequesterId);
            }

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewRequestViewModel.InterviewId);
            if (interviewFound == null)
            {
                return NotFound("No interview found with id " + interviewRequestViewModel.InterviewId);
            }

            var receiverFound = await _userRepository.GetUserById(interviewRequestViewModel.ReceiverId);
            if (receiverFound == null)
            {
                return NotFound("No receiver found with id " + interviewRequestViewModel.ReceiverId);
            }

            await _notificationService.PostInterviewRequestChange(requesterFound.Id, receiverFound.Id, interviewFound, interviewRequestViewModel.Reason);


            return Ok(interviewRequestViewModel);
        }

        [HttpPost]
        [Route("requestInterviewRemoval")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<InterviewRequestViewModel>> RequestInterviewRemovalAsync(InterviewRequestViewModel interviewRequestViewModel)
        {
            var requesterFound = await _userRepository.GetUserById(interviewRequestViewModel.RequesterId);
            if (requesterFound == null)
            {
                return NotFound("No requester found in the database with id " + interviewRequestViewModel.RequesterId);
            }

            var receiverFound = await _userRepository.GetUserById(interviewRequestViewModel.ReceiverId);
            if (requesterFound == null)
            {
                return NotFound("No receiver found in the database with id " + interviewRequestViewModel.ReceiverId);
            }

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewRequestViewModel.InterviewId);
            if (interviewFound == null)
            {
                return NotFound("No interview found in the database with id " + interviewFound.InterviewId);
            }

            await _notificationService.PostInterviewRemovalRequest(requesterFound.Id, receiverFound.Id, interviewFound, interviewRequestViewModel.Reason);

            return Ok(interviewRequestViewModel);
        }


        [HttpPost]
        [Route("requestSpecialInterviewRemoval")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<InterviewRequestViewModel>> RequestSpecialInterviewRemovalAsync(InterviewRequestViewModel interviewRequestViewModel)
        {
            var requesterFound = await _userRepository.GetUserById(interviewRequestViewModel.RequesterId);
            if (requesterFound == null)
            {
                return NotFound("No requester found in the database with id " + interviewRequestViewModel.RequesterId);
            }

            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(interviewRequestViewModel.InterviewId);
            if (interviewFound == null)
            {
                return NotFound("No interview found in the database with id " + interviewRequestViewModel.InterviewId);
            }

            var positionAdministrator = await _positionRepository.GetPositionByNameAsync("Administrator");
            if (positionAdministrator == null)
            {
                return NotFound("No position found with name Administrator");
            }

            var users = await _userRepository.GetAllUsers();
            var administratorsFound = users.Where(u => u.Position.PositionId == positionAdministrator.PositionId);

            if (administratorsFound == null)
            {
                return NotFound("No users found in the database with role Administrator");
            }

            foreach (var administratorFound in administratorsFound)
            {
               await _notificationService.PostInterviewSpecialRemovalRequest(requesterFound.Id, administratorFound.Id, interviewFound);
            }

            return Ok(interviewRequestViewModel);
        }

        [HttpGet]
        [Route("participants/interview/{id}")]
        [Authorize(Roles = "Manager departament resurse umane")]
        public async Task<ActionResult<InterviewParticipantViewModel>> ShowAllInterviewParticipantsAsync(int id)
        {
            var interviewFound = await _interviewRepository.GetInterviewByIdAsync(id);
            if (interviewFound == null)
            {
                return NotFound("No interview found in the database with id " + id);
            }

            var participantsFound = await _interviewRepository.GetAllInterviewParticipantsAsync(interviewFound.InterviewId);
            if (participantsFound == null)
            {
                return NotFound("No participants found in the database for the interview with id " + interviewFound.InterviewId);
            }

            var participantsFoundsModel = participantsFound.Select(p => new InterviewParticipantViewModel()
            {
                FirstName = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).FirstName,
                LastName = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).LastName,
                ParticipantId = p.EmployeeId,
                Email = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).Email,
                PhoneNumber = _submissionContext.Users.FirstOrDefault(u => u.Id == p.EmployeeId).PhoneNumber,
                InterviewId = p.InterviewId,
                AttendanceDate = p.AttendedTime
            });

            return Ok(participantsFoundsModel);
        }


        [HttpGet]
        [Route("cv/{id}")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane")]
        public async Task<ActionResult<SubmissionViewModel>> GetSelectedCandidateCVAsync(string id)
        {
            var candidateFound = await _userRepository.GetUserById(id);
            if (candidateFound == null)
            {
                return NotFound("No candidate found in the database with id " + id);
            }



            var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(candidateFound.Id);
            if (submissionFound == null)
            {
                return NotFound("No submission found for the candidate with id " + candidateFound.Id);
            }

            return Ok(_mapper.Map<SubmissionViewModel>(submissionFound));
        }

        [HttpGet]
        [Route("export/submissions")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane")]
        public async Task<IActionResult> ExportExcelDocumentAsync()
        {
            string contentType = CurrentOperationsController.contentType;
            string fileName = "Submissions.xlsx";

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            var submissionList = await _submissionRepository.GetAllSubmissionsAsync();
            if (submissionList == null) return NotFound("No submission found in the database");

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage pck = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = pck.Workbook.Worksheets.Add("Submissions");

                int index = 1;
                worksheet.Cells["B1"].Value = "No.";
                worksheet.Cells["B1"].Style.Font.Bold = true;
                worksheet.Cells["B1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["C1"].Value = "Candidate complete name";
                worksheet.Cells["C1"].Style.Font.Bold = true;
                worksheet.Cells["C1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["D1"].Value = "Applied position";
                worksheet.Cells["D1"].Style.Font.Bold = true;
                worksheet.Cells["D1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["D1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["E1"].Value = "Submission date";
                worksheet.Cells["E1"].Style.Font.Bold = true;
                worksheet.Cells["E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["F1"].Value = "Current status";
                worksheet.Cells["F1"].Style.Font.Bold = true;
                worksheet.Cells["F1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["F1"].Style.Font.Color.SetColor(System.Drawing.Color.White);


                foreach (var submission in submissionList)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    if (candidateFound == null) return NotFound("No candidate found with id " + submission.CandidateId);

                    var hiringProcessFound = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                    if (hiringProcessFound == null) return NotFound("No hiring process found for submission with id " + submission.SubmissionId);


                    worksheet.Cells["B" + (index + 1)].Value = index;
                    worksheet.Cells["C" + (index + 1)].Value = candidateFound.FirstName + " " + candidateFound.LastName;
                    worksheet.Cells["D" + (index + 1)].Value = submission.AppliedPosition.PositionName;
                    worksheet.Cells["E" + (index + 1)].Value = submission.DateOfSubmission.ToString("dd/MM/yyyy HH:mm:ss");

                    worksheet.Cells["F" + (index + 1)].Value = hiringProcessFound.CandidateSituation.ToString();
                    switch (hiringProcessFound.CandidateSituation)
                    {
                        case CandidateSituation.Accepted:
                            worksheet.Cells["F" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["F" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                            break;
                        case CandidateSituation.Rejected:
                            worksheet.Cells["F" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["F" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red);
                            worksheet.Cells["F" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                        case CandidateSituation.UnderReview:
                            worksheet.Cells["F" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["F" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Aqua);
                            worksheet.Cells["F" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                        case CandidateSituation.Waiting:
                            worksheet.Cells["F" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["F" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.YellowGreen);
                            worksheet.Cells["F" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                            break;
                        case CandidateSituation.ReadyForInterview:
                            worksheet.Cells["F" + (index + 1)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            worksheet.Cells["F" + (index + 1)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Teal);
                            worksheet.Cells["F" + (index + 1)].Style.Font.Color.SetColor(System.Drawing.Color.White);
                            break;
                    }

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                pck.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, fileName);
        }

        [HttpGet]
        [Route("export/interviews")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane")]
        public async Task<IActionResult> ExportInterviewDataAsync()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var interviewsAvailable = await _interviewRepository.GetAllInterviewsAsync();
            if (interviewsAvailable == null) return NotFound("No interviews available in the database");

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Interviews");

                int index = 1;

                worksheet.Cells["B1"].Value = "Candidate complete name";
                worksheet.Cells["B1"].Style.Font.Bold = true;
                worksheet.Cells["B1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["C1"].Value = "Applied position";
                worksheet.Cells["C1"].Style.Font.Bold = true;
                worksheet.Cells["C1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["D1"].Value = "Scheduled date";
                worksheet.Cells["D1"].Style.Font.Bold = true;
                worksheet.Cells["D1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["D1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["E1"].Value = "Participants";
                worksheet.Cells["E1"].Style.Font.Bold = true;
                worksheet.Cells["E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                foreach (var interview in interviewsAvailable)
                {
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                    if (hiringProcessFound == null) return NotFound("No hiring process found in the database with for the interview with id " + interview.InterviewId);

                    var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                    if (submissionFound == null) return NotFound("No submission found in the databasre with id " + hiringProcessFound.SubmissionId);

                    var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                    if (positionFound == null) return NotFound("No position found in the database with id " + submissionFound.PositionId);

                    var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                    if (candidateFound == null) return NotFound("No candidate found in the database with id " + submissionFound.CandidateId);

                    worksheet.Cells["B" + (index + 1)].Value = candidateFound.FirstName + " " + candidateFound.LastName;
                    worksheet.Cells["C" + (index + 1)].Value = positionFound.PositionName;
                    worksheet.Cells["D" + (index + 1)].Value = interview.InterviewDate.ToString("dd/MM/yyyy HH:mm:ss");

                    var interviewParticipants = await _interviewRepository.GetAllInterviewParticipantsAsync(interview.InterviewId);
                    if (interviewParticipants == null) return NotFound("No participants found for the interview with id " + interview.InterviewId);

                    var participantsString = "";

                    foreach (var interviewParticipant in interviewParticipants)
                    {
                        var participantFound = await _userRepository.GetUserById(interviewParticipant.EmployeeId);
                        if (participantFound == null) return NotFound("No participant found with id " + interviewParticipant.EmployeeId);

                        participantsString += $"{participantFound.FirstName} {participantFound.LastName}; ";
                    }

                    worksheet.Cells["E" + (index + 1)].Value = participantsString;

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "AvailableInterviews.xlsx");
        }
    }
}
