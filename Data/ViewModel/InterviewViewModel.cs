﻿using InterviewsManagementNET.Models;
using System;

namespace InterviewsManagementNET.Data
{
    public class InterviewViewModel
    {
        public int InterviewId { get; set; }
        public DateTime InterviewDate { get; set; }
        public string CommunicationChannel { get; set; }
        public string CandidateSituation { get; set; }
        public bool QuizAvailable { get; set; }
        public Quiz Quiz { get; set; }
        public HiringProcess HiringProcess { get; set; }

        public string CandidateFirstName { get; set; }

        public string CandidateLastName { get; set; }

        public string PositionName { get; set; }

        public Submission Submission { get; set; }
        public User CreatedBy { get; set; }
        public User LastModifiedBy { get; set; }
    }
}
