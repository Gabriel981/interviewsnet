﻿using InterviewsManagementNET.Data.Repository.Administrative.Interfaces;
using InterviewsManagementNET.Data.ViewModel.Administrative;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/logs")]
    public class AuditTrailController : ControllerBase
    {
        private readonly IAuditTrailRepository _auditTrailRepository;

        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public AuditTrailController(IAuditTrailRepository auditTrailRepository)
        {
            _auditTrailRepository = auditTrailRepository;
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllAuditTrailsAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            var auditTrails = await _auditTrailRepository.GetAllAuditTrailsAsync();
            if (auditTrails == null) return NotFound();

            return Ok(auditTrails);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAuditTrailByIdAsync(int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            var auditTrail = await _auditTrailRepository.GetAuditTrailByIdAsync(id);
            if (auditTrail == null) return NotFound();

            return Ok(auditTrail);
        }

        [HttpGet("user/{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAuditTrailsByUserIdAsync(string id)
        {
            if (!ModelState.IsValid) return BadRequest();

            var auditTrail = await _auditTrailRepository.GetAllAuditTrailsByUserIdAsync(id);
            if (auditTrail == null) return NotFound();

            return Ok(auditTrail);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveAuditTrailAsync(int id)
        {
            if (!ModelState.IsValid) return BadRequest();

            var auditTrail = await _auditTrailRepository.GetAuditTrailByIdAsync(id);
            if (auditTrail == null) return NotFound();

            _auditTrailRepository.RemoveAuditTrail(auditTrail);
            await _auditTrailRepository.SaveChangesAsync();

            return Ok("The audit log was succesfully removed from the database");
        }

        [HttpGet("export")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GenerateExcelFileAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            var auditTrailLogs = await _auditTrailRepository.GetAllAuditTrailsAsync();
            if (auditTrailLogs == null) return BadRequest();

            MemoryStream memoryStream = new MemoryStream();
            using(ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("AuditLogs");
                int index = 1;

                worksheet.Cells["B1"].Value = "User Id";
                worksheet.Cells["C1"].Value = "Action type";
                worksheet.Cells["D1"].Value = "Action taken on table";
                worksheet.Cells["E1"].Value = "Action taken on";
                worksheet.Cells["F1"].Value = "Old values";
                worksheet.Cells["G1"].Value = "New values";
                worksheet.Cells["H1"].Value = "Affected columns";
                worksheet.Cells["I1"].Value = "Primary key";

                worksheet.Cells["A1:I1"].Style.Font.Bold = true;
                worksheet.Cells["A1:I1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["A1:I1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["A1:I1"].Style.Font.Color.SetColor(System.Drawing.Color.White);
                worksheet.Cells["E1"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                foreach(var auditLog in auditTrailLogs)
                {
                    worksheet.Cells["B" + (index + 1)].Value = auditLog.UserId;
                    worksheet.Cells["C" + (index + 1)].Value = auditLog.Type;
                    worksheet.Cells["D" + (index + 1)].Value = auditLog.TableName;
                    worksheet.Cells["E" + (index + 1)].Value = auditLog.DateTime;
                    worksheet.Cells["F" + (index + 1)].Value = auditLog.OldValues;
                    worksheet.Cells["G" + (index + 1)].Value = auditLog.NewValues;
                    worksheet.Cells["H" + (index + 1)].Value = auditLog.AffectedColumns;
                    worksheet.Cells["I" + (index + 1)].Value = auditLog.PrimaryKey;

                    index ++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "AuditTrailLogs.xlsx");
        }

        [HttpGet("chart")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetDataChartAsync()
        {
            if (!ModelState.IsValid) return BadRequest();
            var crtDate = DateTime.Now;
            var sevenDaysAgo = DateTime.Now.AddDays(-7);

            var auditLogs = await _auditTrailRepository.GetAllAuditTrailsAsync();
            if (auditLogs == null) return BadRequest();
            var auditLogsLastSevenDays = auditLogs.Where(a => a.DateTime.Date >= sevenDaysAgo && a.DateTime.Date < crtDate);

            List<AuditChartViewModel> auditChartData = new List<AuditChartViewModel>();

            var crtDateComparison = auditLogsLastSevenDays.OrderBy(a => a.DateTime).FirstOrDefault().DateTime;
            
            Dictionary<string, int> chartMap = new Dictionary<string, int>();

            bool keyExists = false;

            foreach (var auditDate in auditLogsLastSevenDays)
            {
                var date = auditDate.DateTime.ToShortDateString();
                if (chartMap.ContainsKey(date)) {
                    keyExists = true;
                    chartMap[date] += 1;
                } else
                {
                    keyExists = false;
                    chartMap.Add(date, 1);
                }
            }

            foreach(KeyValuePair<string, int> entry in chartMap)
            {
                auditChartData.Add(new AuditChartViewModel
                {
                    Date = entry.Key,
                    NoOfActions = entry.Value
                });
            }
            

            return Ok(auditChartData);
        }
    }
}
