﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public interface ITaskRepository
    {
        Task<IEnumerable<AutoTask>> GetAllRemovalTasksAsync();
        Task<AutoTask> GetRemovalTaskByUserIdAsync(string id);
        Task AddTaskAsync(AutoTask task);
        void RemoveTask(AutoTask task);
        Task<bool> SaveChangesAsync();
    }
}
