﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface ISubmissionRepo
    {
        Task<IEnumerable<SubmissionViewModel>> GetAllSubmissionsAsync();
        Task<IEnumerable<Submission>> GetAllSubmissionsOfficialAsync();
        Task<SubmissionViewModel> GetSubmissionByIdDetailedAsync(int id);
        Task<IEnumerable<User>> GetAllCandidatesAsync();
        Task<IEnumerable<SubmissionViewModel>> GetSubmissionsDetailsAsync();
        Task<Submission> GetSubmissionByIdAsync(int id);
        Task<Submission> GetSubmissionByCandidateIdAsync(string id);
        Task AddSubmissionAsync(Submission submission);
        void SubmissionUpdate(Submission submission);
        void RemoveSubmission(Submission submission);
        Task<bool> SaveChangesAsync();

        Task<Submission> GetLastSubmission();
    }
}
