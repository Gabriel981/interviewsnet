﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class DepartmentWriteDTO
    {
        [Required(ErrorMessage = "Department name is required")]
        [StringLength(30, ErrorMessage = "Department name length must be between 2 and 30", MinimumLength = 2)]
        [Display(Name = "Department name")]
        public string DepartmentName { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [StringLength(350, ErrorMessage = "Description must be between 8 and 350 characters", MinimumLength = 8)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Department current status is required")]
        public bool Active { get; set; }

        public int CurrentNumberOfPositions { get; set; }
    }
}
