﻿using AutoMapper;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/answers")]
    public class AnswerController : ControllerBase
    {
        private readonly IAnswerRepository answerRepository;
        private readonly IQuestionRepository questionRepository;
        private readonly IMapper mapper;

        public AnswerController(
            IAnswerRepository answerRepository,
            IQuestionRepository questionRepository,
            IMapper mapper)
        {
            this.answerRepository = answerRepository;
            this.questionRepository = questionRepository;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet]
        public ActionResult<IEnumerable<AnswerReadDTO>> GetAllAnswers()
        {
            var answersList = this.answerRepository.GetAllAnswers();
            if (answersList == null)
                return NotFound("No answers was found in the database");

            return Ok(mapper.Map<IEnumerable<AnswerReadDTO>>(answersList));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("{id}")]
        public ActionResult<AnswerReadDTO> GetAnswerById(int id)
        {
            var answerFound = this.answerRepository.GetAnswerById(id);
            if (answerFound == null)
                return NotFound("No answer was found in the database with id " + id);

            return Ok(mapper.Map<AnswerReadDTO>(answerFound));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPost("post")]
        public async System.Threading.Tasks.Task<ActionResult<string>> AddAnswerAsync(AnswerWriteDTO answerWriteDTO)
        {
            var questionFound = await questionRepository.GetQuestionByIdAsync(answerWriteDTO.QuestionId);
            if (questionFound == null)
                return NotFound("No question was found in the database with id " + answerWriteDTO.QuestionId);

            var currentTimePost = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm:ss");

            Answer answer = new Answer()
            {
                AnswerBody = answerWriteDTO.AnswerBody,
                CreatedById = answerWriteDTO.CreatedById,
                QuestionId = questionFound.QuestionId,
                CreatedAt = currentTimePost
            };

            this.answerRepository.AddAnswer(answer);
            this.answerRepository.SaveChanges();

            return Ok("The answer was succesfully submitted");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPost("post/answers")]
        public async System.Threading.Tasks.Task<ActionResult<string>> AddAnswersAsync(List<AnswerWriteDTO> answersWriteDTO)
        {
            foreach (var answerWriteDTO in answersWriteDTO)
            {
                var questionFound = await questionRepository.GetQuestionByIdAsync(answerWriteDTO.QuestionId);
                if (questionFound == null)
                    return NotFound("No question was found in the database with id " + answerWriteDTO.QuestionId);

                var currentTimePost = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm:ss");

                Answer answer = new Answer()
                {
                    AnswerBody = answerWriteDTO.AnswerBody,
                    CreatedById = answerWriteDTO.CreatedById,
                    QuestionId = questionFound.QuestionId,
                    CreatedAt = currentTimePost
                };

                this.answerRepository.AddAnswer(answer);
                this.answerRepository.SaveChanges();
            }

            return Ok("The provided answers was succesfully submitted to database");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpDelete("{id}")]
        public ActionResult<string> RemoveAnswer(int id)
        {
            var answerFound = this.answerRepository.GetAnswerById(id);
            if (answerFound == null)
                return NotFound("No answer was found in the database with id " + id);

            this.answerRepository.RemoveAnswer(answerFound);
            this.answerRepository.SaveChanges();

            return Ok("The selected answer was succesfully removed from the database");
        }
    }
}
