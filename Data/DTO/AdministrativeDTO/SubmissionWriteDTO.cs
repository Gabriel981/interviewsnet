﻿namespace InterviewsManagementNET.Data
{
    public class SubmissionWriteDTO
    {
        /*        public DateTime DateOfSubmission { get; set; }

                [MaxLength(60, ErrorMessage = "The mentions can't be more than 200 characters")]
                [MinLength(0)]
                public string Mentions { get; set; }

                public int PositionId { get; set; }
                public int CandidateId { get; set; }*/

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public int PositionId { get; set; }
    }
}
