﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface IPositionRepo
    {
        Task<IEnumerable<Position>> GetAllPositionsAsync();

        Task<Position> GetPositionByIdAsync(int Id);

        Task AddPositionAsync(Position position);

        Task<bool> SaveChangesAsync();

        void PositionUpdate(Position position);

        void RemovePosition(Position position);

        Task<Position> GetPositionByNameAsync(string positionName);
        Task<IEnumerable<Position>> GetPositionsByDepartmentId(int departmentId);
    }
}
