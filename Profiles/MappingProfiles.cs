﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO.DepartmentDTO;
using InterviewsManagementNET.Data.DTO.ClientDTO;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Profiles
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {

            //Candidate mappers
            CreateMap<User, UserReadDTO>(); //mapping for get method
            CreateMap<UserWriteDTO, User>(); //mapping for post method
            /*            CreateMap<CandidateUpdateDTO, Candidate>(); //mapping for put method
                        CreateMap<Candidate, CandidateUpdateDTO>(); //mapping for patchA method*/

            //Position mappers
            CreateMap<Position, PositionReadDTO>(); //mapping for get method
            CreateMap<PositionWriteDTO, Position>(); //mapping for post method
            CreateMap<PositionUpdateDTO, Position>(); //mapping for put method
            CreateMap<Position, PositionUpdateDTO>(); //mapping for patch method

            //Submission mappers
            CreateMap<Submission, SubmissionViewModel>(); //mapping for get method
            CreateMap<SubmissionWriteDTO, Submission>(); //mapping for post method
            CreateMap<Submission, SubmissionReadDTO>(); //mapping for get method (simple)
            CreateMap<SubmissionUpdateDTO, Submission>(); //mapping for put method
            CreateMap<SubmissionViewModel, Submission>();

            //Hiring mappers
            CreateMap<HiringProcess, HiringViewModel>();

            CreateMap<Interview, InterviewReadDTO>();
            CreateMap<Interview, InterviewViewModel>();
            CreateMap<InterviewWriteDTO, Interview>();
            CreateMap<InterviewUpdateDTO, Interview>();
            CreateMap<HiringViewModel, HiringProcess>();

            //Client side
            CreateMap<Position, ApplicationPositionReadDTO>();
            CreateMap<User, Data.DTO.AdministrativeDTO.UserViewModel>();
            CreateMap<User, UserViewModelCandidate>();

            CreateMap<InterviewParticipant, InterviewParticipantViewModel>();
            CreateMap<EmployeeParticipantViewModel, User>();

            CreateMap<Quiz, QuizReadDTO>();
            CreateMap<Answer, AnswerReadDTO>();

            CreateMap<Question, QuestionReadDTO>();

            CreateMap<DepartmentWriteDTO, Department>();
            CreateMap<Department, DepartmentReadDTO>();
        }
    }
}
