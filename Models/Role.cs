﻿using Microsoft.AspNetCore.Identity;

namespace InterviewsManagementNET.Models
{
    public class Role : IdentityRole
    {

        public Role() : base() { }

        public Role(string name) : base(name)
        {

        }
        /*public const string Admin = "Admin";
        public const string Employee = "Employee";
        public const string Candidate = "Candidate";
        public const string Recruiter = "Recruiter";*/
    }
}
