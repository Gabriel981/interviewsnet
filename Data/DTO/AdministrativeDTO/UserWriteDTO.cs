﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class UserWriteDTO
    {
        [Required]
        [MinLength(2, ErrorMessage = "The first name cannot be less than 2 characters")]
        [MaxLength(30, ErrorMessage = "The first name cannot be more than 30 characters")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "The last name cannot be less than 2 characters")]
        [MaxLength(30, ErrorMessage = "The last name cannot be more than 30 characters")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "The address details cannot be less than 5 characters")]
        [MaxLength(100, ErrorMessage = "The address cannot be more than 100 characters")]
        public string Address { get; set; }

        public int PositionId { get; set; }

        public string Role { get; set; }

        public string Mentions { get; set; }

        [Required]
        public string SecurityQuestion { get; set; }

        [Required]
        public string SecurityQuestionAnswer { get; set; }
    }
}
