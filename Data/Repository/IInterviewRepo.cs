﻿using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface IInterviewRepo
    {
        Task<IEnumerable<InterviewViewModel>> GetAllInterviewsAsync();
        Task<IEnumerable<Interview>> ShowAllInterviewsAsync();

        Task<Interview> GetInterviewByIdAsync(int id);

        Task<IEnumerable<Interview>> GetAllInterviewsByDateAsync(DateTime date);

        Task<Interview> GetInterviewByHiringProcessAsync(HiringProcess hiringProcess);

        Task<IEnumerable<InterviewParticipant>> GetAllInterviewsParticipantsAsync();

        Task<IEnumerable<InterviewParticipant>> GetAllInterviewParticipantsAsync(int id);

        Task<InterviewParticipant> GetInterviewParticipantByIdsAsync(int interviewId, string participantId);

        Task AddInterviewParticipantAsync(InterviewParticipant interviewParticipant);

        Task AddInterviewAsync(Interview interview);

        void UpdateInterview(Interview interview);

        Task removeInterviewAsync(Interview interview);

        void removeParticipantFromInterview(InterviewParticipant interviewParticipant);

        Task<bool> SaveChangesAsync();

    }
}
