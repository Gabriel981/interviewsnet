﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Linq;

namespace InterviewsManagementNET.Data.Repository
{
    public class AnswerRepositoryImpl : IAnswerRepository
    {
        private readonly SubmissionContext context;

        public AnswerRepositoryImpl(SubmissionContext context)
        {
            this.context = context;
        }

        public void AddAnswer(Answer answer)
        {
            context.Answers.Add(answer);
        }

        public IEnumerable<Answer> GetAllAnswers()
        {
            return context.Answers.ToList();
        }

        public Answer GetAnswerById(int id)
        {
            return context.Answers.FirstOrDefault(a => a.AnswerId == id);
        }

        public IEnumerable<Answer> GetAnswersByQuestionId(int questionId)
        {
            return context.Answers.ToList().Where(a => a.QuestionId == questionId);
        }

        public void RemoveAnswer(Answer answer)
        {
            context.Answers.Remove(answer);
        }

        public bool SaveChanges()
        {
            return (context.SaveChanges() > 0);
        }
    }
}
