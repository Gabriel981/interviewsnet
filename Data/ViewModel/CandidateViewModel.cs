﻿using System;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class CandidateViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompleteName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PositionName { get; set; }
        public DateTime SubmissionDate { get; set; }
        public string CandidateSituation { get; set; }
    }
}
