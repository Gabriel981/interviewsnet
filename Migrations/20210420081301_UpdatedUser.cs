﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InterviewsManagementNET.Migrations
{
    public partial class UpdatedUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnabledTwoFactorGoogle",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnabledTwoFactorGoogle",
                table: "AspNetUsers");
        }
    }
}
