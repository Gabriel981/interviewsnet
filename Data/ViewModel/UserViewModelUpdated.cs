﻿using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class UserViewModelUpdated
    {
        public User AppUser { get; set; }
        public string Role { get; set; }
        public string refreshToken { get; set; }
        public bool Is2StepVerificationRequired { get; set; }
        public string Provider { get; set; }
    }
}
