﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public class QuestionRepositoryImpl : IQuestionRepository
    {
        private readonly SubmissionContext _submissionContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public QuestionRepositoryImpl(SubmissionContext submissionContext, IHttpContextAccessor httpContextAccessor)
        {
            _submissionContext = submissionContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task AddQuestionAsync(Question question)
        {
            await _submissionContext.Questions.AddAsync(question);
        }

        public async Task AddQuestionToQuizAsync(QuizQuestion quizQuestion)
        {
            await _submissionContext.QuestionQuizzes.AddAsync(quizQuestion);
        }

        public async Task<IEnumerable<QuizQuestion>> GetAllOccurencesByQuestionIdAsync(int questionId)
        {
            return await _submissionContext.QuestionQuizzes
                .Where(q => q.QuestionId == questionId)
                .ToListAsync();
        }

        public async Task<IEnumerable<Question>> GetAllQuestionsAsync()
        {
            return await _submissionContext.Questions.ToListAsync();
        }

        public async Task<IEnumerable<Answer>> GetAnswersByQuestionIdAsync(int questionId)
        {
            return await _submissionContext.Answers
                .Where(a => a.QuestionId == questionId)
                .ToListAsync();
        }

        public async Task<Question> GetLastQuestionAsync()
        {
            return await _submissionContext.Questions.OrderByDescending(q => q.QuestionId).FirstOrDefaultAsync();
        }

        public async Task<Question> GetQuestionByIdAsync(int id)
        {
            return await _submissionContext.Questions.FirstOrDefaultAsync(q => q.QuestionId == id);
        }

        public async Task<Question> GetQuestionByParamsAsync(string createdById, string createdAt, int positionId)
        {
            return await _submissionContext.Questions
                .Where(q => q.CreatedById == createdById && q.CreatedAt == createdAt && q.PositionId == positionId)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Question>> GetQuestionsByQuizAsync(int quizId)
        {
            var questionQuizes = await _submissionContext.QuestionQuizzes.Where(q => q.QuizId == quizId).ToListAsync();

            var list = new List<Question>();
            foreach (var questionQuiz in questionQuizes)
            {
                var questionFound = await _submissionContext.Questions.FirstOrDefaultAsync(q => q.QuestionId == questionQuiz.QuestionId);
                list.Add(questionFound);
            }

            return list;
        }

        public void RemoveQuestion(Question question)
        {
            _submissionContext.Remove(question);
        }

        public void RemoveQuestionFromQuiz(QuizQuestion quizQuestion)
        {
            _submissionContext.Remove(quizQuestion);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _submissionContext.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }

        public void UpdateQuestion(Question question)
        {
            // No needs to provide implementation
        }
    }
}
