﻿using System;

namespace InterviewsManagementNET.Jobs
{
    public class EmailJobSchedule
    {
        public EmailJobSchedule(Type jobType, string cronExpression)
        {
            JobType = jobType;
            CronExpression = cronExpression;
        }

        public Type JobType { get; set; }
        public string CronExpression { get; set; }

    }
}
