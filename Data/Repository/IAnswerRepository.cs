﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;

namespace InterviewsManagementNET.Data.Repository
{
    public interface IAnswerRepository
    {
        IEnumerable<Answer> GetAllAnswers();
        Answer GetAnswerById(int id);
        IEnumerable<Answer> GetAnswersByQuestionId(int questionId);
        void AddAnswer(Answer answer);
        void RemoveAnswer(Answer answer);
        bool SaveChanges();
    }
}
