﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace InterviewsManagementNET.Controllers
{
    [Route("candidates")]
    [ApiController]
    public class CandidatesController : ControllerBase
    {
        private readonly ICandidateRepository _repository;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly ISubmissionRepo _submissionRepository;
        private readonly IPositionRepo _positionRepository;
        private readonly IHiringRepo _hiringRepository;

        public CandidatesController(
            ICandidateRepository repository, 
            IMapper mapper, 
            UserManager<User> userManager, 
            ISubmissionRepo submissionRepository, 
            IPositionRepo positionRepository,
            IHiringRepo hiringRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _userManager = userManager;
            _submissionRepository = submissionRepository;
            _positionRepository = positionRepository;
            _hiringRepository = hiringRepository;
        }

        [HttpGet]
        [Authorize(Roles = "Recruiter, Administrator")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<CandidateViewModel>>> GetAllCandidatesAsync()
        {
            var candidatesFound = await _userManager.GetUsersInRoleAsync("Candidate registered");

            IList<CandidateViewModel> candidates = new List<CandidateViewModel>();

            foreach(var candidate in candidatesFound)
            {
                var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(candidate.Id);
                if(submissionFound != null)
                {
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessBySubmissionId(submissionFound.SubmissionId);

                    candidates.Add(new CandidateViewModel
                    {
                        FirstName = candidate.FirstName,
                        LastName = candidate.LastName,
                        Address = candidate.Address,
                        CompleteName = candidate.FirstName + " " + candidate.LastName,
                        DateOfBirth = candidate.DateOfBirth,
                        Email = candidate.Email,
                        PositionName = positionFound.PositionName,
                        SubmissionDate = submissionFound.DateOfSubmission,
                        CandidateSituation = hiringProcessFound.CandidateSituation.ToString()
                    });
                }
            }

            return Ok(candidates);
        }

        [HttpGet("{id}", Name = "GetCandidateById")]
        [Authorize(Roles = "Recruiter, Administrator")]
        public async System.Threading.Tasks.Task<ActionResult<UserReadDTO>> GetCandidateByIdAsync(string id)
        {
            var candidate = await _repository.GetCandidateByIdAsync(id);

            if (candidate == null) return NotFound();

            return Ok(_mapper.Map<UserReadDTO>(candidate));
        }

        /*       //Full update - PUT in Postman and Web (Angular)
               [HttpPut]
               [Route("/candidates/update/{id}")]
               public ActionResult UpdateCandidate(string id, CandidateUpdateDTO candidateUpdateDTO)
               {
                   var candidateFromRepository = _repository.GetCandidateById(id);
                   if(candidateFromRepository == null)
                   {
                       return NotFound();
                   }

                   //candidateUpdateDTO is the Source and candidateFromRepository is the destination
                   _mapper.Map(candidateUpdateDTO, candidateFromRepository);

                   _repository.CandidateUpdate(candidateFromRepository);
                   _repository.SaveChanges();

                   return Ok();
               }*/

        /*[HttpPatch("{id}")]
        public ActionResult PatchCandidate(string id, JsonPatchDocument<CandidateUpdateDTO> doc)
        {
            var candidateFromRepository = _repository.GetCandidateById(id);
            if(candidateFromRepository == null)
            {
                return NotFound();
            }

            var candidatePatch = _mapper.Map<CandidateUpdateDTO>(candidateFromRepository);
            doc.ApplyTo(candidatePatch, ModelState);

            if (!TryValidateModel(candidatePatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(candidatePatch, candidateFromRepository);
            _repository.CandidateUpdate(candidateFromRepository);
            _repository.SaveChanges();

            return Ok();
        }*/

        /*        [HttpDelete("{id}")]
                public ActionResult RemoveCandidate(string id)
                {
                    var candidateFromRepository = _repository.GetCandidateById(id);
                    if(candidateFromRepository == null)
                    {
                        return NotFound();
                    }

                    _repository.RemoveCandidate(candidateFromRepository);
                    _repository.SaveChanges();

                    return Ok();
                }*/
    }
}
