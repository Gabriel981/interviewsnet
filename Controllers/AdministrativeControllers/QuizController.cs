﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/quiz")]
    public class QuizController : ControllerBase
    {
        private readonly IQuizRepository quizRepository;
        private readonly IInterviewRepo interviewRepository;
        private readonly IUserRepository userRepository;
        private readonly IHiringRepo hiringRepository;
        private readonly ISubmissionRepo submissionRepository;
        private readonly INotificationService notificationService;
        private readonly IQuestionRepository questionRepository;
        private readonly IMapper mapper;

        public QuizController(
            IQuizRepository quizRepository,
            IInterviewRepo interviewRepository,
            IUserRepository userRepository,
            IHiringRepo hiringRepository,
            ISubmissionRepo submissionRepository,
            INotificationService notificationService,
            IQuestionRepository questionRepository,
            IMapper mapper)
        {
            this.quizRepository = quizRepository;
            this.interviewRepository = interviewRepository;
            this.userRepository = userRepository;
            this.hiringRepository = hiringRepository;
            this.submissionRepository = submissionRepository;
            this.notificationService = notificationService;
            this.questionRepository = questionRepository;
            this.mapper = mapper;
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("/all")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<QuizReadDTO>>> GetAllQuizesAsync()
        {
            var quizesFound = await quizRepository.GetAllQuizesAsync();
            if (quizesFound == null)
                return NotFound("No quizes found in the database");

            return Ok(mapper.Map<IEnumerable<QuizReadDTO>>(quizesFound));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<ActionResult<QuizReadDTO>> GetQuizByInterviewIdAsync(int id)
        {
            var interviewFound = await interviewRepository.GetInterviewByIdAsync(id);
            if (interviewFound == null)
                return NotFound("No interview found in the database with id " + id);

            var quizFound = await quizRepository.GetQuizByIdAsync(interviewFound.QuizId.GetValueOrDefault());
            if (quizFound == null)
                return NotFound("No quiz found in the database with id " + interviewFound.QuizId);

            return Ok(mapper.Map<QuizReadDTO>(quizFound));
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPost("add")]
        public async System.Threading.Tasks.Task<ActionResult> AddQuizFromInterviewAsync([FromBody] QuizWriteDTO quizWriteDTO)
        {
            var interviewFound = await interviewRepository.GetInterviewByIdAsync(quizWriteDTO.InterviewId);
            if (interviewFound == null)
                return NotFound("No interview found in the database with id " + quizWriteDTO.InterviewId);

            if (interviewFound.QuizId != null)
            {
                return BadRequest("The selected interview has already a quiz allocated. Remove it to proceed to quiz creation");
            }

            var userFound = await userRepository.GetUserById(quizWriteDTO.CreatedById);
            if (userFound == null)
                return NotFound("No user found in the database with id " + quizWriteDTO.CreatedById);

            var hiringFound = await hiringRepository.GetHiringProcessByInterviewId(interviewFound.InterviewId);
            if (hiringFound == null)
                return NotFound("No hiring process found in the database with id " + interviewFound.HiringId);

            var submissionFound = await submissionRepository.GetSubmissionByIdAsync(hiringFound.SubmissionId);
            if (submissionFound == null)
                return NotFound("No submission found in the database with id " + hiringFound.SubmissionId);

            var currentTime = DateTime.UtcNow.AddHours(2).ToString();

            var newlyCreatedQuiz = new Quiz()
            {
                Mentions = quizWriteDTO.Mentions,
                ScoreResult = 0,
                PositionId = submissionFound.PositionId,
                CreatedAt = currentTime,
                CreatedById = userFound.Id,
            };

            await quizRepository.AddQuizAsync(newlyCreatedQuiz);
            await quizRepository.SaveChangesAsync();

            var quizFound = await quizRepository.GetQuizVariousParamsAsync(quizWriteDTO.CreatedById, currentTime, submissionFound.PositionId);
            if (quizFound == null)
                return NotFound("No quiz found in the database with the params provided");

            interviewFound.QuizId = quizFound.QuizId;

            interviewRepository.UpdateInterview(interviewFound);
            await interviewRepository.SaveChangesAsync();

            var users = this.userRepository.GetAllUsers().Result;
            var receiversFound = users.Where(u => u.Position.PositionName == "Recruiter").ToList();

            foreach (var receiverFound in receiversFound)
            {
                if (receiverFound.Id != userFound.Id)
                    await notificationService.PostNotificationCreatedQuiz(userFound.Id, receiverFound.Id, interviewFound.InterviewId);
            }

            return Ok("Quiz successfully added to database");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<ActionResult> RemoveQuizFromInterviewAsync(int id)
        {
            var interviewFound = await interviewRepository.GetInterviewByIdAsync(id);
            if (interviewFound == null)
                return NotFound("No interview found in the database with id " + id);

            var quizFound = await quizRepository .GetQuizByIdAsync(interviewFound.QuizId.GetValueOrDefault());
            if (quizFound == null)
                return NotFound("No quiz found in the database with id " + interviewFound.QuizId);

            var creatorFound = await userRepository.GetUserById(quizFound.CreatedById);
            if (creatorFound == null)
                return NotFound("No user found in the database with id " + quizFound.CreatedById);

            interviewFound.QuizId = null;
            interviewRepository.UpdateInterview(interviewFound);
            await interviewRepository.SaveChangesAsync();

            quizRepository.RemoveQuiz(quizFound);
            await quizRepository.SaveChangesAsync();

            var receiversFound = this.userRepository.GetAllUsers().Result.Where(u => u.Position.PositionName == "Recruiter").ToList();

            foreach (var receiverFound in receiversFound)
            {
                if (receiverFound.Id != creatorFound.Id)
                {
                    await notificationService.PostNotificationRemovedQuiz(creatorFound.Id, receiverFound.Id, interviewFound.InterviewId);
                }
            }

            return Ok("The quiz allocated to the selected Interview was succesfully removed from database");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpPost("postQuestion")]
        public async System.Threading.Tasks.Task<ActionResult> AddQuestionToQuizDynamicAsync([FromBody] QuestionWriteDTO questionWriteDTO)
        {
            var creatorFound = await userRepository.GetUserById(questionWriteDTO.CreatedById);
            if (creatorFound == null)
                return NotFound("No user was found in the database with id " + questionWriteDTO.CreatedById);

            var quizFound = await quizRepository.GetQuizByIdAsync(questionWriteDTO.QuizId);
            if (quizFound == null)
                return NotFound("No quiz was found in the database with id " + questionWriteDTO.QuizId);

            var currentQuestionCreationTime = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm:ss");

            Question question = new Question()
            {
                QuestionBody = questionWriteDTO.QuestionBody,
                CreatedById = creatorFound.Id,
                CreatedAt = currentQuestionCreationTime,
                PositionId = questionWriteDTO.PositionId
            };

            await questionRepository.AddQuestionAsync(question);
            await questionRepository.SaveChangesAsync();

            var questionFound = await questionRepository.GetQuestionByParamsAsync(creatorFound.Id, currentQuestionCreationTime, questionWriteDTO.PositionId);
            if (questionFound == null)
                return NotFound("No question found in the database based on the parameters provided");

            var currentQuizQuestionCreationTime = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm:ss");

            QuizQuestion quizQuestion = new QuizQuestion()
            {
                QuestionId = questionFound.QuestionId,
                QuizId = questionWriteDTO.QuizId,
                AllocatedAt = currentQuizQuestionCreationTime
            };

            await questionRepository.AddQuestionToQuizAsync(quizQuestion);
            await questionRepository.SaveChangesAsync();

            var questionObj = await questionRepository.GetLastQuestionAsync();
            var questionId = questionObj.QuestionId;
            return Ok(questionId);
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("questions/{id}")]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<QuestionReadDTO>>> GetQuizQuestionsAsync(int id)
        {
            var quizFound = await quizRepository.GetQuizByIdAsync(id);
            if (quizFound == null)
                return NotFound("No quiz found in the database with id " + id);

            var questionsFound = await questionRepository.GetQuestionsByQuizAsync(quizFound.QuizId);
            if (questionsFound == null)
                return NotFound("No questions found in the database for quiz with id " + quizFound.QuizId);

            /*            var questionsReadDTOs = questionsFound.Select(async s => new QuestionReadDTO()
                        {
                            QuestionBody = s.QuestionBody,
                            CreatedBy = await userRepository.GetUserById(s.CreatedById).Result.FirstName + " " + this.userRepository.GetUserById(s.CreatedById).LastName,
                            QuestionId = s.QuestionId,
                            CreatedAt = s.CreatedAt,
                            NoOfAnswers = this.questionRepository.GetAnswersByQuestionId(s.QuestionId).Count()
                        });*/

            List<QuestionReadDTO> questionsReadDTOs = new List<QuestionReadDTO>();

            foreach(var question in questionsFound)
            {
                User createdBy = await userRepository.GetUserById(question.CreatedById);
                var answers = await questionRepository.GetAnswersByQuestionIdAsync(question.QuestionId);
                var noOfAnswers = answers.Count();

                questionsReadDTOs.Add(new QuestionReadDTO
                {
                    QuestionBody = question.QuestionBody,
                    CreatedBy = createdBy.FirstName + " " + createdBy.LastName,
                    QuestionId = question.QuestionId,
                    CreatedAt = question.CreatedAt,
                    NoOfAnswers = noOfAnswers
                });
            }

            if (questionsReadDTOs == null)
                return NotFound();

            return Ok(questionsReadDTOs);
        }
    }
}
