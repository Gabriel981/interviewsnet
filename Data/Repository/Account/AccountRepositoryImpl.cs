﻿using InterviewsManagementNET.Data.ViewModel.Account;
using InterviewsManagementNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Account
{
    public class AccountRepositoryImpl : IAccountRepository
    {
        private readonly SubmissionContext _submissionContext;

        public AccountRepositoryImpl(SubmissionContext submissionContext)
        {
            _submissionContext = submissionContext;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _submissionContext.SaveChangesAsync() > 0;
        }

        public void UpdateUserSettings(User user)
        {
            _submissionContext.Users.Update(user);
        }
    }
}
