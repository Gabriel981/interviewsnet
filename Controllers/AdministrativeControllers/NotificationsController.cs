﻿using AutoMapper;
using Firebase.Database;
using Firebase.Database.Query;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/notifications")]
    public class NotificationsController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IPositionRepo _positionRepository;
        private readonly IMapper _mapper;

        public NotificationsController(IUserRepository userRepository, IPositionRepo positionRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _positionRepository = positionRepository;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("send")]
        [Authorize(Roles = "Administrator, Recruiter, Candidate registered, Employee, Manager departament resurse umane")]
        public async Task<ActionResult> PostData(Data.ViewModel.UserViewModel userViewModel1)
        {
            var senderId = await _userRepository.GetUserByEmailAddress(userViewModel1.Email);

            if (senderId == null)
            {
                return NotFound();
            }

            var adminReceiverId = _userRepository.GetUserByRole("Administrator");

            if (adminReceiverId == null)
            {
                return NotFound();
            }

            var recruiterReceiversIds = new List<string>();

            var availableUsers = await _userRepository.GetAllUsers();

            foreach (var availableUser in availableUsers)
            {
                if (availableUser.Role == "Recruiter")
                {
                    recruiterReceiversIds.Add(availableUser.Id);
                }
            }

            var finalReceiversIds = new List<string>();
            finalReceiversIds.Add(adminReceiverId.Id);

            foreach (var recruiterReceiverId in recruiterReceiversIds)
            {
                finalReceiversIds.Add(recruiterReceiverId);
            }

            var currentLoginTime = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            var appliedPosition = await _positionRepository.GetPositionByIdAsync(userViewModel1.PositionId);

            var notificationMessage = new Notification() { SenderId = senderId.Id, ReceviverId = adminReceiverId.Id, Message = userViewModel1.Message, Timestamp = currentLoginTime, PositionName = appliedPosition.PositionName };

            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");

            foreach (var finalReceiverId in finalReceiversIds)
            {
                await firebaseClient
                    .Child("Notifications")
                    .Child(finalReceiverId)
                    .Child("ReceivedMessages")
                    .PostAsync(notificationMessage);
            }

            return Ok("Notification send successfully");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Candidate registered, Employee, Recruiter")]
        [HttpGet]
        [Route("show/{id}")]
        public async Task<IEnumerable<NotificationViewModel>> GetData(string id)
        {
            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            var dbNotifications = await firebaseClient
                .Child("Notifications")
                .Child(id)
                .Child("ReceivedMessages")
                .OnceAsync<Notification>();

            var notificationsDictionary = new Dictionary<string, Notification>();

            /*            var notifications = new List<Notification>();

                        foreach(var notification in dbNotifications)
                        {
                            notifications.Add(notification.Object);
                        }
            */
            foreach (var notification in dbNotifications)
            {
                notificationsDictionary.Add(notification.Key, notification.Object);
            }

            var notificationsViewModels = new List<NotificationViewModel>();


            foreach (KeyValuePair<string, Notification> notificationView in notificationsDictionary)
            {
                User senderFound = await _userRepository.GetUserById(notificationView.Value.SenderId);
                User receiverFound = await _userRepository.GetUserById(notificationView.Value.ReceviverId);

                NotificationViewModel notificationViewModel = new NotificationViewModel
                {
                    FirstNameReceiver = receiverFound.FirstName,
                    LastNameReceiver = receiverFound.LastName,
                    FirstNameSender = senderFound.FirstName,
                    LastNameSender = senderFound.LastName,
                    EmailSender = senderFound.Email,
                    EmailReceiver = senderFound.Email,
                    Timestamp = notificationView.Value.Timestamp,
                    Title = notificationView.Value.Title,
                    Message = notificationView.Value.Message,
                    PositionName = notificationView.Value.PositionName,
                    NotificationId = notificationView.Key
                };

                notificationsViewModels.Add(notificationViewModel);
            }

            return notificationsViewModels;
        }

        [HttpPost]
        [Route("remove")]
        [Authorize(Roles = "Administrator, Recruiter, Candidate registered, Employee, Manager departament resurse umane")]
        public async Task<ActionResult> RemoveNotification(NotificationDeleteViewModel notificationDeleteViewModel)
        {
            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            await firebaseClient
                .Child("Notifications")
                .Child(notificationDeleteViewModel.UserId)
                .Child("ReceivedMessages")
                .Child(notificationDeleteViewModel.NotificationId)
                .DeleteAsync();

            return Ok("The notification was successfully removed");
        }

        [HttpGet]
        [Route("notificationDetails/{id}/{id2}")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter, Employee, Candidate registered")]
        public async Task<ActionResult<CandidateRemovalDetailedViewModel>> GetNotificationDetails(string id, string id2)
        {

            var firebaseClient = new FirebaseClient("https://interviewsnetproject-default-rtdb.europe-west1.firebasedatabase.app/");
            var dbNotifications = await firebaseClient
                .Child("Notifications")
                .Child(id2)
                .Child("ReceivedMessages")
                .OnceAsync<Notification>();

            Notification notificationFound = null;

            foreach (var notification in dbNotifications)
            {
                if (notification.Key == id)
                {
                    notificationFound = notification.Object;
                }
            }

            var senderFound = await _userRepository.GetUserById(notificationFound.SenderId);

            var senderCurrentPosition = await _positionRepository.GetPositionByIdAsync(senderFound.PositionId);

            senderFound.Position = senderCurrentPosition;

            var candidateFound = await _userRepository.GetUserById(notificationFound.CandidateId);

            var candidateRemovalDetails = new CandidateRemovalDetailedViewModel()
            {
                Admin = senderFound,
                Candidate = candidateFound,
                Message = notificationFound.Message,
                Timestamp = notificationFound.Timestamp
            };

            return Ok(candidateRemovalDetails);
        }
    }
}
