﻿using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace InterviewsManagementNET.Data.Repository
{
    public class ApplicationRepositoryImpl : IApplicationRepository
    {
        public readonly SubmissionContext _context;

        public ApplicationRepositoryImpl(SubmissionContext context)
        {
            _context = context;
        }

        public IEnumerable<Position> GetAllAvailablePositions()
        {
            return _context.Positions
                .Where(p => p.AvailableForRecruting == true && p.NoVacantPositions > 0)
                .ToList();
        }

        public Position GetPositionById(int id)
        {
            return _context.Positions.Where(p => p.AvailableForRecruting == true && p.NoVacantPositions > 0)
                .FirstOrDefault(p => p.PositionId == id);
        }

        public Position GetPositionByName(string positionName)
        {
            return _context.Positions.FirstOrDefault(p => p.PositionName == positionName);
        }

        public IEnumerable<SubmissionViewModel> GetUserSubmissions(string id)
        {
            return _context.Submissions.Include(s => s.HiringProcess).Include(s => s.AppliedPosition).Select(s => new SubmissionViewModel
            {
                AppliedPosition = s.AppliedPosition,
                DateOfSubmission = s.DateOfSubmission,
                Status = s.HiringProcess.CandidateSituation.ToString(),
                CandidateId = s.CandidateId,
                PositionId = s.PositionId,
                SubmissionId = s.SubmissionId
            }).ToList().Where(s => s.CandidateId == id);
        }
    }
}
