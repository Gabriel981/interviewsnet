﻿using System;
using System.Collections.Generic;

namespace InterviewsManagementNET.Models
{
    public class Submission
    {
        public int SubmissionId { get; set; }
        public DateTime DateOfSubmission { get; set; }
        public int PositionId { get; set; }
        public Position AppliedPosition { get; set; }
        public string CandidateId { get; set; }
        public User User { get; set; }
        public int? HiringId { get; set; }
        public HiringProcess HiringProcess { get; set; }
        public ICollection<Interview> Interviews { get; set; }
        public string Mentions { get; set; }
        public string CV { get; set; }
    }
}