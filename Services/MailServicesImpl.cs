﻿using InterviewsManagementNET.Data.ViewModel.Administrative;
using InterviewsManagementNET.Models;
using System;
using System.Net;
using System.Net.Mail;

namespace InterviewsManagementNET.Services
{
    public class MailServicesImpl : IMailServices
    {
        private static readonly string CONNECTION_HOST = "smtp.mailtrap.io";
        private static readonly string CONNECTION_USERNAME = "c66c7a99de9d79";
        private static readonly string CONNECTION_PASS = "39da7e6360b986";
        private static readonly int CONNECTION_PORT = 2525;
        private static readonly string EMAIL_SENDER = "admin@mail.com";


        public void SendEmailInterview(HiringProcess hiringProcess)
        {
            throw new NotImplementedException();
        }

        public void SendEmailSubmission(Submission submission)
        {
            MailAddress RecipientAddress = new MailAddress(submission.User.Email);
            MailAddress SenderAddress = new MailAddress(EMAIL_SENDER);


            MailMessage messageToBeSend = new MailMessage(SenderAddress, RecipientAddress);
            messageToBeSend.Subject = "Application for our vacant position: ' + submission.AppliedPosition.PositionName";
            messageToBeSend.Body = "We are grad that you've applied for our vacant position " + submission.AppliedPosition.PositionName
                    + "Please check further if are any changes in your submission process / hiring process!";


            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(messageToBeSend);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

        }

        public void SendCandidateRemovalProposal(User proposedCandidate, User manager, User admin, string Reason)
        {
            MailAddress RecipientAddress = new MailAddress(manager.Email);
            MailAddress SenderAddress = new MailAddress(admin.Email);

            MailMessage candidateRemovalProposal = new MailMessage(SenderAddress, RecipientAddress);
            candidateRemovalProposal.Subject = "Candidate removal proposal for " + proposedCandidate.FirstName + " " + proposedCandidate.LastName;
            candidateRemovalProposal.Body = "My name is " + admin.FirstName + " " + admin.LastName + " and after a search I've found that the candidate with the name " + proposedCandidate.FirstName + " "
                    + proposedCandidate.LastName + " needs to be removed, because " + " he " + Reason + " so I need an accept from you, to continue the procedure.";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(candidateRemovalProposal);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }
        }

        public bool SendCandidateRemovalSuccess(User candidate, User manager, User admin, string reason)
        {
            MailAddress RecipientAddress = new MailAddress(manager.Email);
            MailAddress SenderAddress = new MailAddress(admin.Email);

            MailMessage candidateRemovalSuccess = new MailMessage(SenderAddress, RecipientAddress);
            candidateRemovalSuccess.Subject = "Notification about the request of removal that you've sent to Administrators";
            candidateRemovalSuccess.Body = $"Good morning, {manager.FirstName} {manager.LastName}. After analysing your request of removal" +
                $" the candidate with name {candidate.FirstName} {candidate.LastName} and the id {candidate.Id}, I've decided to remove it's traces from our database." +
                $" With regards, {admin.FirstName} {admin.LastName} - Administrator at -- Company name --";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(candidateRemovalSuccess);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            MailAddress CandidateRecipientAddress = new MailAddress(candidate.Email);
            MailAddress AdminSenderAddress = new MailAddress(admin.Email);

            MailMessage candidateRemovalConfirmation = new MailMessage(AdminSenderAddress, CandidateRecipientAddress);
            candidateRemovalConfirmation.Subject = "Notification about the removal of your data from our database";
            candidateRemovalConfirmation.Body = $"Hello, {candidate.FirstName} {candidate.LastName}. After analysing your last activity, we've decided that due to your inactivity" +
                $" we are in need to remove your data, so it can be stealed or used in other ways than our porpose to recruit talented people for our vacant jobs." +
                $" With regards, {admin.FirstName} {admin.LastName} - Administrator at -- Company name -- " +
                $" With delegation from {manager.FirstName} {manager.LastName} - Manager at -- Company name --";

            SmtpClient SMTP_CLIENT2 = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT2.Send(candidateRemovalConfirmation);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            return true;
        }

        public bool SendCandidateApproval(User candidate, User manager, string positionName)
        {
            MailAddress RecipientAddress = new MailAddress(candidate.Email);
            MailAddress SenderAddress = new MailAddress(manager.Email);

            MailMessage candidateApproval = new MailMessage(SenderAddress, RecipientAddress);
            candidateApproval.Subject = "Notification about your hiring process";
            candidateApproval.Body = $"Hello, {candidate.FirstName} {candidate.LastName}. After analysing your activity and result during the hiring process" +
                $" we've concluded that you are the perfect candidate for our {positionName} vacant position." +
                $" We'll be in contact with you these days to proceed to the next step of your hiring in the position mentioned before." +
                $" Keep waiting a little, we'll contact you!" +
                $" {manager.FirstName} {manager.LastName} - Manager at -- Company name --";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(candidateApproval);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            return true;
        }

        public bool SendCandidateRejection(User candidate, User manager, string positionName)
        {
            MailAddress RecipientAddress = new MailAddress(candidate.Email);
            MailAddress SenderAddress = new MailAddress(manager.Email);

            MailMessage candidateRejection = new MailMessage(SenderAddress, RecipientAddress);
            candidateRejection.Subject = "Notification about your hiring process";
            candidateRejection.Body = $"Hello, {candidate.FirstName} {candidate.LastName}. After analysing your activity and results during the hiring process " +
                $" for position {positionName}, we're sorry to announce you that you didn't take to the next step. But, you can be our employee in time," +
                $" by applying to our future vacant positions or internships." +
                $" Thanks for patience and specially, for your time taken by applying to our vacant position." +
                $" {manager.FirstName} {manager.LastName} - Manager at -- Company name --";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(candidateRejection);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.Write(SMTP_EXC.ToString());
            }

            return true;
        }

        public bool SendParticipantAttendanceInterview(User sender, User receiver, Interview interview, User candidate)
        {
            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress(sender.Email);

            MailMessage attendanceInterview = new MailMessage(SenderAddress, RecipientAddress);
            attendanceInterview.Subject = "Interview attendance";
            attendanceInterview.Body = $"Hello, {receiver.FirstName} {receiver.LastName}. You've been allocated to the interview with id " +
                $" {interview.InterviewId}, scheduled on {interview.InterviewDate}, for candidate {candidate.FirstName} {candidate.LastName}." +
                $" Check the notification that you've received on the app." +
                $" With regards, {sender.FirstName} {sender.LastName} - Recruiter at -- Company name --";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(attendanceInterview);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            return true;
        }

        public string SendCandidateDeleteConfirmation(User receiver)
        {
            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress("iManagement@mail.com");

            MailMessage deleteConfirmationMessage = new MailMessage(SenderAddress, RecipientAddress);
            deleteConfirmationMessage.Subject = "Delete confirmation test";
            deleteConfirmationMessage.Body = "test";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(deleteConfirmationMessage);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            string successMessage = "Successfully sent";

            return successMessage;
        }

        public string SendCandidatePassword(User receiver, string generatedPassword)
        {
            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress("iManagement@mail.com");

            MailMessage generatedPasswordMail = new MailMessage(SenderAddress, RecipientAddress);
            generatedPasswordMail.Subject = "Generated password for iManagement application";
            generatedPasswordMail.Body = $"Hello, {receiver.FirstName} {receiver.LastName}. Your generated password to access iManagement application is {generatedPassword}." +
                $" Be aware that this password needs to be changed to one that you can easily reproduce. " +
                $" With regards, iManagemenent";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(generatedPasswordMail);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            string successMessage = "Succesfully sent";

            return successMessage;
        }

        public string SendPasswordRequest(MessageViewModel message)
        {
            MailAddress RecipientAddress = new MailAddress(message.Email);
            MailAddress SenderAddress = new MailAddress("iManagement@mail.com");

            MailMessage generatedPasswordMail = new MailMessage(SenderAddress, RecipientAddress);
            generatedPasswordMail.Subject = "Reset password";
            generatedPasswordMail.Body = "Hello. Due to security issues, you need to change your password immediatelly. "
               + $" {message.Callback}"
               + " With regards, iManagemenent";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(generatedPasswordMail);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            string successMessage = "Succesfully sent";

            return successMessage;
        }

        public string SendEmailFromAdmin(User receiver, User admin, string reason, string additionalInformation)
        {
            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress(admin.Email);

            MailMessage generatedPasswordMail = new MailMessage(SenderAddress, RecipientAddress);
            generatedPasswordMail.Subject = "Reset password";
            generatedPasswordMail.Body = "Hello. Due to a decision taken inside our company, I've decided that your account will be removed from our database. "
               + $" One of the reason: 1. Due to inactivity, 2. Due to a security issue, 3. You have been fired, 4. Security/Administrative issues"
               + $" With regards, {admin.FirstName} {admin.LastName} - Administrator on iManagement Hiring Platform";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(generatedPasswordMail);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            string successMessage = "Succesfully sent";

            return successMessage;
        }

        public string SendParticipationRemovalResponse(User receiver, string reason)
        {
            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress("iManagement@mail.com");

            MailMessage generatedPasswordMail = new MailMessage(SenderAddress, RecipientAddress);
            generatedPasswordMail.Subject = "Reset password";
            generatedPasswordMail.Body = $"Good morning, {receiver.FirstName} {receiver.LastName}.\n" +
                $" This email notifies you about your removal from one of the interviews.\n" +
                $" Reason: {reason}" +
                $" Kindly regards iManagement Team";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(generatedPasswordMail);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            string successMessage = "Succesfully sent";

            return successMessage;
        }

        public bool SendTwoFactorToken(User receiver, string token)
        {

            MailAddress RecipientAddress = new MailAddress(receiver.Email);
            MailAddress SenderAddress = new MailAddress("iManagement@mail.com");

            MailMessage generatedPasswordMail = new MailMessage(SenderAddress, RecipientAddress);
            generatedPasswordMail.Subject = "Reset password";
            generatedPasswordMail.Body = $"Here's your token to log in {token}";

            SmtpClient SMTP_CLIENT = new SmtpClient(CONNECTION_HOST, CONNECTION_PORT)
            {
                Credentials = new NetworkCredential(CONNECTION_USERNAME, CONNECTION_PASS),
                EnableSsl = true
            };

            try
            {
                SMTP_CLIENT.Send(generatedPasswordMail);
            }
            catch (SmtpException SMTP_EXC)
            {
                Console.WriteLine(SMTP_EXC.ToString());
            }

            return true;
        }
    }
}
