﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace InterviewsManagementNET.Controllers
{
    [Route("api/positions")]
    [ApiController]
    public class PositionsController : ControllerBase
    {
        public readonly IPositionRepo _repository;
        public readonly IMapper _mapper;
        private readonly SubmissionContext _context;
        private readonly IDepartmentRepository _depRepository;
        private readonly IUserRepository _userRepository;
        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        //Dependency injection per Repository
        public PositionsController(IPositionRepo repository, IMapper mapper, SubmissionContext context, IDepartmentRepository depRepository, IUserRepository userRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _context = context;
            _depRepository = depRepository;
            _userRepository = userRepository;
        }

        [Authorize(Roles = "Recruiter, Administrator")]
        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult<IEnumerable<PositionReadDTO>>> GetAllPositionsAsync()
        {
            var positions = await _repository.GetAllPositionsAsync();
            return Ok(_mapper.Map<IEnumerable<PositionReadDTO>>(positions.Where(p => p.PositionName != "Not assigned" && p.PositionName != "Registered candidate")));
        }

        [HttpGet("{id}", Name = "GetPositionById")]
        [Authorize(Roles = "Employee, Administrator")]
        public async System.Threading.Tasks.Task<ActionResult<Position>> GetPositionByIdAsync(int id)
        {
            var positionFound = await _repository.GetPositionByIdAsync(id);

            return Ok(_mapper.Map<PositionReadDTO>(positionFound));
        }

        [HttpPost]
        [Authorize(Roles = "Recruiter, Administrator, Manager departament resurse umane")]
        public async System.Threading.Tasks.Task<ActionResult<PositionReadDTO>> AddPositionAsync([FromBody] PositionWriteDTO positionWrite)
        {
            Position newPosition = new Position()
            {
                PositionName = positionWrite.PositionName,
                Description = positionWrite.Description,
                DepartmentId = positionWrite.DepartmentId,
                NoVacantPositions = positionWrite.NoVacantPositions,
                AvailableForRecruting = positionWrite.NoVacantPositions > 0 ? true : false
            };

            await _repository.AddPositionAsync(newPosition);
            /*            await _repository.SaveChangesAsync();*/
            await _context.SaveChangesAsync(User?.FindFirst(ClaimTypes.NameIdentifier).Value);

            Department crtDepartment = await _depRepository.GetDepartmentById(newPosition.DepartmentId.GetValueOrDefault());
            if (crtDepartment == null) return NotFound();

            crtDepartment.CurrentNumberOfPositions += crtDepartment.CurrentNumberOfPositions + 1;

            _depRepository.UpdateDepartment(crtDepartment);
            await _depRepository.SaveAllChangesAsync();
            /*            await _auditableIdentityContext.SaveChangesAsync(User?.FindFirst(ClaimTypes.NameIdentifier).Value);
            */
            return Ok("Position was succesfully submitted to database");

/*
            var positionModelConversion = _mapper.Map<Position>(positionWrite);
            _repository.AddPositionAsync(positionModelConversion);
            _repository.SaveChangesAsync();
*/
/*            var positionReadDTO = _mapper.Map<PositionReadDTO>(positionModelConversion);

            //Documentation for CreatedAtRoute (routeName, routeValues, content)
            return CreatedAtRoute(nameof(GetPositionById), new { Id = positionReadDTO.PositionId }, positionReadDTO);*/
        }


        //Works
        [HttpPut("{id}")]
        [Authorize(Roles = "Recruiter, Administrator")]
        public async System.Threading.Tasks.Task<ActionResult> UpdatePositionAsync(int id, [FromBody] PositionUpdateDTO positionUpdateDTO)
        {
            var positionFromRepository = await _repository.GetPositionByIdAsync(id);
            if (positionFromRepository == null)
            {
                return NotFound();
            }

            positionUpdateDTO.AvailableForRecruting = positionFromRepository.AvailableForRecruting;
            positionUpdateDTO.NoVacantPositions = positionFromRepository.NoVacantPositions;

            _mapper.Map(positionUpdateDTO, positionFromRepository);

            _repository.PositionUpdate(positionFromRepository);
            await _repository.SaveChangesAsync();

            return Ok("The position details was succesfully updated");
        }

        [HttpPut("vacant/{id}")]
        [Authorize(Roles = "Recruit, Administrator")]
        public async System.Threading.Tasks.Task<ActionResult> UpdatePosition(int id, [FromBody] PositionUpdateDTO positionUpdateDTO)
        {
            var administratorPosition = await _repository.GetPositionByNameAsync("Administrator");
            var administratorDepartmentFound = await _depRepository.GetDepartmentById(administratorPosition.DepartmentId.GetValueOrDefault());

            if (positionUpdateDTO.PositionName == administratorPosition.PositionName) return BadRequest("Cannot change the name of position to Administrator");
            
            var positionFound = await _repository.GetPositionByIdAsync(id);
            if (positionFound == null) return NotFound();

            var departmentFound = await _depRepository.GetDepartmentById(positionUpdateDTO.DepartmentId);
            if (departmentFound == null) return NotFound($"No department was found in the dabase with id {positionFound.DepartmentId}");

            if (departmentFound.DepartmentName == administratorDepartmentFound.DepartmentName) return BadRequest($"Cannot change the department to {administratorDepartmentFound.DepartmentName}");

            if (positionUpdateDTO.NoVacantPositions > 0)
            {
                positionUpdateDTO.AvailableForRecruting = true;
            }
            else if(positionUpdateDTO.NoVacantPositions == 0)
            {
                positionUpdateDTO.AvailableForRecruting = false;
            }
            else
            {
                return BadRequest("Cannot update the position details because the number of vacant positions is negative");
            }

            _mapper.Map(positionUpdateDTO, positionFound);
            _repository.PositionUpdate(positionFound);
            await _repository.SaveChangesAsync();

            return Ok("The number of available for recruiting positions was succesfully updated in database");
        }

        //Works
        [HttpPatch("{id}")]
        [Authorize(Roles = "Recruiter")]
        public async System.Threading.Tasks.Task<ActionResult> PatchPositionAsync(int id, JsonPatchDocument<PositionUpdateDTO> doc)
        {
            var positionFromRepository = await _repository.GetPositionByIdAsync(id);
            if (positionFromRepository == null)
            {
                return NotFound();
            }

            var positionPatch = _mapper.Map<PositionUpdateDTO>(positionFromRepository);

            //Documentation:
            //Model state represents errors that come from two subsystems: model binding and model validation
            //Both model binding and model validation occur before the execution 
            //of a controller action or a Razor Pages handler method

            doc.ApplyTo(positionPatch, ModelState);

            //TryValidateModel will check if there are any validation errors
            if (!TryValidateModel(positionPatch))
            {
                return ValidationProblem(ModelState);
            }

            //Mapping from positionPatch to position retrieved from repository
            //Also, add the updated data to positionFromRepository
            _mapper.Map(positionPatch, positionFromRepository);
            _repository.PositionUpdate(positionFromRepository);
            await _repository.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Recruiter, Administrator, Manager departament resurse umane")]
        public async System.Threading.Tasks.Task<ActionResult> RemovePositionAsync(int id)
        {
            var positionFromRepository = await _repository.GetPositionByIdAsync(id);
            if (positionFromRepository == null)
            {
                return NotFound();
            }

            _repository.RemovePosition(positionFromRepository);
            await _repository.SaveChangesAsync();

            return Ok("Selected position was succesfully removed from database");
        }

        [Authorize(Roles = "Administrator, Recruiter")]
        [HttpGet("export")]
        public async System.Threading.Tasks.Task<IActionResult> ExportPositionsDataAsync()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var positionsAvailable = await _repository.GetAllPositionsAsync();
            if (positionsAvailable == null)
                return NotFound("No positions was found in the database");

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Available positions");

                int index = 1;

                worksheet.Cells["B1"].Value = "Position id";
                worksheet.Cells["C1"].Value = "Position name";
                worksheet.Cells["D1"].Value = "Available for recruiting";
                worksheet.Cells["E1"].Value = "No. of available positions";

                worksheet.Cells["B1:E1"].Style.Font.Bold = true;
                worksheet.Cells["B1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1:E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1:E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                foreach (var position in positionsAvailable)
                {
                    worksheet.Cells["B" + (index + 1)].Value = position.PositionId;
                    worksheet.Cells["C" + (index + 1)].Value = position.PositionName;
                    worksheet.Cells["D" + (index + 1)].Value = position.AvailableForRecruting;
                    worksheet.Cells["E" + (index + 1)].Value = position.NoVacantPositions;

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "AvailablePositions.xlsx");
        }

       /* [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/positions/name")]
        public ActionResult<IEnumerable<PositionReadDTO>> CustomFilterPositionName(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == "")
            {
                var retrievedPositions = this._repository.GetAllPositions().Where(p => p.PositionName != "Candidate registered");
                if (retrievedPositions == null)
                    return NotFound("No positions was found in the database");

                return Ok(retrievedPositions);
            }

            if(searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var positions = this._repository.GetAllPositions().Where(p => p.PositionName != "Candidate registered");
                if (positions == null)
                    return NotFound("No positions was found in the database");

                var positionsFinalList = new List<PositionReadDTO>();

                if(IsAlphaNumeric(searchKey) == true)
                {
                    bool isNumeric = int.TryParse(searchKey, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the name by numbers");

                    foreach(var position in positions)
                    {
                        if(position.PositionName.ToLower().ToString().Contains(searchKey))
                        {
                            positionsFinalList.Add(new PositionReadDTO()
                            {
                                PositionId = position.PositionId,
                                PositionName = position.PositionName,
                                Department = position.Department.DepartmentName,
                                AvailableForRecruting = position.AvailableForRecruting,
                                NoVacantPositions = position.NoVacantPositions
                            });
                        }
                    }

                    if (positionsFinalList == null)
                        return NotFound("No positions has been pushed in the final list");

                    return Ok(positionsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }*/

        /*[Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/positions/department")]
        public ActionResult<IEnumerable<PositionReadDTO>> CustomFilterDepartmentName(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var retrievedPositions = this._repository.GetAllPositions().Where(p => p.PositionName != "Candidate registered");
                if (retrievedPositions == null)
                    return NotFound("No positions was found in the database");

                return Ok(retrievedPositions);
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var positions = this._repository.GetAllPositions().Where(p => p.PositionName != "Candidate registered");
                if (positions == null)
                    return NotFound("No positions was found in the database");

                var positionsFinalList = new List<PositionReadDTO>();

                if (IsAlphaNumeric(searchKey) == true)
                {
                    bool isNumeric = int.TryParse(searchKey, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the name by numbers");

                    foreach (var position in positions)
                    {
                        if (position.Department.ToLower().ToString().Contains(searchKey))
                        {
                            positionsFinalList.Add(new PositionReadDTO()
                            {
                                PositionId = position.PositionId,
                                PositionName = position.PositionName,
                                Department = position.Department,
                                AvailableForRecruting = position.AvailableForRecruting,
                                NoVacantPositions = position.NoVacantPositions
                            });
                        }
                    }

                    if (positionsFinalList == null)
                        return NotFound("No positions has been pushed in the final list");

                    return Ok(positionsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }*/

        [HttpGet]
        [Route("filters")]
        public ActionResult PositionsFilters()
        {
            List<FilterViewModel> posFilters = _context.Departments.Select(d => new FilterViewModel
            {
                FilterKey = d.DepartmentId.ToString(),
                FilterValue = d.DepartmentName,
                DefaultFilter = true
            }).ToList();

            return Ok(posFilters);
        }

        public static bool IsAlphaNumeric(string keyToCheck)
        {
            return keyToCheck.All(char.IsLetterOrDigit);
        }
    }
}
