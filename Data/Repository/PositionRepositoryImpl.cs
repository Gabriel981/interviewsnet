﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class PositionRepositoryImpl : IPositionRepo
    {
        public readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PositionRepositoryImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task AddPositionAsync(Position position)
        {
            await _context.Positions.AddAsync(position);
        }

        public async Task<IEnumerable<Position>> GetAllPositionsAsync()
        {
            return await _context.Positions.Include(p => p.Department).Where(p => p.PositionName != "Not retrieved" && p.PositionName != "Anonymous" && p.PositionName != "Administrator").ToListAsync();
           /* List<Position> positionsRetrieved = new List<Position>();
            foreach (Position p in _context.Positions.ToList())
            {
                var departmentFound = _context.Departments.FirstOrDefault(d => d.DepartmentId == p.DepartmentId);
                p.Department = departmentFound;
                if (p.PositionName != "Not retrieved" && p.PositionName != "Anonymous")
                {
                    positionsRetrieved.Add(p);
                }
            }

            *//*return _context.Positions.ToList();*//*
            return positionsRetrieved;*/
        }

        public async Task<Position> GetPositionByIdAsync(int Id)
        {
            return await _context.Positions.Include(p => p.Department).FirstOrDefaultAsync(p => p.PositionId == Id);
/*            return _context.Positions.Find(Id);*/
        }

        public async Task<Position> GetPositionByNameAsync(string positionName)
        {
            return await _context.Positions.Include(p => p.Department).FirstOrDefaultAsync(p => p.PositionName == positionName);
/*            return _context.Positions.FirstOrDefault(p => p.PositionName == positionName);*/ 
        }

        public async Task<IEnumerable<Position>> GetPositionsByDepartmentId(int departmentId)
        {
            return await _context.Positions.Where(p => p.DepartmentId == departmentId).ToListAsync();
        }

        public void PositionUpdate(Position position)
        {
            //not throwing anything
        }

        public void RemovePosition(Position position)
        {
            if (position == null)
            {
                throw new ArgumentNullException(nameof(position));
            }

            _context.Positions.Remove(position);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) >= 0;
        }


    }
}
