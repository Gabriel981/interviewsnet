﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Data.Repository.Administrative;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Data.ViewModel.Administrative;
using InterviewsManagementNET.Data.ViewModel.Administrative.Charts;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using PasswordGenerator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [ApiController]
    [Route("api/admin")]
    public class AdminController : ControllerBase
    {
        public readonly IUserRepository _userRepository;
        public readonly IPositionRepo _positionRepository;
        public readonly ISubmissionRepo _submissionRepository;
        public readonly IHiringRepo _hiringRepository;
        public readonly IMailServices _mailServices;
        public readonly INotificationService _notificationService;
        private readonly IStateSeqRepository _stateSeqRepository;
        private readonly IInterviewRepo _interviewRepository;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IRejectAllocationRepository _rejectAllocationRepository;
        private readonly IDepartmentRepository _departmentRepository;
        public readonly IMapper _mapper;

        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public AdminController(
            IUserRepository userRepository,
            IPositionRepo positionRepository,
            ISubmissionRepo submissionRepository,
            IHiringRepo hiringRepository,
            IMailServices mailServices,
            INotificationService notificationService,
            IStateSeqRepository stateSeqRepository,
            IInterviewRepo interviewRepository,
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            IRejectAllocationRepository rejectAllocationRepository,
            IDepartmentRepository departmentRepository,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _positionRepository = positionRepository;
            _submissionRepository = submissionRepository;
            _hiringRepository = hiringRepository;
            _mailServices = mailServices;
            _notificationService = notificationService;
            _stateSeqRepository = stateSeqRepository;
            _interviewRepository = interviewRepository;
            _userManager = userManager;
            _roleManager = roleManager;
            _rejectAllocationRepository = rejectAllocationRepository;
            _departmentRepository = departmentRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("employees")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<IEnumerable<Data.DTO.AdministrativeDTO.UserViewModel>>> GetAllAvailableEmployeesAsync()
        {
            IEnumerable<Data.DTO.AdministrativeDTO.UserViewModel> Users = await _userRepository.GetAllUsers();
            List<Data.DTO.AdministrativeDTO.UserViewModel> EmployeesFound = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

            var recruitersList = await _userManager.GetUsersInRoleAsync("Recruiter");
            var managersList = await _userManager.GetUsersInRoleAsync("Manager departament resurse umane");
            var othersEmployeesList = await _userManager.GetUsersInRoleAsync("Employee");

            EmployeesFound.AddRange(recruitersList.Select(r => new Data.DTO.AdministrativeDTO.UserViewModel
            {
                Id = r.Id,
                FirstName = r.FirstName,
                LastName = r.LastName,
                Address = r.Address,
                DateOfBirth = r.DateOfBirth,
                Email = r.Email,
                Mentions = r.Mentions,
                PhoneNumber = r.PhoneNumber,
                Position = r.Position,
                ProfilePhotoUrl = r.ProfilePhotoURL,
                Role = r.Role,
                SecurityQuestion = r.SecurityQuestion,
                Answer = r.SecurityQuestionAnswer
            }).ToList());

            EmployeesFound.AddRange(managersList.Select(m => new Data.DTO.AdministrativeDTO.UserViewModel {
                Id = m.Id,
                FirstName = m.FirstName,
                LastName = m.LastName,
                Address = m.Address,
                DateOfBirth = m.DateOfBirth,
                Email = m.Email,
                Mentions = m.Mentions,
                PhoneNumber = m.PhoneNumber,
                Position = m.Position,
                ProfilePhotoUrl = m.ProfilePhotoURL,
                Role = m.Role,
                SecurityQuestion = m.SecurityQuestion,
                Answer = m.SecurityQuestionAnswer
            }).ToList());

            EmployeesFound.AddRange(othersEmployeesList.Select(o => new Data.DTO.AdministrativeDTO.UserViewModel
            {
                Id = o.Id,
                FirstName = o.FirstName,
                LastName = o.LastName,
                Address = o.Address,
                DateOfBirth = o.DateOfBirth,
                Email = o.Email,
                Mentions = o.Mentions,
                PhoneNumber = o.PhoneNumber,
                Position = o.Position,
                ProfilePhotoUrl = o.ProfilePhotoURL,
                Role = o.Role,
                SecurityQuestion = o.SecurityQuestion,
                Answer = o.SecurityQuestionAnswer
            }).ToList());

            /*            foreach (Data.DTO.AdministrativeDTO.UserViewModel user in Users)
                        {
                            if (user.Role != "Registered candidate" && user.Role != "Anonymous" && user.Role != "Administrator")
                            {
                                EmployeesFound.Add(user);
                            }
                        }*/

            /*if (EmployeesFound.Count == 0)
            {
                return NotFound();
            }*/

            return Ok(EmployeesFound);
        }

        [HttpGet("notretrived")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetNotRetrievedUsers()
        {
            var notRetrievedUsers = await _userManager.GetUsersInRoleAsync("Not retrieved");
            if (notRetrievedUsers == null) return NotFound();

            List<Data.DTO.AdministrativeDTO.UserViewModel> NotRetrievedUsersFound = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

            NotRetrievedUsersFound.AddRange(notRetrievedUsers.Select(n => new Data.DTO.AdministrativeDTO.UserViewModel
            {
                Id = n.Id,
                FirstName = n.FirstName,
                LastName = n.LastName,
                Address = n.Address,
                DateOfBirth = n.DateOfBirth,
                Email = n.Email,
                Mentions = n.Mentions,
                PhoneNumber = n.PhoneNumber,
                ProfilePhotoUrl = n.ProfilePhotoURL,
                Role = n.Role
            }).ToList());

            return Ok(notRetrievedUsers);
        }

        [HttpGet("positions")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetFilteredPositions()
        {
            var positions = await _positionRepository.GetAllPositionsAsync();
            return Ok(positions.Where(p => p.PositionName != "Administrator" && p.PositionName != "Registered candidate" && p.PositionName != "Not assigned").ToList());
        }

        [HttpPost("allocate")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> AllocateToRole([FromBody] RoleAllocationViewModel roleAllocation)
        {
            var user = await _userRepository.GetUserById(roleAllocation.UserId);
            if (user == null) return NotFound();

            var position = await _positionRepository.GetPositionByIdAsync(roleAllocation.PositionId);
            if (position == null) return NotFound();

            user.PositionId = position.PositionId;
            user.Role = position.PositionName;

            bool roleExists = await _roleManager.RoleExistsAsync("Employee");

            if (!roleExists)
            {
                var role = new IdentityRole();
                role.Name = "Employee";
                await _roleManager.CreateAsync(role);
            }

            await _userManager.RemoveFromRoleAsync(user, "Not retrieved");

            var roleAssigned = await _roleManager.FindByNameAsync("Employee");

            await _userManager.AddToRoleAsync(user, roleAssigned.Name);

            return Ok("Succesfully completed the request to assign user to role");
        }

        [HttpPost("reject")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RejectEmployee([FromBody] RejectionAllocationViewModel rejectedAllocationView)
        {
            var rejectedUser = await _userRepository.GetUserById(rejectedAllocationView.UserId);
            if (rejectedUser == null) return NotFound();

            _userRepository.RemoveUser(rejectedUser);
            await _userRepository.SaveChangesAsync();

            var crtTime = DateTime.UtcNow;

            RejectedAllocation rejectedAllocation = new RejectedAllocation()
            {
                RejectionId = Guid.NewGuid().ToString(),
                CompleteName = rejectedUser.FirstName + " " + rejectedUser.LastName,
                EmailAddress = rejectedUser.Email,
                PhoneNo = rejectedUser.PhoneNumber,
                Reason = rejectedAllocationView.Reason,
                RejectedOn = crtTime.ToLongDateString(),
                LastModifiedOn = null
            };

            await _rejectAllocationRepository.AddRejectionAsync(rejectedAllocation);
            await _rejectAllocationRepository.SaveChangesAsync();

            return Ok("User succesfully rejected and removed from database");
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<Data.DTO.AdministrativeDTO.UserViewModel>> UpdateEmployeeDetailsAsync(string id, [FromBody] EmployeeUpdateViewModel employeeViewModel)
        {
            var userFound = await _userRepository.GetUserById(id);
            if (userFound == null) return BadRequest();

            userFound.FirstName = employeeViewModel.FirstName;
            userFound.LastName = employeeViewModel.LastName;
            userFound.Address = employeeViewModel.Address;
            userFound.Email = employeeViewModel.EmailAddress;
            userFound.DateOfBirth = employeeViewModel.DateOfBirth;
            userFound.PhoneNumber = employeeViewModel.PhoneNumber;
            userFound.SecurityQuestion = employeeViewModel.SecurityQuestion;
            userFound.SecurityQuestionAnswer = employeeViewModel.SecurityQuestionAnswer;

            if (await _positionRepository.GetPositionByIdAsync(employeeViewModel.PositionId) == null) return BadRequest();

            userFound.PositionId = employeeViewModel.PositionId;
            var position = await _positionRepository.GetPositionByIdAsync(employeeViewModel.PositionId);
            if(position.PositionName == "Recruiter")
            {
                var roles = await _userManager.GetRolesAsync(userFound);
                await _userManager.RemoveFromRoleAsync(userFound, roles.FirstOrDefault());
                await _userManager.RemoveFromRoleAsync(userFound, roles.FirstOrDefault());
                var recruiterRole = await _roleManager.FindByNameAsync("Recruiter");
                await _userManager.AddToRoleAsync(userFound, recruiterRole.Name);
            }

            if(position.PositionName == "Manager departament resurse umane")
            {
                var roles = await _userManager.GetRolesAsync(userFound);
                await _userManager.RemoveFromRoleAsync(userFound, roles.FirstOrDefault());
                await _userManager.RemoveFromRoleAsync(userFound, roles.FirstOrDefault());
                var recruiterRole = await _roleManager.FindByNameAsync("Manager departament resurse umane");
                await _userManager.AddToRoleAsync(userFound, recruiterRole.Name);
            }

            _userRepository.UpdateUserDetails(userFound);
            await _userRepository.SaveChangesAsync();

            return Ok("The employee details was succesfully updated");
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveEmployeeAsync(string id)
        {
            var userFound = await _userRepository.GetUserById(id);
            if (userFound == null)
                return NotFound("No employee found in the database with id " + id);

            var allParticipations = await _interviewRepository.GetAllInterviewsParticipantsAsync();
            if (allParticipations == null)
                return NotFound("No available participants on interviews found in the database");

            foreach (var participation in allParticipations)
            {
                if (participation.EmployeeId == userFound.Id)
                {
                    _interviewRepository.removeParticipantFromInterview(participation);
                }
            }

            _userRepository.RemoveUser(userFound);
            await _userRepository.SaveChangesAsync();

            return Ok("The selected employee was succesfully removed from the database");
        }

        [HttpGet]
        [Route("candidates")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<IEnumerable<CandidateViewModel>>> GetAvailableCandidatesAsync()
        {
            var submissionsRetrieved = await _submissionRepository.GetSubmissionsDetailsAsync();
            if (submissionsRetrieved == null)
            {
                return NotFound();
            }

            var candidatesFound = submissionsRetrieved.Select(s => new CandidateViewModel()
            {
                FirstName = s.User.FirstName,
                LastName = s.User.LastName,
                Address = s.User.Address,
                DateOfBirth = s.User.DateOfBirth,
                SubmissionDate = s.DateOfSubmission,
                Email = s.User.Email,
                PositionName = s.AppliedPosition.PositionName,
                CandidateSituation = s.Status
            });

            return Ok(candidatesFound);
        }

        [HttpPost]
        [Route("removal_proposal")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<Task>> RemoveSelectedCandidateAsync(CandidateRemovalViewModel candidateRemovalViewModel)
        {
            User candidateFound = await _userRepository.GetUserByEmailAddress(candidateRemovalViewModel.CandidateEmail);

            if (candidateFound == null)
            {
                return NotFound("No candidate found in the database with email " + candidateRemovalViewModel.CandidateEmail);
            }

            User managerFound = _userRepository.GetUserByRole("Manager departament resurse umane");

            if (managerFound == null)
            {
                return NotFound("No manager found in the database");
            }

            User adminFound = await _userRepository.GetUserById(candidateRemovalViewModel.AdminId);

            if (adminFound == null)
            {
                return NotFound("No administrator found in the database with id " + candidateRemovalViewModel.AdminId);
            }

            var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(candidateFound.Id);

            if (submissionFound == null)
            {
                return NotFound("No submission found for candidate with id " + candidateFound.Id);
            }

            _submissionRepository.RemoveSubmission(submissionFound);
            await _submissionRepository.SaveChangesAsync();

            _userRepository.RemoveUser(candidateFound);
            await _userRepository.SaveChangesAsync();

            var confirmationNotificationSent = this._notificationService.PostRemovalSuccessNotification(adminFound.Id);
            if (confirmationNotificationSent.Result == true)
            {
                this._mailServices.SendCandidateRemovalSuccess(candidateFound, managerFound, adminFound, null);
            }

            return Ok("Notification send successfully to manager for approval");
        }

        [HttpPut]
        [Route("candidate/update/")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<Task>> UpdateCandidateDetailsByAdminAsync(CandidateViewModel candidateViewModel)
        {
            if (candidateViewModel == null)
            {
                throw new ArgumentNullException(nameof(candidateViewModel));
            }

            User candidateFound = await _userRepository.GetUserByEmailAddress(candidateViewModel.Email);
            if (candidateFound == null)
            {
                return NotFound("Candidate not found!");
            }

            candidateFound.FirstName = candidateViewModel.FirstName;
            candidateFound.LastName = candidateViewModel.LastName;
            candidateFound.Email = candidateViewModel.Email;
            candidateFound.DateOfBirth = candidateViewModel.DateOfBirth;
            candidateFound.Address = candidateViewModel.Address;

            _userRepository.UpdateUserDetails(candidateFound);
            await _userRepository.SaveChangesAsync();

            return Ok("The update was successfully pushed to database");
        }

        [HttpPost]
        [Route("candidate/status")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<HiringViewModel>> UpdateCandidateStatusByAdminAsync(CandidateSituationResetViewModel candidateSituationResetViewModel)
        {
            User candidateFound = await _userRepository.GetUserByEmailAddress(candidateSituationResetViewModel.Email);
            if (candidateFound == null)
            {
                return NotFound("No candidate with " + candidateSituationResetViewModel.Email + " email address found!");
            }

            var submissionsAvailable = await _submissionRepository.GetAllSubmissionsAsync();
            var candidateSubmission = submissionsAvailable.FirstOrDefault(s => s.CandidateId == candidateFound.Id);
            if (candidateSubmission == null)
            {
                return NotFound("No submission found for candidate with id " + candidateFound.Id);
            }

            var hiringProcess = await _hiringRepository.GetHiringProcessBySubmissionId(candidateSubmission.SubmissionId);
            if (hiringProcess == null)
            {
                return NotFound("No hiring process found for submission with id " + candidateSubmission.SubmissionId);
            }

            hiringProcess.CandidateSituation = CandidateSituation.UnderReview;
            hiringProcess.Mentions = candidateSituationResetViewModel.Reason;
            this._hiringRepository.UpdateHiringProcess(hiringProcess);
            await this._hiringRepository.SaveChangesAsync();

            await _notificationService.PostResetNotificationToManager(candidateSituationResetViewModel.SenderId, candidateFound.Id, null);

            var positionsFound = await _positionRepository.GetAllPositionsAsync();
            var candidateApplicationPosition = positionsFound.FirstOrDefault(p => p.PositionId == candidateSubmission.PositionId);

            HiringViewModel hiringModel = new HiringViewModel() { CandidateSituation = hiringProcess.CandidateSituation.ToString() };

            return Ok("The candidate situation was succesfully updated to default");
        }

        [HttpGet]
        [Route("properties")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<IEnumerable<string>>> GetAllHiringFormFieldsAsync()
        {
            var lastStateProperties = await _stateSeqRepository.GetAllFieldsStateAsync();
            var lastStatePropertiesOrdered = lastStateProperties.OrderByDescending(s => s.Id).FirstOrDefault();
            if (lastStatePropertiesOrdered == null)
            {
                var availableHiringProperties = typeof(UserRegisterViewModel).GetProperties();

                var propertiesName = new List<string>();

                foreach (var property in availableHiringProperties)
                {
                    if (!property.Name.Contains("Available") && property.Name != "PositionId" && property.Name != "Role")
                    {
                        propertiesName.Add(property.Name);
                    }
                }

                if (propertiesName == null) return NotFound("No fields are found in API");

                return Ok(propertiesName);
            }

            var finalList = new List<string>();

            foreach (PropertyInfo lastStateProperty in lastStatePropertiesOrdered.GetType().GetProperties())
            {
                if (lastStateProperty.Name != "Id" && lastStateProperty.Name != "StateId" && lastStateProperty.Name != "State" && lastStateProperty.Name != "Sequences")
                {
                    if (lastStateProperty.GetValue(lastStatePropertiesOrdered, null).ToString() == "false" || lastStateProperty.GetValue(lastStatePropertiesOrdered, null).ToString() == "False")
                    {
                        finalList.Add(lastStateProperty.Name);
                    }
                }
            }

            return Ok(finalList);
        }

        [HttpGet]
        [Route("currentFields")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<IEnumerable<string>>> GetCurrentFormFieldsAsync()
        {
            var lastSaved = await _stateSeqRepository.GetAllFieldsStateAsync();
            var lastSavedOrdered = lastSaved.OrderByDescending(s => s.Id).FirstOrDefault();

            if (lastSavedOrdered == null)
            {
                /*                var availableHiringProperties = typeof(UserRegisterViewModel).GetProperties();
                                var propertiesName = new List<string>();

                                foreach (var propertyName in availableHiringProperties)
                                {
                                    propertiesName.Add(propertyName.Name);
                                }

                                if (propertiesName == null)
                                    return NotFound("No properties found in the database");

                                return Ok(propertiesName);*/

                return NotFound("No last saved state found in the database");
            }

            var availableFields = new List<string>();
            foreach (PropertyInfo propertyInfo in lastSavedOrdered.GetType().GetProperties())
            {
                var property = propertyInfo.GetValue(lastSavedOrdered, null);
                if (property != null)
                {
                    if (property.ToString() == "true" || property.ToString() == "True")
                    {
                        availableFields.Add(propertyInfo.Name);
                    }
                }
            }

            var lastSavedSequence = await _stateSeqRepository.GetAllFieldsSequencesAsync();
            var lastSavedSequenceOrdered = lastSavedSequence.OrderByDescending(seq => seq.Id).FirstOrDefault();
            if (lastSavedSequenceOrdered != null)
            {
                var sequenceList = new SortedList();
                foreach (PropertyInfo propertyInfo in lastSavedSequenceOrdered.GetType().GetProperties())
                {
                    if (propertyInfo.Name != "Id" && propertyInfo.Name != "StateId" && propertyInfo.Name != "State")
                    {
                        var property = propertyInfo.GetValue(lastSavedSequenceOrdered, null);
                        if (int.Parse(property.ToString()) > 0)
                        {
                            sequenceList.Add(int.Parse(property.ToString()), propertyInfo.Name);
                        }

                    }
                }

                if (sequenceList == null)
                    return NotFound("No content found in sequenceList");

                return Ok(sequenceList);
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("properties/set")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> PostNewFieldsDataAsync([FromBody] Dictionary<int, string> selectedFields)
        {
            var userRegisterType = typeof(UserRegisterViewModel);

            var fieldsDatabaseState = new Dictionary<string, bool>();
            var fieldsDatabaseSequence = new Dictionary<int, string>();

            foreach (KeyValuePair<int, string> selectedField in selectedFields)
            {
                if (userRegisterType.GetProperty(selectedField.Value) != null)
                {
                    /*var propertyFound = userRegisterType.GetProperty(selectedField.Value).Name;
                    SetBrowsableAttributeValue(propertyFound, true);
                    fieldsDatabaseState.Add(nameof(propertyFound), true);
                    fieldsDatabaseSequence.Add(selectedField.Key, nameof(propertyFound));*/

                    var propertyFound = userRegisterType.GetProperty(selectedField.Value + "Available");
                    fieldsDatabaseState.Add(selectedField.Value, true);
                    fieldsDatabaseSequence.Add(selectedField.Key, selectedField.Value);
                }
            }

            if (fieldsDatabaseState == null)
                return NotFound();

            if (fieldsDatabaseSequence == null)
                return NotFound();

            var stateType = typeof(State);
            var stateProperties = stateType.GetProperties();

            var stateObj = new State();
            var sequenceObj = new Sequence();

            foreach (KeyValuePair<string, bool> keyValue in fieldsDatabaseState)
            {
                foreach (var stateProperty in stateProperties)
                {
                    if (stateProperty.Name == keyValue.Key)
                    {
                        var objState = stateObj.GetType().GetProperty(stateProperty.Name);
                        objState.SetValue(stateObj, keyValue.Value, null);
                    }
                }
            }

            await _stateSeqRepository.AddFieldStateAsync(stateObj);
            await _stateSeqRepository.SaveChangesAsync();

            var lastFieldState = await _stateSeqRepository.GetAllFieldsStateAsync();
            var lastFieldStateOrdered = lastFieldState.OrderByDescending(s => s.Id).FirstOrDefault();

            foreach (KeyValuePair<int, string> keyValue in fieldsDatabaseSequence)
            {
                /*foreach(var state in stateProperties)
                {
                    if(keyValue.Value == state.Name)
                    {
                        sequenceObj.GetType().GetProperty(keyValue.Value).SetValue(keyValue.Key, null);
                    }
                }*/

                foreach (var stateProperty in stateProperties)
                {
                    if (stateProperty.Name == keyValue.Value)
                    {
                        var seqObject = sequenceObj.GetType().GetProperty(keyValue.Value);
                        seqObject.SetValue(sequenceObj, keyValue.Key + 1, null);
                    }
                }
            }

            sequenceObj.StateId = lastFieldStateOrdered.Id;

            await _stateSeqRepository.AddFieldsSequenceAsync(sequenceObj);
            await _stateSeqRepository.SaveChangesAsync();

            return Ok("Succesfully sent new settings for submission form");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("search")]
        public async Task<ActionResult<IEnumerable<SubmissionViewModel>>> SearchByKeyAsync(SearchKeyDTO id)
        {
            if (id.key == "")
            {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                return Ok(submissions);
            }

            if (id.key is string)
            {
                string searchKey = id.key as string;
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();

                if (IsAlphaNumeric(id.key) == true)
                {
                    bool isNumeric = int.TryParse(id.key, out _);

                    if (isNumeric == true)
                    {
                        foreach (var submission in submissions)
                        {
                            if (submission.DateOfSubmission.Date.Day.ToString().Contains(id.key) || submission.DateOfSubmission.Date.Month.ToString().Contains(id.key)
                                || submission.DateOfSubmission.Date.Year.ToString().Contains(id.key))
                            {
                                var userFound = await _userRepository.GetUserById(submission.CandidateId);
                                var positionFound = _positionRepository.GetPositionByIdAsync(submission.PositionId).Result;
                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    CandidateId = submission.CandidateId,
                                    User = userFound,
                                    AppliedPosition = positionFound,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    Mentions = submission.Mentions,
                                    Status = submission.Status,
                                    CV = submission.CV,
                                    SubmissionId = submission.SubmissionId
                                });
                            }
                        }

                        return Ok(submissionsFinalList);
                    }

                    foreach (var submission in submissions)
                    {
                        var userFound = await _userRepository.GetUserById(submission.CandidateId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                        if (userFound.FirstName.ToLower().Contains(searchKey) || userFound.LastName.ToLower().Contains(searchKey) || positionFound.PositionName.ToLower().Contains(searchKey))
                        {
                            submissionsFinalList.Add(new SubmissionViewModel()
                            {
                                CandidateId = submission.CandidateId,
                                User = userFound,
                                AppliedPosition = positionFound,
                                DateOfSubmission = submission.DateOfSubmission,
                                Mentions = submission.Mentions,
                                Status = submission.Status,
                                CV = submission.CV,
                                SubmissionId = submission.SubmissionId
                            });
                        }
                    }

                    return Ok(submissionsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/name")]
        public async Task<ActionResult<SubmissionViewModel>> SearchByNameFilterAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                return Ok(submissions);
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var submission in submissions)
                    {
                        var userFound = await _userRepository.GetUserById(submission.CandidateId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                        if (userFound.FirstName.ToLower().Contains(searchKey) || userFound.LastName.ToLower().Contains(searchKey))
                        {
                            submissionsFinalList.Add(new SubmissionViewModel()
                            {
                                CandidateId = submission.CandidateId,
                                User = userFound,
                                AppliedPosition = positionFound,
                                DateOfSubmission = submission.DateOfSubmission,
                                Mentions = submission.Mentions,
                                Status = submission.Status,
                                CV = submission.CV,
                                SubmissionId = submission.SubmissionId
                            });
                        }
                    }

                    return Ok(submissionsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/position")]
        public async Task<ActionResult<IEnumerable<Submission>>> SearchByPositionNameAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                return Ok(submissions);
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var submission in submissions)
                    {
                        var userFound = await _userRepository.GetUserById(submission.CandidateId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                        if (positionFound.PositionName.ToLower().Contains(searchKey))
                        {
                            submissionsFinalList.Add(new SubmissionViewModel()
                            {
                                CandidateId = submission.CandidateId,
                                User = userFound,
                                AppliedPosition = positionFound,
                                DateOfSubmission = submission.DateOfSubmission,
                                Mentions = submission.Mentions,
                                Status = submission.Status,
                                CV = submission.CV,
                                SubmissionId = submission.SubmissionId
                            });
                        }
                    }

                    return Ok(submissionsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/date")]
        public async Task<ActionResult<IEnumerable<SubmissionViewModel>>> SearchSubmissionsByDatesAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate == "")
            {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                return Ok(submissions);
            }

            if (searchKeyDTO.startDate is string && searchKeyDTO.lastDate is string)
            {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinalList = new List<SubmissionViewModel>();

                if(searchKeyDTO.startDate != "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1, d2;

                    if (DateTime.TryParse(searchKeyDTO.startDate, out d1) && DateTime.TryParse(searchKeyDTO.lastDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            var userFound = await _userRepository.GetUserById(submission.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                            if (submission.DateOfSubmission.Date >= d1 && submission.DateOfSubmission.Date <= d2)
                            {
                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    CandidateId = submission.CandidateId,
                                    User = userFound,
                                    AppliedPosition = positionFound,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    Mentions = submission.Mentions,
                                    Status = submission.Status,
                                    CV = submission.CV,
                                    SubmissionId = submission.SubmissionId
                                });
                            }
                        }
                        return Ok(submissionsFinalList);
                    }
                }

                if(searchKeyDTO.startDate == "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1;
                    if(DateTime.TryParse(searchKeyDTO.lastDate, out d1))
                    {
                        foreach (var submission in submissions)
                        {
                            var userFound = await _userRepository.GetUserById(submission.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                            if(submission.DateOfSubmission <= d1)
                            {
                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    CandidateId = submission.CandidateId,
                                    User = userFound,
                                    AppliedPosition = positionFound,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    Mentions = submission.Mentions,
                                    Status = submission.Status,
                                    CV = submission.CV,
                                    SubmissionId = submission.SubmissionId
                                });
                            }
                        }
                    }
                }

                if(searchKeyDTO.startDate != "" && searchKeyDTO.lastDate == "")
                {
                    DateTime d2;
                    if(DateTime.TryParse(searchKeyDTO.startDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            var userFound = await _userRepository.GetUserById(submission.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                            if(submission.DateOfSubmission >= d2)
                            {
                                submissionsFinalList.Add(new SubmissionViewModel()
                                {
                                    CandidateId = submission.CandidateId,
                                    User = userFound,
                                    AppliedPosition = positionFound,
                                    DateOfSubmission = submission.DateOfSubmission,
                                    Mentions = submission.Mentions,
                                    Status = submission.Status,
                                    CV = submission.CV,
                                    SubmissionId = submission.SubmissionId
                                });
                            }
                        }

                        return Ok(submissionsFinalList);
                    }
                }

                
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("search/interview")]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> SearchByKeyInterviewsAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var retrievedInterviews = await _interviewRepository.GetAllInterviewsAsync();
                if (retrievedInterviews == null)
                    return NotFound("No interviews found in the database");
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var interviews = await _interviewRepository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews found in the database");

                var interviewsFinalList = new List<InterviewViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                    {
                        foreach(var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                            if(interview.InterviewDate.Date.Day.ToString().Contains(searchKey) || interview.InterviewDate.Date.Month.ToString().Contains(searchKey) 
                                || interview.InterviewDate.Date.Year.ToString().Contains(searchKey)) 
                            {
                                interviewsFinalList.Add(new InterviewViewModel()
                                {
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    InterviewId = interview.InterviewId,
                                    PositionName = positionFound.PositionName,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }

                        return Ok(interviewsFinalList);
                    }


                    foreach (var interview in interviews)
                    {
                        var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                        var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                        var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                        if (candidateFound.FirstName.ToLower().Contains(searchKey) || candidateFound.LastName.ToLower().Contains(searchKey) || positionFound.PositionName.ToLower().Contains(searchKey))
                        {
                            interviewsFinalList.Add(new InterviewViewModel()
                            {
                                CandidateFirstName = candidateFound.FirstName,
                                CandidateLastName = candidateFound.LastName,
                                CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                CommunicationChannel = interview.CommunicationChannel.ToString(),
                                InterviewDate = interview.InterviewDate,
                                InterviewId = interview.InterviewId,
                                PositionName = positionFound.PositionName,
                                QuizAvailable = interview.QuizAvailable
                            });
                        }
                    }

                    return Ok(interviewsFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/interview/name")]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> SearchByNameFilterInterviewAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var interviews = await _interviewRepository.GetAllInterviewsAsync();
                return Ok(interviews);
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var interviews = await _interviewRepository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No interviews was found in the database");

                var interviewsFinalList = new List<InterviewViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var interview in interviews)
                    {
                        var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                        var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                        var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                        if (candidateFound.FirstName.ToLower().Contains(searchKey) || candidateFound.LastName.ToLower().Contains(searchKey))
                        {
                            interviewsFinalList.Add(new InterviewViewModel()
                            {
                                CandidateFirstName = candidateFound.FirstName,
                                CandidateLastName = candidateFound.LastName,
                                CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                CommunicationChannel = interview.CommunicationChannel.ToString(),
                                InterviewDate = interview.InterviewDate,
                                InterviewId = interview.InterviewId,
                                PositionName = positionFound.PositionName,
                                QuizAvailable = interview.QuizAvailable
                            });
                        }
                    }

                    return Ok(interviewsFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/interview/position")]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> SearchByFilterPositionAsync(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == "")
            {
                var interviewsFound = await _interviewRepository.GetAllInterviewsAsync();
                if (interviewsFound == null)
                    return NotFound("No interviews was found in the database");
            }

                if(searchKeyDTO.key is string)
                {
                    string searchKey = searchKeyDTO.key as string;
                    var interviews = await _interviewRepository.GetAllInterviewsAsync();
                    if (interviews == null)
                        return NotFound("No interviews was found in the database");

                    var interviewsFinalList = new List<InterviewViewModel>();

                    if (IsAlphaNumeric(searchKeyDTO.key) == true)
                    {
                        bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                        if (isNumeric == true)
                            return BadRequest("Cannot filter the names by numbers");

                        foreach(var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                            if(positionFound.PositionName.ToLower().Contains(searchKey))
                            {
                                interviewsFinalList.Add(new InterviewViewModel()
                                {
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    InterviewId = interview.InterviewId,
                                    PositionName = positionFound.PositionName,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }

                        return Ok(interviewsFinalList);
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/interview/date")]
        public async Task<ActionResult<IEnumerable<InterviewViewModel>>> SearchInterviewsByDateFilterAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate == "")
            {
                var interviews = await _interviewRepository.GetAllInterviewsAsync();
                return Ok(interviews);
            }

            if (searchKeyDTO.startDate is string && searchKeyDTO.lastDate is string)
            {
                var interviews = await _interviewRepository.GetAllInterviewsAsync();
                if (interviews == null)
                    return NotFound("No submission was found in the database");

                var interviewsFinalList = new List<InterviewViewModel>();

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1, d2;

                    if (DateTime.TryParse(searchKeyDTO.startDate, out d1) && DateTime.TryParse(searchKeyDTO.lastDate, out d2))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                            if (interview.InterviewDate.Date >= d1 && interview.InterviewDate.Date <= d2)
                            {
                                interviewsFinalList.Add(new InterviewViewModel()
                                {
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    InterviewId = interview.InterviewId,
                                    PositionName = positionFound.PositionName,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }
                        return Ok(interviewsFinalList);
                    }
                }

                if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1;
                    if (DateTime.TryParse(searchKeyDTO.lastDate, out d1))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                            if (interview.InterviewDate.Date <= d1)
                            {
                                interviewsFinalList.Add(new InterviewViewModel()
                                {
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    InterviewId = interview.InterviewId,
                                    PositionName = positionFound.PositionName,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }
                    }
                }

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate == "")
                {
                    DateTime d2;
                    if (DateTime.TryParse(searchKeyDTO.startDate, out d2))
                    {
                        foreach (var interview in interviews)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessByInterviewId(interview.InterviewId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);
                            if (interview.InterviewDate.Date >= d2)
                            {
                                interviewsFinalList.Add(new InterviewViewModel()
                                {
                                    CandidateFirstName = candidateFound.FirstName,
                                    CandidateLastName = candidateFound.LastName,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    CommunicationChannel = interview.CommunicationChannel.ToString(),
                                    InterviewDate = interview.InterviewDate,
                                    InterviewId = interview.InterviewId,
                                    PositionName = positionFound.PositionName,
                                    QuizAvailable = interview.QuizAvailable
                                });
                            }
                        }

                        return Ok(interviewsFinalList);
                    }
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }


        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("search/employees")]
        public async Task<ActionResult<IEnumerable<Data.DTO.AdministrativeDTO.UserViewModel>>> FilterEmployeesByKeyAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var retrievedEmployees = await _userRepository.GetAllUsers();
                if (retrievedEmployees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach(var employee in retrievedEmployees)
                {
                    if(employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }
                if (availableEmployees == null)
                    return NotFound("No employees found in the database");
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var employees = await _userRepository.GetAllUsers();
                if (employees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach(var employee in employees)
                {
                    if (employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }

                var employeesFinalList = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var employee in availableEmployees)
                    {
                        var positionFound = await _positionRepository.GetPositionByIdAsync(employee.Position.PositionId);
                        if (employee.FirstName.ToLower().ToString().Contains(searchKey) || employee.LastName.ToLower().ToString().Contains(searchKey)
                            || positionFound.PositionName.ToLower().ToString().Contains(searchKey))
                        {
                            employeesFinalList.Add(new Data.DTO.AdministrativeDTO.UserViewModel()
                            {
                                FirstName = employee.FirstName,
                                LastName = employee.LastName,
                                Address = employee.Address,
                                DateOfBirth = employee.DateOfBirth,
                                Email = employee.Email,
                                Position = positionFound,
                                ProfilePhotoUrl = employee.ProfilePhotoUrl,
                                Id = employee.Id,
                                Mentions = employee.Mentions,
                                Role = employee.Role
                            });
                        }
                    }

                    return Ok(employeesFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }


        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/employee_name")]
        public async Task<ActionResult<IEnumerable<Data.DTO.AdministrativeDTO.UserViewModel>>> CustomSearchByEmployeeNameAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var retrievedEmployees = await _userRepository.GetAllUsers();
                if (retrievedEmployees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach (var employee in retrievedEmployees)
                {
                    if (employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }
                if (availableEmployees == null)
                    return NotFound("No employees found in the database");
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var employees = await _userRepository.GetAllUsers();
                if (employees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach (var employee in employees)
                {
                    if (employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }

                var employeesFinalList = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var employee in availableEmployees)
                    {
                        var positionFound = await _positionRepository.GetPositionByIdAsync(employee.Position.PositionId);
                        if (employee.FirstName.ToLower().ToString().Contains(searchKey.ToLower()) || employee.LastName.ToLower().ToString().Contains(searchKey.ToLower()))
                        {
                            employeesFinalList.Add(new Data.DTO.AdministrativeDTO.UserViewModel()
                            {
                                FirstName = employee.FirstName,
                                LastName = employee.LastName,
                                Address = employee.Address,
                                DateOfBirth = employee.DateOfBirth,
                                Email = employee.Email,
                                Position = positionFound,
                                ProfilePhotoUrl = employee.ProfilePhotoUrl,
                                Id = employee.Id,
                                Mentions = employee.Mentions,
                                Role = employee.Role
                            });
                        }
                    }

                    return Ok(employeesFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles ="Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/employee/position")]
        public async Task<ActionResult<IEnumerable<Data.DTO.AdministrativeDTO.UserViewModel>>> CustomSearchByPositionNameAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.key == "")
            {
                var retrievedEmployees = await _userRepository.GetAllUsers();
                if (retrievedEmployees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach (var employee in retrievedEmployees)
                {
                    if (employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }
                if (availableEmployees == null)
                    return NotFound("No employees found in the database");
            }

            if (searchKeyDTO.key is string)
            {
                string searchKey = searchKeyDTO.key as string;
                var employees = await _userRepository.GetAllUsers();
                if (employees == null)
                    return NotFound("No employees found in the database");

                var availableEmployees = new List<Data.DTO.AdministrativeDTO.UserViewModel>();
                foreach (var employee in employees)
                {
                    if (employee.Role != "Candidate registered" && employee.Role != "Anonymous" && employee.Role != "Administrator")
                    {
                        availableEmployees.Add(employee);
                    }
                }

                var employeesFinalList = new List<Data.DTO.AdministrativeDTO.UserViewModel>();

                if (IsAlphaNumeric(searchKeyDTO.key) == true)
                {
                    bool isNumeric = int.TryParse(searchKeyDTO.key, out _);

                    if (isNumeric == true)
                        return BadRequest("Cannot filter the names by numbers");

                    foreach (var employee in availableEmployees)
                    {
                        var positionFound = await _positionRepository.GetPositionByIdAsync(employee.Position.PositionId);
                        if (positionFound.PositionName.ToLower().Contains(searchKey.ToLower()))
                        {
                            employeesFinalList.Add(new Data.DTO.AdministrativeDTO.UserViewModel()
                            {
                                FirstName = employee.FirstName,
                                LastName = employee.LastName,
                                Address = employee.Address,
                                DateOfBirth = employee.DateOfBirth,
                                Email = employee.Email,
                                Position = positionFound,
                                ProfilePhotoUrl = employee.ProfilePhotoUrl,
                                Id = employee.Id,
                                Mentions = employee.Mentions,
                                Role = employee.Role
                            });
                        }
                    }

                    return Ok(employeesFinalList);
                }
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/candidates/name")]
        public async Task<ActionResult<IEnumerable<CandidateViewModel>>> CustomSearchByCandidateSubmissionsAsync(SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == null) {
                var submissions = await _submissionRepository.GetAllSubmissionsAsync();
                var candidatesFound = new List<CandidateViewModel>();
                foreach(var submission in submissions)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessBySubmissionId(submission.SubmissionId);
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submission.SubmissionId);
                    if(candidateFound != null)
                    {
                        candidatesFound.Add(new CandidateViewModel()
                        {
                             FirstName = candidateFound.FirstName,
                             LastName = candidateFound.LastName,
                             Address = candidateFound.Address,
                             DateOfBirth = candidateFound.DateOfBirth,
                             Email = candidateFound.Email,
                             CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                             PositionName = positionFound.PositionName
                        });
                    }
                }
                if (candidatesFound == null)
                    return NotFound("No candidates was found in the database");

                return Ok(candidatesFound);
            }

            if(searchKeyDTO.key is string)
            {
                var searchKey = searchKeyDTO.key as string;
                var submissions = await _submissionRepository.GetAllSubmissionsOfficialAsync();

                if (submissions == null)
                    return NotFound("No submissions was found in the database");

                var submissionsFinalList = new List<CandidateViewModel>();
                foreach(var submission in submissions)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    if(candidateFound.FirstName.ToLower().ToString().Contains(searchKey.ToLower()) || candidateFound.LastName.ToLower().ToString().Contains(searchKey.ToLower()))
                    {
                        var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(candidateFound.Id);
                        var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submissionFound.HiringId);
                        var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                        submissionsFinalList.Add(new CandidateViewModel() {
                            FirstName = candidateFound.FirstName,
                            LastName = candidateFound.LastName,
                            Address = candidateFound.Address,
                            DateOfBirth = candidateFound.DateOfBirth,
                            Email = candidateFound.Email,
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            PositionName = positionFound.PositionName
                        });
                    }
                }

                return Ok(submissionsFinalList);
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [Authorize(Roles = "Administrator, Recruiter, Manager departament resurse umane")]
        [HttpPost("filter/candidates/position")]
        public async Task<ActionResult<IEnumerable<CandidateViewModel>>> CustomSearchFilterByPositionAsync (SearchKeyDTO searchKeyDTO)
        {
            if(searchKeyDTO.key == null)
            {
                var submissions = await _submissionRepository.GetAllSubmissionsOfficialAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinal = new List<CandidateViewModel>();

                foreach(var submission in submissions)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                    if (candidateFound != null)
                        submissionsFinal.Add(new CandidateViewModel()
                        {
                            FirstName = candidateFound.FirstName,
                            LastName = candidateFound.LastName,
                            Address = candidateFound.Address,
                            DateOfBirth = candidateFound.DateOfBirth,
                            Email = candidateFound.Email,
                            SubmissionDate = submission.DateOfSubmission,
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            PositionName = positionFound.PositionName
                        });
                }

                if (submissionsFinal == null)
                    return NotFound("No submission was found in database");

                return Ok(submissionsFinal);
            }

            if(searchKeyDTO.key is string)
            {
                var searchKey = searchKeyDTO.key as string;
                var submissions = await _submissionRepository.GetAllSubmissionsOfficialAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinal = new List<CandidateViewModel>();

                foreach(var submission in submissions)
                {
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);

                    if (positionFound.PositionName.ToLower().ToString().Contains(searchKey.ToLower()))
                    {
                        submissionsFinal.Add(new CandidateViewModel()
                        {
                            FirstName = candidateFound.FirstName,
                            LastName = candidateFound.LastName,
                            Address = candidateFound.Address,
                            DateOfBirth = candidateFound.DateOfBirth,
                            Email = candidateFound.Email,
                            SubmissionDate = submission.DateOfSubmission,
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            PositionName = positionFound.PositionName
                        });
                    }
                }

                if (submissionsFinal == null)
                    return NotFound("No submissions was found in the database");

                return Ok(submissionsFinal);
            }
            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [HttpPost("request_password_urgent")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> SendRequestPasswordChange([FromBody] ForgotPasswordViewModel forgotPasswodView)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await _userManager.FindByEmailAsync(forgotPasswodView.Email);
            if (user == null) return BadRequest("Invalid request");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            token = HttpUtility.UrlEncode(token);
            string callBack = "http://localhost:4200" + "/reset-password/" + user.Id + "/" + token;

            var crtTime = DateTime.UtcNow;

            string body = string.Format("<div>&nbsp;</div>" +
                $"<div><p>Hello, {user.FirstName + " " + user.LastName}, </p>" +
                $"<p>A password change request was sent to us at {crtTime} from your account</p>" +
                $"<p>Press the button provided below to be redirected to our application, valid for 24 hours</p>" + 
                $"<p>If you are not the sender of this request, ignore this e-mail till new informations are provided by us" +
                $"<div><p><a href=\"" + callBack + "\" style=\"color: #fff; background-color= \"#000 \"> Change password </a></p>");

            var message = new MessageViewModel()
            {
                Email = user.Email,
                Callback = body
            };

            _mailServices.SendPasswordRequest(message);
            return Ok("Password forms was succesfully sent to the users");
        }

        [HttpPost("request_password_generated")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> GenerateSecurityPasswords([FromBody] ForgotPasswordViewModel forgotPasswordView)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await _userManager.FindByEmailAsync(forgotPasswordView.Email);
            if (user == null) return BadRequest("Invalid request");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);


            var pwd = new Password().IncludeLowercase().IncludeUppercase().IncludeSpecial().IncludeNumeric().LengthRequired(16);
            var generatedPassword = pwd.Next();


            await _userManager.ResetPasswordAsync(user, token, generatedPassword);

            var message = new MessageViewModel()
            {
                Email = user.Email,
                Callback = $"Hello, {user.FirstName} {user.LastName}. After a security breach in our application we've decided that your password has been compromised." +
                $" Your new password is: {generatedPassword}. You can reset it in profile section inside the app." +
                $" Have a nice day!" +
                $" iManagement Team"
            };

            _mailServices.SendPasswordRequest(message);
            return Ok("Passwords was succesfully generated and sent to the users by e-mail");
        }

        [HttpPost("reset_password")]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword([FromBody] ResetPasswordViewModel resetPasswordView)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await _userManager.FindByEmailAsync(resetPasswordView.Email);

            await _userManager.ResetPasswordAsync(user, resetPasswordView.Token, resetPasswordView.NewPassword);

            return Ok();
        }

        [HttpDelete("remove_employees")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveAllEmployeesAsync()
        {
            var employees = await _userManager.GetUsersInRoleAsync("Employee");
            if (employees == null) return NotFound();

            foreach(var employee in employees)
            {
                _userRepository.RemoveUser(employee);
                await _userRepository.SaveChangesAsync();
            }

            var checkEmployees = await _userManager.GetUsersInRoleAsync("Employee");
            if (checkEmployees.Count == 0) return Ok("All employees was succesfully removed from database");

            return BadRequest();
        }

        [HttpDelete("remove_unretrieved")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveUnretrievedUsersAsync()
        {
            var unretrievedUsers = await _userManager.GetUsersInRoleAsync("Not retrieved");
            if (unretrievedUsers == null) return BadRequest();

            var administrator = await _userManager.GetUsersInRoleAsync("Administrator");
            if (administrator == null) return BadRequest();

            foreach(var unretrievedUser in unretrievedUsers)
            {
                _userRepository.RemoveUser(unretrievedUser);
                await _userRepository.SaveChangesAsync();
                _mailServices.SendEmailFromAdmin(unretrievedUser, administrator[0], "", "");
            }

            var checkUnretrievedUsers = await _userManager.GetUsersInRoleAsync("Not retrieved");
            if (checkUnretrievedUsers.Count == 0) return Ok("All not retrieved users was succesfully removed from database");

            return BadRequest();
        }

        [HttpPost("create_role")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> CreateNewRoleAsync([FromBody] AppRoleViewModel appRoleView)
        {
            await _roleManager.CreateAsync(new IdentityRole(appRoleView.RoleName));

            return Ok("User role was succesfully created and added to database");
        }


        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpPost("filter/candidates/date")]
        public async Task<ActionResult<IEnumerable<CandidateViewModel>>> CustomSearchByDateAsync(SearchKeyDTO searchKeyDTO)
        {
            if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate == "")
            {
                var submissions = await _submissionRepository.GetAllSubmissionsOfficialAsync();
                if (submissions == null)
                    return NotFound("No submission was found in the database");

                var submissionsFinal = new List<CandidateViewModel>();

                foreach (var submission in submissions)
                {
                    var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                    var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);
                    var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                    if (candidateFound != null)
                        submissionsFinal.Add(new CandidateViewModel()
                        {
                            FirstName = candidateFound.FirstName,
                            LastName = candidateFound.LastName,
                            Address = candidateFound.Address,
                            DateOfBirth = candidateFound.DateOfBirth,
                            Email = candidateFound.Email,
                            SubmissionDate = submission.DateOfSubmission,
                            CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                            PositionName = positionFound.PositionName
                        });
                }

                if (submissionsFinal == null)
                    return NotFound("No submission was found in database");

                return Ok(submissionsFinal);
            }

            if (searchKeyDTO.startDate is string && searchKeyDTO.lastDate is string)
            {
                var submissions = await _submissionRepository.GetAllSubmissionsOfficialAsync();
                if (submissions == null)
                    return NotFound("No submissions was found in the database");

                var submissionsFinal = new List<CandidateViewModel>();

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1, d2;

                    if (DateTime.TryParse(searchKeyDTO.startDate, out d1) && DateTime.TryParse(searchKeyDTO.lastDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);
                            var submissionFound = await _submissionRepository.GetSubmissionByIdAsync(hiringProcessFound.SubmissionId);
                            var candidateFound = await _userRepository.GetUserById(submissionFound.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submissionFound.PositionId);

                            if (submission.DateOfSubmission.Date >= d1 && submission.DateOfSubmission.Date <= d2)
                            {
                                submissionsFinal.Add(new CandidateViewModel()
                                {
                                    FirstName = candidateFound.FirstName,
                                    LastName = candidateFound.LastName,
                                    Address = candidateFound.Address,
                                    DateOfBirth = candidateFound.DateOfBirth,
                                    Email = candidateFound.Email,
                                    SubmissionDate = submission.DateOfSubmission,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    PositionName = positionFound.PositionName
                                });
                            }
                        }
                        return Ok(submissionsFinal);
                    }
                    return Ok(submissionsFinal);
                }

                if (searchKeyDTO.startDate == "" && searchKeyDTO.lastDate != "")
                {
                    DateTime d1;
                    if (DateTime.TryParse(searchKeyDTO.lastDate, out d1))
                    {
                        foreach (var submission in submissions)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);
                            var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);
                            if (submission.DateOfSubmission.Date <= d1)
                            {
                                submissionsFinal.Add(new CandidateViewModel()
                                {
                                    FirstName = candidateFound.FirstName,
                                    LastName = candidateFound.LastName,
                                    Address = candidateFound.Address,
                                    DateOfBirth = candidateFound.DateOfBirth,
                                    Email = candidateFound.Email,
                                    SubmissionDate = submission.DateOfSubmission,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    PositionName = positionFound.PositionName
                                });
                            }
                        }
                    }
                }

                if (searchKeyDTO.startDate != "" && searchKeyDTO.lastDate == "")
                {
                    DateTime d2;
                    if (DateTime.TryParse(searchKeyDTO.startDate, out d2))
                    {
                        foreach (var submission in submissions)
                        {
                            var hiringProcessFound = await _hiringRepository.GetHiringProcessById(submission.HiringId);
                            var candidateFound = await _userRepository.GetUserById(submission.CandidateId);
                            var positionFound = await _positionRepository.GetPositionByIdAsync(submission.PositionId);

                            if (submission.DateOfSubmission.Date >= d2)
                            {
                                submissionsFinal.Add(new CandidateViewModel()
                                {
                                    FirstName = candidateFound.FirstName,
                                    LastName = candidateFound.LastName,
                                    Address = candidateFound.Address,
                                    DateOfBirth = candidateFound.DateOfBirth,
                                    Email = candidateFound.Email,
                                    SubmissionDate = submission.DateOfSubmission,
                                    CandidateSituation = hiringProcessFound.CandidateSituation.ToString(),
                                    PositionName = positionFound.PositionName
                                });
                            }
                        }
                        return Ok(submissionsFinal);
                    }
                }
            }

            return BadRequest("Cannot search in Submissions database for records. Try with another set of characters");
        }

        [HttpGet("employees_report")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GenerateEmployeeExcelAsync()
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            var employees = await _userManager.GetUsersInRoleAsync("Employee");
            if (employees == null) return BadRequest();

            MemoryStream memoryStream = new MemoryStream();
            using (ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Submissions");
                int index = 1;

                worksheet.Cells["B1"].Value = "Complete name";
                worksheet.Cells["B1"].Style.Font.Bold = true;
                worksheet.Cells["B1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["C1"].Value = "Birth date";
                worksheet.Cells["C1"].Style.Font.Bold = true;
                worksheet.Cells["C1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["C1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["D1"].Value = "Home address";
                worksheet.Cells["D1"].Style.Font.Bold = true;
                worksheet.Cells["D1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["D1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["E1"].Value = "Email address";
                worksheet.Cells["E1"].Style.Font.Bold = true;
                worksheet.Cells["E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["F1"].Value = "Phone number";
                worksheet.Cells["F1"].Style.Font.Bold = true;
                worksheet.Cells["F1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["F1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["G1"].Value = "Occupied position";
                worksheet.Cells["G1"].Style.Font.Bold = true;
                worksheet.Cells["G1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["G1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                worksheet.Cells["H1"].Value = "Security Question";
                worksheet.Cells["H1"].Style.Font.Bold = true;
                worksheet.Cells["H1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["H1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                foreach (var employee in employees)
                {
                    var position = await _positionRepository.GetPositionByIdAsync(employee.PositionId);

                    worksheet.Cells["B" + (index + 1)].Value = employee.FirstName + " " + employee.LastName;
                    worksheet.Cells["C" + (index + 1)].Value = employee.DateOfBirth.ToString("dd/MM/yyyy");
                    worksheet.Cells["D" + (index + 1)].Value = employee.Address;
                    worksheet.Cells["E" + (index + 1)].Value = employee.Email;
                    worksheet.Cells["F" + (index + 1)].Value = employee.PhoneNumber;
                    worksheet.Cells["G" + (index + 1)].Value = position.PositionName;
                    worksheet.Cells["H" + (index + 1)].Value = employee.SecurityQuestion;

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                excelPackage.Save();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "Employees.xlsx");
        }

        [HttpGet("participants")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllParticipantsAsync()
        {
            var participants = await _interviewRepository.GetAllInterviewsParticipantsAsync();
            if (participants == null) return BadRequest();

            var participantsModel = new List<ParticipantViewModel>();
            
            foreach(var participant in participants)
            {
                var interview = await _interviewRepository.GetInterviewByIdAsync(participant.InterviewId);
                var participantF = await _userRepository.GetUserById(participant.EmployeeId);
                var hiring = await _hiringRepository.GetHiringProcessById(interview.HiringId);
                var submission = await _submissionRepository.GetSubmissionByIdAsync(hiring.SubmissionId);
                var position = await _positionRepository.GetPositionByIdAsync(participantF.PositionId);
                var candidate = await _userRepository.GetUserById(submission.CandidateId);

               participantsModel.Add(new ParticipantViewModel()
               {
                    Id = participantF.Id,
                    FirstName = participantF.FirstName,
                    LastName = participantF.LastName,
                    Email = participantF.Email,
                    PhoneNumber = participantF.PhoneNumber,
                    PositionName = position.PositionName,
                    InterviewScheduledOn = interview.InterviewDate,
                    AttendedOn = participant.AttendedTime,
                    InterviewScheduledFor = candidate.FirstName + " " + candidate.LastName,
                    InterviewId = interview.InterviewId
               });
            }

            return Ok(participantsModel);
        }

        [HttpPost("participant_update")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> UpdateParticipantDetailsAsync([FromBody] UpdateParticipantViewModel updateViewModel)
        {
            var participant = await _userRepository.GetUserById(updateViewModel.ParticipantId);
            if (participant == null) return BadRequest();

            var oldInterview = await _interviewRepository.GetInterviewByIdAsync(updateViewModel.OldInterviewId);
            if (oldInterview == null) return BadRequest();

            var newInterview = await _interviewRepository.GetInterviewByIdAsync(updateViewModel.InterviewId);
            if (newInterview == null) return BadRequest();

            var interviewParticipation = await _interviewRepository.GetInterviewParticipantByIdsAsync(oldInterview.InterviewId, participant.Id);
            if (interviewParticipation == null) return BadRequest();

            _interviewRepository.removeParticipantFromInterview(interviewParticipation);
            await _interviewRepository.SaveChangesAsync();

            InterviewParticipant newParticipation = new InterviewParticipant()
            {
                InterviewId = newInterview.InterviewId,
                EmployeeId = participant.Id,
                AttendedTime = DateTime.UtcNow
            };

            await _interviewRepository.AddInterviewParticipantAsync(newParticipation);
            await _interviewRepository.SaveChangesAsync();

            return Ok("The participant details was succesfully updated");
        }

        [HttpDelete("remove_participation/{id}/{id2}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveParticipationAsync(int id, string id2)
        {
            if (!ModelState.IsValid) return BadRequest();

            var participant = await _userRepository.GetUserById(id2);
            if (participant == null) return BadRequest();

            var interview = await _interviewRepository.GetInterviewByIdAsync(id);
            if (interview == null) return BadRequest();

            var interviewParticipation = await _interviewRepository.GetInterviewParticipantByIdsAsync(interview.InterviewId, participant.Id);
            if (interviewParticipation == null) return NotFound();

            _interviewRepository.removeParticipantFromInterview(interviewParticipation);
            await _interviewRepository.SaveChangesAsync();

            _mailServices.SendParticipationRemovalResponse(participant, "Removal");

            return Ok("The participation was succesfully removed from database");
        }

        [HttpGet("users")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetAllAvailableUsersAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            var users = await _userManager.Users.Where(u => u.Role != "Administrator").ToListAsync();

            return Ok(users);
        }

        [HttpGet("chart/employees")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> GetEmployeesChartDataAsync()
        {
            if (!ModelState.IsValid) return BadRequest();

            var departments = await _departmentRepository.GetDepartmentsAsync();
            if (departments == null) return BadRequest();

            var departmentsOrderedByName = departments.OrderBy(d => d.DepartmentName);

            Dictionary<string, int> departmentsData = new Dictionary<string, int>();

            int positionsCount = 0;
            foreach(var department in departmentsOrderedByName)
            {
                positionsCount = _positionRepository.GetPositionsByDepartmentId(department.DepartmentId).Result.Count();
                departmentsData.Add(department.DepartmentName, positionsCount);
            }

            List<EmployeesChartViewModel> employeesChart = new List<EmployeesChartViewModel>();

            foreach (KeyValuePair<string, int> keyValue in departmentsData)
            {
                employeesChart.Add(new EmployeesChartViewModel
                {
                    Department = keyValue.Key,
                    NoOfEmployees = keyValue.Value
                });
            }

            return Ok(employeesChart);
        }

        private void SetBrowsableAttributeValue(string attribute, object value)
    {
        var applicationType = typeof(UserRegisterViewModel);
        var descriptor = TypeDescriptor.GetProperties(applicationType)[attribute];

        var attrib = (BrowsableAttribute)descriptor.Attributes[typeof(BrowsableAttribute)];
        var isBrow = attrib.GetType().GetField("Browsable", BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Instance);
        var attr = attrib.GetType();
        isBrow.SetValue(attrib, value);
    }


    public static bool IsAlphaNumeric(string keyToCheck)
    {
        return keyToCheck.All(char.IsLetterOrDigit);
    }
}
}
