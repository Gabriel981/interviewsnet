﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class AppRoleViewModel
    {
        [Required]
        [MinLength(3, ErrorMessage = "The role name cannot be less than 3 characters")]
        [MaxLength(20, ErrorMessage = "The role name cannot be more than 20 characters")]
        public string RoleName { get; set; }
    }
}
