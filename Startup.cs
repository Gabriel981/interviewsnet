using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Data.Repository.Account;
using InterviewsManagementNET.Data.Repository.Administrative;
using InterviewsManagementNET.Data.Repository.Administrative.Implementations;
using InterviewsManagementNET.Data.Repository.Administrative.Interfaces;
using InterviewsManagementNET.Helpers;
using InterviewsManagementNET.Jobs;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using InterviewsManagementNET.Services.Implementations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace InterviewsManagementNET
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jwtSecuritySettings = Configuration.GetSection("JwtSecuritySettings");

            services.AddHttpContextAccessor();

            services.AddDbContext<SubmissionContext>(options => options.UseSqlServer(Configuration.GetConnectionString("InterviewsConnection")));
/*            services.AddDbContext<AuditableIdentityContext>(options => options.UseSqlServer(Configuration.GetConnectionString("InterviewsConnection")));
*/            /*services.AddDbContext<SubmissionContext>(options => options.UseSqlServer(Configuration.GetConnectionString("InterviewsConnection")));*/

            services.AddControllers().AddNewtonsoftJson(c =>
            {
                c.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            /*
                        services.AddIdentity<Employee, IdentityRole>()
                           .AddEntityFrameworkStores<SubmissionContext>()
                           .AddDefaultTokenProviders();*/

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<IUserRepository, UserRepositoryImpl>();
            services.AddScoped<IPositionRepo, PositionRepositoryImpl>();
            services.AddScoped<ISubmissionRepo, SubmissionRepoImpl>();
            services.AddScoped<IHiringRepo, HiringRepositoryImpl>();
            services.AddScoped<IInterviewRepo, InterviewRepositoryImpl>();
            services.AddScoped<IApplicationRepository, ApplicationRepositoryImpl>();
            services.AddScoped<IMailServices, MailServicesImpl>();
            services.AddScoped<INotificationService, NotificationServiceImpl>();
            services.AddScoped<ITaskRepository, TaskImpl>();
            services.AddScoped<IStateSeqRepository, StateSeqImpl>();
            services.AddScoped<IQuestionRepository, QuestionRepositoryImpl>();
            services.AddScoped<IQuizRepository, QuizRepositoryImpl>();
            services.AddScoped<IAnswerRepository, AnswerRepositoryImpl>();
            services.AddScoped<ICandidateRepository, CandidateRepositoryImpl>();
            services.AddScoped<IDepartmentRepository, DepartmentRepositoryImpl>();
            services.AddScoped<IRejectAllocationRepository, RejectAllocationImpl>();
            services.AddScoped<IAuditTrailRepository, AuditTrailImpl>();
            services.AddScoped<IAccountRepository, AccountRepositoryImpl>();
            services.AddScoped<ITokenService, TokenService>();


            services.Configure<JwtSecuritySettings>(jwtSecuritySettings);

            /*IdentityBuilder builder = services.AddIdentityCore<Employee>(opt =>
            {
                opt.Password.RequireDigit = true;
                opt.Password.RequiredLength = 8;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = true;
                opt.Password.RequireLowercase = true;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);

            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>(); */

            /*
                        services.AddDefaultIdentity<User>()
                            .AddEntityFrameworkStores<SubmissionContext>();*/

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<SubmissionContext>()
                .AddDefaultTokenProviders();


            /*            services.AddDefaultIdentity<Candidate>().AddEntityFrameworkStores<SubmissionContext>();
            */
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });

            services.AddMvc().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddCors(o => o.AddPolicy("CorsPolicy", ctr =>
            {
                var allowedOrigins = new List<string>();
                allowedOrigins.Add("http://localhost:4200");
                allowedOrigins.Add("http://localhost:4200");

                ctr.WithOrigins(allowedOrigins.ToArray())
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                .WithExposedHeaders("Token-Expired");
            }));

            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JWT-Token:SecretKey").Value));

                options.IncludeErrorDetails = true; //for debugging purposes

                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSecuritySettings.Get<JwtSecuritySettings>().SecretKey)),
                    ValidAudience = jwtSecuritySettings.Get<JwtSecuritySettings>().Audience,
                    ValidIssuer = jwtSecuritySettings.Get<JwtSecuritySettings>().Issuer,
                    RequireSignedTokens = true,
                    RequireExpirationTime = true,
                    ValidateLifetime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = System.TimeSpan.Zero
                };

                options.Events = new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }

                        return System.Threading.Tasks.Task.CompletedTask;
                    }
                };
            });

            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

            services.AddSingleton<EmailSenderJob>();
            services.AddSingleton(new EmailJobSchedule(
                jobType: typeof(EmailSenderJob),
                cronExpression: "0 0 0/1 1/1 * ? *"
            ));

            services.AddHostedService<QuartzHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            CreateRoles(serviceProvider).Wait();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
         
            using(var scope = serviceProvider.CreateScope())
            {
                var posRepository = scope.ServiceProvider.GetRequiredService<IPositionRepo>();
                var depRepository = scope.ServiceProvider.GetRequiredService<IDepartmentRepository>();

                string[] roles = { "Administrator", "Manager departament resurse umane", "Registered candidate", "Anonymous"};

                IdentityResult roleResult;

                foreach (var role in roles)
                {
                    var roleExists = await roleManager.RoleExistsAsync(role);
                    if (!roleExists)
                    {
                        if(depRepository.GetDepartment("Administrative").Result == null)
                        {
                            depRepository.AddDepartment(new Department
                            {
                                DepartmentName = "Administrative",
                                Active = false,
                                CurrentNumberOfPositions = 0
                            });

                            await depRepository.SaveAllChangesAsync();

                            await posRepository.AddPositionAsync(new Position
                            {
                                PositionName = "Administrator",
                                DepartmentId = depRepository.GetDepartment("Administrative").Result.DepartmentId,
                                AvailableForRecruting = false,
                                Description = "",
                                NoVacantPositions = 0
                            });

                            await posRepository.SaveChangesAsync();
                        }

                        if(depRepository.GetDepartment("Resurse umane").Result == null)
                        {
                            depRepository.AddDepartment(new Department
                            {
                                DepartmentName = "Resurse umane",
                                Active = true,
                                CurrentNumberOfPositions = 0
                            });

                            await depRepository.SaveAllChangesAsync();

                            await posRepository.AddPositionAsync(new Position
                            {
                                PositionName = "Manager departament resurse umane",
                                DepartmentId = depRepository.GetDepartment("Resurse umane").Result.DepartmentId,
                                AvailableForRecruting = false,
                                Description = "",
                                NoVacantPositions = 0
                            });

                            await posRepository.SaveChangesAsync();
                        }


                        if(await posRepository.GetPositionByNameAsync("Registered candidate") == null)
                        {
                            await posRepository.AddPositionAsync(new Position
                            {
                                PositionName = "Registered candidate",
                                DepartmentId = null,
                                AvailableForRecruting = false,
                                NoVacantPositions = 0,
                                Description = ""
                            });

                            await posRepository.SaveChangesAsync();
                        }

                        if(await posRepository.GetPositionByNameAsync("Not assigned") == null)
                        {
                            await posRepository.AddPositionAsync(new Position
                            {
                                PositionName = "Not assigned",
                                DepartmentId = null,
                                AvailableForRecruting = false,
                                NoVacantPositions = 0,
                                Description = ""
                            });

                            await posRepository.SaveChangesAsync();
                        }

                        roleResult = await roleManager.CreateAsync(new IdentityRole(role));
                    }
                }

                var superUser = new User
                {
                    UserName = Configuration["AppConfigurationUser:Username"],
                    Email = Configuration["AppConfigurationUser:Email"],
                    FirstName = Configuration["AppConfigurationUser:FirstName"],
                    LastName = Configuration["AppConfigurationUser:LastName"],
                    DateOfBirth = DateTime.UtcNow,
                    PositionId = posRepository.GetPositionByNameAsync("Administrator").Result.PositionId,
                    Address = Configuration["AppConfigurationUser:Address"],
                    SecurityQuestion = Configuration["AppConfigurationUser:SecurityQuestion"],
                    SecurityQuestionAnswer = Configuration["AppConfigurationUser:SecurityQuestionAnswer"]
                };

                string userPWD = Configuration["AppConfigurationUser:Password"];
                var user = await userManager.FindByEmailAsync(Configuration["AppConfigurationUser:Email"]);

                if (user == null)
                {
                    var superUserCreate = await userManager.CreateAsync(superUser, userPWD);
                    if (superUserCreate.Succeeded)
                    {
                        await userManager.AddToRoleAsync(superUser, "Administrator");
                    }
                }
            }
        }
    }
}
