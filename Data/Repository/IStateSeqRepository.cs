﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository
{
    public interface IStateSeqRepository
    {
        Task<IEnumerable<State>> GetAllFieldsStateAsync();
        Task<State> GetFieldStateAsync(int stateId);
        Task<IEnumerable<Sequence>> GetAllFieldsSequencesAsync();
        Task<Sequence> GetFieldSequenceAsync(int sequenceId);
        Task AddFieldStateAsync(State state);
        void RemoveFieldState(State state);
        Task AddFieldsSequenceAsync(Sequence sequence);
        void RemoveFieldsSequence(Sequence sequence);
        Task<bool> SaveChangesAsync();

    }
}
