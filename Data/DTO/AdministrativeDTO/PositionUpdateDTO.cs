﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class PositionUpdateDTO
    {
        [Required]
        [MaxLength(60, ErrorMessage = "The specified position name can't be more than 60 characters")]
        [MinLength(4)]
        public string PositionName { get; set; }

        [Required]
        public int DepartmentId { get; set; }

        [Required]
        public int NoVacantPositions { get; set; }

        public bool AvailableForRecruting { get; set; }


    }
}
