﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class InterviewRequestViewModel
    {
        public string RequesterId;
        public string Reason;
        public string ReceiverId;
        public int InterviewId;
    }
}
