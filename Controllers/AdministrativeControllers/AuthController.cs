﻿using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using InterviewsManagementNET.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PasswordGenerator;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.IdentityModel;
using System.Threading.Tasks;
using System.Linq;
using InterviewsManagementNET.Data.ViewModel;
using Google.Authenticator;
using InterviewsManagementNET.Data.ViewModel.Account;

namespace InterviewsManagementNET.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly IUserRepository _userRepository;
        private readonly SignInManager<User> _singInManager;
        private readonly IConfiguration configuration;
        private readonly IPositionRepo _position_repository;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ITaskRepository _taskRepository;
        private readonly ISubmissionRepo _submissionRepository;
        private readonly IHiringRepo _hiringRepository;
        private readonly IMailServices _mailService;
        private readonly ITokenService _tokenService;

        public AuthController(
            UserManager<User> userManager,
            IUserRepository repository,
            SignInManager<User> sigInManager,
            IConfiguration configuration,
            IPositionRepo pos_repository,
            RoleManager<IdentityRole> roleManager,
            ITaskRepository taskRepository,
            ISubmissionRepo submissionRepository,
            IHiringRepo hiringRepository,
            ITokenService tokenService,
            IMailServices mailServices)
        {
            this._userManager = userManager;
            this._userRepository = repository;
            this._singInManager = sigInManager;
            this.configuration = configuration;
            this._position_repository = pos_repository;
            this._roleManager = roleManager;
            this._taskRepository = taskRepository;
            this._submissionRepository = submissionRepository;
            this._hiringRepository = hiringRepository;
            this._mailService = mailServices;
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("user/login")]
        public async Task<IActionResult> LoginAsUser([FromBody] UserLoginViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            User user = await _userRepository.GetUserByEmailAddress(model.EmailAddress);
            if (user == null)
                throw new ArgumentNullException(nameof(user));


            try
            {
                var result = await _singInManager.CheckPasswordSignInAsync(user, model.Password, false);

                if(result.Succeeded)
                {
                    if (await _userManager.GetTwoFactorEnabledAsync(user))
                    {
                        if (user.EnabledTwoFactorGoogle == true)
                            return GenerateCodeGoogleAuth(user);

                        return await GenerateOTPFor2StepVerification(user);
                    }

                    IList<string> userRoles = await _userManager.GetRolesAsync(user);

                    List<Claim> claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(ClaimTypes.Name, user.Email),
                        new Claim(ClaimTypes.Role, userRoles.First())
                    };

                    string tokenStr = _tokenService.GenerateAccToken(claims);
                    string refreshToken = _tokenService.GenerateRefreshToken();

                    user.JwtToken = tokenStr;
                    user.RefreshToken = refreshToken;
                   
                    await _userRepository.SaveChangesAsync();

                    AuthResponseDto userViewModel = new AuthResponseDto()
                    {
                        AppUser = user,
                        Role = userRoles.FirstOrDefault(),
                        refreshToken = refreshToken
                    };

                    return Ok(userViewModel);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return BadRequest();

            /*if (model == null)
            {
                return BadRequest("Invalid user request to login!");
            }

            User EmplFound = _userRepository.GetUserByEmailAddress(model.EmailAddress);

            if (EmplFound == null)
            {
                return NotFound();
            }
            else
            {

                var result = await _singInManager.CheckPasswordSignInAsync(EmplFound, model.Password, lockoutOnFailure: true);

                if (result.Succeeded)
                {
                    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                    var signininCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, model.EmailAddress),
                        new Claim(ClaimTypes.Role, EmplFound.Role)
                    };

                    var tokenOptions = new JwtSecurityToken
                    (
                       issuer: "http://localhost:5001",
                       audience: "http://localhost:5001",
                       claims: claims,
                       expires: DateTime.UtcNow.AddDays(2),
                       signingCredentials: signininCredentials
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                    return Ok(new { Token = tokenString });
                }
            }

            return Unauthorized();*/
        }

        public async Task<ActionResult> GenerateOTPFor2StepVerification(User user)
        {
            var providers = await _userManager.GetValidTwoFactorProvidersAsync(user);
            if (!providers.Contains("Email")) return BadRequest("Invalid two-step verification profider");

            var token = await _userManager.GenerateTwoFactorTokenAsync(user, "Email");
            _mailService.SendTwoFactorToken(user, token);

            return Ok(new AuthResponseDto { Is2StepVerificationRequired = true, Provider = "Email", Method = "Email"});
        }

        public ActionResult GenerateCodeGoogleAuth(User user)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            var setupInfo = tfa.GenerateSetupCode("iManagement", user.Email, configuration.GetSection("JWT-Token:SecretKey").Value, false, 300);

            string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
            string manualEntrySetupCode = setupInfo.ManualEntryKey;

            return Ok(new GoogleAuthViewModel { ImageUrl = qrCodeImageUrl, ManualEntrySetupCode = manualEntrySetupCode, Method = "Google", Is2StepVerificationRequired = true });
        }

        [HttpPost("twostepverification")]
        public async Task<ActionResult> TwoStepVerificationAsync([FromBody] TwoFactorDto twoFactorDto)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = await _userManager.FindByEmailAsync(twoFactorDto.Email);
            if (user == null) return BadRequest("Invalid request");

            var validVerification = await _userManager.VerifyTwoFactorTokenAsync(user, twoFactorDto.Provider, twoFactorDto.Token);
            if (!validVerification) return BadRequest("Invalid token verification");

            IList<string> userRoles = await _userManager.GetRolesAsync(user);

            List<Claim> claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(ClaimTypes.Name, user.Email),
                        new Claim(ClaimTypes.Role, userRoles.First())
                    };

            string tokenStr = _tokenService.GenerateAccToken(claims);
            string refreshToken = _tokenService.GenerateRefreshToken();

            user.JwtToken = tokenStr;
            user.RefreshToken = refreshToken;

            await _userRepository.SaveChangesAsync();

            AuthResponseDto userViewModel = new AuthResponseDto()
            {
                AppUser = user,
                Role = userRoles.FirstOrDefault(),
                refreshToken = refreshToken
            };

            return Ok(userViewModel);
        }

        [HttpPost("googleverification")]
        [AllowAnonymous]
        public async Task<ActionResult> VerifyGoogleCodeAsync([FromBody] GoogleValidationViewModel googleValidation)
        {
            if (!ModelState.IsValid) return BadRequest();

            TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
            bool isCorrect = twoFactorAuthenticator.ValidateTwoFactorPIN(configuration.GetSection("JWT-Token:SecretKey").Value, googleValidation.UserKey);
            if (isCorrect == false) return BadRequest();

            var user = await _userManager.FindByEmailAsync(googleValidation.Email);
            if (user == null) return BadRequest("Invalid request");

            IList<string> userRoles = await _userManager.GetRolesAsync(user);

            List<Claim> claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(ClaimTypes.Name, user.Email),
                        new Claim(ClaimTypes.Role, userRoles.First())
                    };

            string tokenStr = _tokenService.GenerateAccToken(claims);
            string refreshToken = _tokenService.GenerateRefreshToken();

            user.JwtToken = tokenStr;
            user.RefreshToken = refreshToken;

            await _userRepository.SaveChangesAsync();

            AuthResponseDto userViewModel = new AuthResponseDto()
            {
                AppUser = user,
                Role = userRoles.FirstOrDefault(),
                refreshToken = refreshToken
            };

            return Ok(userViewModel);
        }
     

        [HttpPost]
        [Route("user/register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var notAssignedPosition = await _position_repository.GetPositionByNameAsync("Not assigned");

                var newEmployee = new User
                {
                    UserName = model.EmailAddress,
                    Email = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PositionId = notAssignedPosition.PositionId,
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber,
                    SecurityQuestion = model.SecurityQuestion,
                    SecurityQuestionAnswer = model.SecurityQuestionAnswer
                };

                if (notAssignedPosition != null)
                {
                    newEmployee.Position = notAssignedPosition;
                    newEmployee.Role = notAssignedPosition.PositionName;
                }

                var resultPost = await _userManager.CreateAsync(newEmployee, model.Password);
                var user = await _userRepository.GetUserByName(newEmployee.UserName);

                bool roleExists = await _roleManager.RoleExistsAsync("Not retrieved");

                if (!roleExists)
                {
                    var role = new IdentityRole();
                    role.Name = "Not retrieved";
                    await _roleManager.CreateAsync(role);
                }

                if (resultPost.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Not retrieved");
                    return Ok("Registered successfully");
                }
            }

            return Ok();
        }

        [HttpPost]
        [Route("user/register/candidates")]
        public async Task<IActionResult> RegisterCandidate([FromBody] UserRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newCandidate = new User
                {
                    UserName = model.EmailAddress,
                    Email = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PositionId = model.PositionId,
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber,
                    SecurityQuestion = model.SecurityQuestion,
                    SecurityQuestionAnswer = model.SecurityQuestionAnswer
                };

                Position notRetrievedPosition = await _position_repository.GetPositionByNameAsync("Registered candidate");
                if (notRetrievedPosition != null)
                {
                    newCandidate.Position = notRetrievedPosition;
                    newCandidate.Role = notRetrievedPosition.PositionName;
                }

                var resultPost = (IdentityResult)null;
                var generatedPassword = "";

                if (model.Password == null || model.Password == "")
                {
                    /*Guid g = Guid.NewGuid();
                    string GuidString = Convert.ToBase64String(g.ToByteArray());
                    GuidString = GuidString.Replace("=", "");
                    GuidString = GuidString.Replace("+", "");*/

                    var pwd = new Password().IncludeLowercase().IncludeUppercase().IncludeSpecial().IncludeNumeric().LengthRequired(16);
                    generatedPassword = pwd.Next();

                    resultPost = await _userManager.CreateAsync(newCandidate, generatedPassword);
                }
                else
                {
                    resultPost = await _userManager.CreateAsync(newCandidate, model.Password);
                }

                var user = await _userRepository.GetUserByName(newCandidate.UserName);

                bool roleExists = await _roleManager.RoleExistsAsync("Candidate registered");

                if (!roleExists)
                {
                    var role = new IdentityRole();
                    role.Name = "Candidate registered";
                    await _roleManager.CreateAsync(role);
                }

                if (resultPost.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Candidate registered");
                    this._mailService.SendCandidatePassword(user, generatedPassword);
                    return Ok(newCandidate);
                }
            }

            return NoContent();
        }

        [HttpGet("get_email/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult> GetEmail(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return BadRequest();

            return Ok(user.Email);
        }

        public string GeneratePassword(bool useLowerCase, bool useUpperCase, bool useNumbers, bool useSpecial, int passwordSize)
        {
            const string LOWER_CASE = "abcdefghijklmnopqursuvwxyz";
            const string UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NUMBERS = "123456789";
            const string SPECIALS = @"!@£$%^&*()#€";

            char[] password = new char[passwordSize];
            string charSet = "";
            System.Random random = new Random();
            int counter;

            if (useLowerCase)
                charSet += LOWER_CASE;

            if (useUpperCase)
                charSet += UPPER_CASE;

            if (useNumbers)
                charSet += NUMBERS;

            if (useSpecial)
                charSet += SPECIALS;

            for (counter = 0; counter < passwordSize; counter++)
            {
                password[counter] = charSet[random.Next(charSet.Length - 1)];
            }

            return String.Join(null, password);
        }

       /* [HttpGet]
        [Route("currentUser")]
        public async Task<ActionResult> GetCurrentUser()
        {
            if (HttpContext.User.Identity.Name == null)
            {
                var roleExists = await _roleManager.RoleExistsAsync("Anonymous");
                
                if(!roleExists)
                {
                    await _roleManager.CreateAsync(new IdentityRole("Anonymous"));
                }


                User userAnonymous = _userRepository.GetUserByName("Anonymous");
                return userAnonymous;
            }

            User user = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            return user;
        }*/

        [HttpPost("reset/password")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter, Employee, Candidate registered")]
        public async Task<IActionResult> ResetPassword(PasswordResetDTO passwordResetDTO)
        {
            var userFound = await _userRepository.GetUserByEmailAddress(passwordResetDTO.Email);
            if (userFound == null) return NotFound("No user found in the database with email address " + passwordResetDTO.Email);

            string tokenCode = await _userManager.GeneratePasswordResetTokenAsync(userFound);

            if (passwordResetDTO.NewPassword != passwordResetDTO.ConfirmPassword)
                return BadRequest("The password provided was not the same with the one in Confirm password input");

            var result = await _userManager.ResetPasswordAsync(userFound, tokenCode, passwordResetDTO.NewPassword);

            if (result.Succeeded)
                return Ok("The password was successfully changed");

            return BadRequest("Invalid details");
        }

        [HttpPost("scheduleDelete")]
        [Authorize(Roles = "Candidate registered")]
        public async Task<IActionResult> RemoveAccountScheduleAsync(RemoveAccountScheduleDTO removeAccountScheduleDTO)
        {
            var userFound = await _userRepository.GetUserById(removeAccountScheduleDTO.UserId);
            if (userFound == null) return NotFound("No user found in the database with id " + removeAccountScheduleDTO.UserId);

            var submissionFound = await _submissionRepository.GetSubmissionByCandidateIdAsync(userFound.Id);
            if (submissionFound == null) return NotFound("No submission found in the database for candidate with id " + userFound.Id);

            var hiringProcessFound = await _hiringRepository.GetHiringProcessBySubmissionId(submissionFound.SubmissionId);
            if (hiringProcessFound == null) return NotFound("No hiring process found in the database for submission with id " + submissionFound.SubmissionId);

            if (hiringProcessFound.InterviewId == null)
            {
                var reason = new Tasks();

                if (removeAccountScheduleDTO.Reason == "Delete")
                {
                    reason = Tasks.Delete;
                }

                var currentTimeStamp = DateTime.UtcNow.AddHours(2).ToString("dd/MM/yyyy HH:mm");
                var scheduledOperation = DateTime.UtcNow.AddHours(2);
                var scheduledOperationHour = scheduledOperation.Hour;
                var scheduledOperationMinutes = scheduledOperation.Minute;

                var hourDifference = 0;
                var minutesDifference = 0;

                if (scheduledOperationHour < 24)
                {
                    hourDifference += 24 - scheduledOperationHour;
                }

                if (scheduledOperationMinutes < 60)
                {
                    minutesDifference += 60 - scheduledOperationMinutes;
                }

                var scheduledOperationFinalDate = scheduledOperation.AddDays(3).AddHours(hourDifference).AddMinutes(minutesDifference).ToString("dd/MM/yyyy HH:mm");

                AutoTask task = new AutoTask()
                {
                    Task = reason,
                    UserId = removeAccountScheduleDTO.UserId,
                    Timestamp = currentTimeStamp,
                    ScheduledOperation = scheduledOperationFinalDate
                };

                await _taskRepository.AddTaskAsync(task);
                await _taskRepository.SaveChangesAsync();

                return Ok("The removal request was successfully registered into server. The account will be removed in 3 days!");
            }

            return BadRequest("An interview exists in the database or invalid data!");
        }

        [HttpPost("user/refresh")]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenModel refreshTokenModel)
        {
            ClaimsPrincipal claimsPrincipal = _tokenService.GetPrincipalExpiredToken(refreshTokenModel.JwtToken);
            string username = claimsPrincipal.Identity.Name;

            var user = await _userRepository.GetUserByName(username);
            if (user == null)
                return NotFound();

            string newJwtToken = _tokenService.GenerateAccToken(claimsPrincipal.Claims);
            string newRefreshToken = _tokenService.GenerateRefreshToken();
            user.RefreshToken = newRefreshToken;
            await _userRepository.SaveChangesAsync();

            user.JwtToken = newJwtToken;
            user.RefreshToken = newRefreshToken;

            IList<string> roles = await _userManager.GetRolesAsync(user);

            UserViewModelUpdated userViewModel = new UserViewModelUpdated()
            {
                AppUser = user,
                Role = roles.First(),
                refreshToken = newRefreshToken
            };

            return Ok(userViewModel);
        }

        
    }
}
