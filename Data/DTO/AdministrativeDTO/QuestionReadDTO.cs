﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class QuestionReadDTO
    {
        public int QuestionId { get; set; }
        public string QuestionBody { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedAt { get; set; }
        public string Position { get; set; }
        public int NoOfAnswers { get; set; }
        public string LastModifiedBy { get; set; }
    }
}
