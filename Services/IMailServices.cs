﻿using InterviewsManagementNET.Data.ViewModel.Administrative;
using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Services
{
    public interface IMailServices
    {
        public void SendEmailSubmission(Submission submission);

        public void SendEmailInterview(HiringProcess hiringProcess);

        public void SendCandidateRemovalProposal(User candidate, User manager, User admin, string reason);

        public bool SendCandidateRemovalSuccess(User candidate, User manager, User admin, string reason);

        public bool SendCandidateApproval(User candidate, User manager, string positionName);

        public bool SendCandidateRejection(User candidate, User manager, string positionName);

        public bool SendParticipantAttendanceInterview(User sender, User receiver, Interview interview, User candidate);

        public string SendCandidateDeleteConfirmation(User receiver);

        public string SendCandidatePassword(User receiver, string generatedPassword);
        public string SendPasswordRequest(MessageViewModel message);
        public string SendEmailFromAdmin(User receiver, User admin, string reason, string additionalInformation);
        public string SendParticipationRemovalResponse(User receiver, string reason);
        public bool SendTwoFactorToken(User receiver, string token);
    }
}
