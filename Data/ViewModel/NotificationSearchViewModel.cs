﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class NotificationSearchViewModel
    {
        public string SenderId { get; set; }
        public int PositionId { get; set; }
    }
}
