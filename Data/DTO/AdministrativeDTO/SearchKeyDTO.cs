﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class SearchKeyDTO
    {
        public string key { get; set; }
        public string startDate { get; set; }
        public string lastDate { get; set; }
    }
}
