﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int CurrentNumberOfPositions { get; set; }

        public ICollection<Position> Positions { get; set; }
    }
}
