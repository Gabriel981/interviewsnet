﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class AnswerReadDTO
    {
        public int AnswerId { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "The answer body length cannot be less than 6 characters")]
        [MaxLength(30, ErrorMessage = "The answer body length cannot be more than 30 characters")]
        public string AnswerBody { get; set; }
        public int QuestionId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedAt { get; set; }
    }
}
