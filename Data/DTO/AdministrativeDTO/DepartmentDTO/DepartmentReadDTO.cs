﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO.DepartmentDTO
{
    public class DepartmentReadDTO
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int CurrentNumberOfPositions { get; set; }
    }
}
