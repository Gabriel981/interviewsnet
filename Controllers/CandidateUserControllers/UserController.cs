﻿using AutoMapper;
using Firebase.Storage;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.ClientDTO;
using InterviewsManagementNET.Data.ViewModel;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.CandidateUserControllers
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {

        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private static string Bucket = "interviewsnetproject.appspot.com";

        public UserController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpPut]
        [Route("update/{id}")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Employee, Registered candidate, Recruiter")]
        public async Task<ActionResult<UserViewModelCandidate>> UpdateCandidateDetailsAsync(string id, [FromBody] UserViewModelCandidate userViewModel)
        {
            if (userViewModel == null)
            {
                throw new ArgumentNullException(nameof(userViewModel));
            }

            User userFound = await _userRepository.GetUserById(id);
            if (userFound == null)
            {
                return NotFound();
            }

            userFound.FirstName = userViewModel.FirstName;
            userFound.LastName = userViewModel.LastName;
            userFound.Address = userViewModel.Address;
            userFound.Email = userViewModel.Email;
            userFound.DateOfBirth = userViewModel.DateOfBirth;

            userFound.UserName = userViewModel.Email;

            this._userRepository.UpdateUserDetails(userFound);
            await _userRepository.SaveChangesAsync();

            UserViewModelCandidate returnedUser = _mapper.Map<UserViewModelCandidate>(userFound);

            return Ok(returnedUser);
        }


        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> UploadProfilePhoto([FromForm] FileUploadViewModel file)
        {
            var userFound = await _userRepository.GetUserByEmailAddress(file.Email);

            if (userFound == null) 
                return NotFound("No user found in the database with email address: " + file.Email);

            var uploadedImage = file.File;

            var uploadedImageUrl = "";

            if (uploadedImage.Length > 0)
            {
                using (var memoryStream = new MemoryStream())
                {
                    uploadedImage.CopyTo(memoryStream);

                    var cancellationToken = new CancellationTokenSource();

                    var uploadTask = new FirebaseStorage(Bucket)
                        .Child("Profile")
                        .Child(userFound.Id)
                        .Child("Images")
                        .Child($"{uploadedImage.FileName}")
                        .PutAsync(uploadedImage.OpenReadStream(), cancellationToken.Token);

                    try
                    {
                        uploadedImageUrl = await uploadTask;
                        userFound.ProfilePhotoURL = uploadedImageUrl;
                        this._userRepository.UpdateUserDetails(userFound);
                        await _userRepository.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception was thrown: {0}", ex);
                    }
                }
            }

            return Ok();
        }

        [HttpPost]
        [Route("notifications/{id}/{value}")]
        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter, Employee, Candidate registered")]
        public async Task<IActionResult> ChangeNotificationStatusAsync(string id, bool value)
        {
            if (value == null) return BadRequest("No value was provided");

            var userFound = await _userRepository.GetUserById(id);
            if (userFound == null) return NotFound("No user was found in the database with id " + id);

            if (userFound.EnableNotifications == value)
            {
                return BadRequest("No changes can be made when the value is the same with the one in the database");
            }
            else
            {
                userFound.EnableNotifications = value;
                _userRepository.UpdateUserDetails(userFound);
                await _userRepository.SaveChangesAsync();
            }

            return Ok();
        }
    }
}
