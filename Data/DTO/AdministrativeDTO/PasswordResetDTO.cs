﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class PasswordResetDTO
    {
        [Required]
        public string Token { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 5)]
        public string NewPassword { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 5)]
        public string ConfirmPassword { get; set; }
    }
}
