﻿namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class AnswerWriteDTO
    {
        public string AnswerBody { get; set; }
        public int QuestionId { get; set; }
        public string CreatedById { get; set; }
        public string CreatedAt { get; set; }
    }
}
