﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Roles
{
    public class AssignRoleViewModel
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}
