﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Account
{
    public class GoogleAuthViewModel
    {
        public string Method { get; set; }
        public bool Is2StepVerificationRequired { get; set; }
        public string ImageUrl { get; set; }
        public string ManualEntrySetupCode { get; set; }
    }
}
