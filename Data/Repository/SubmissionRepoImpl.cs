﻿using InterviewsManagementNET.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class SubmissionRepoImpl : ISubmissionRepo
    {
        private readonly SubmissionContext _context;

        public SubmissionRepoImpl(SubmissionContext context)
        {
            _context = context;
        }

        public async Task AddSubmissionAsync(Submission submission)
        {
            await _context.Submissions.AddAsync(submission);
        }

        public async Task<IEnumerable<SubmissionViewModel>> GetAllSubmissionsAsync()
        {
            var submissionsList = await _context.Submissions.Include(s => s.HiringProcess).ToListAsync();

            var submissionViewModels = submissionsList.Select(s => new SubmissionViewModel
            {
                SubmissionId = s.SubmissionId,
                DateOfSubmission = s.DateOfSubmission,
                Mentions = s.Mentions,
                CandidateId = s.CandidateId,
                User = _context.Users.FirstOrDefault(c => c.Id == s.CandidateId),
                PositionId = s.PositionId,
                AppliedPosition = _context.Positions.FirstOrDefault(p => p.PositionId == s.PositionId),
                Status = s.HiringProcess.CandidateSituation.ToString(),
                CV = s.CV
            });

            return submissionViewModels;
            /*return await _context.Submissions.Include(s => s.HiringProcess).ToList().Select(s => new SubmissionViewModel
            {
                SubmissionId = s.SubmissionId,
                DateOfSubmission = s.DateOfSubmission,
                Mentions = s.Mentions,
                CandidateId = s.CandidateId,
                User = _context.Users.FirstOrDefault(c => c.Id == s.CandidateId),
                PositionId = s.PositionId,
                AppliedPosition = _context.Positions.FirstOrDefault(p => p.PositionId == s.PositionId),
                Status = s.HiringProcess.CandidateSituation.ToString()
            });*/
        }

        public async Task<SubmissionViewModel> GetSubmissionByIdDetailedAsync(int Id)
        {
            return await _context.Submissions.Include(u => u.User)
               .Include(p => p.AppliedPosition).Select(s => new SubmissionViewModel
               {
                   SubmissionId = s.SubmissionId,
                   DateOfSubmission = s.DateOfSubmission,
                   Mentions = s.Mentions,
                   AppliedPosition = s.AppliedPosition,
                   CV = s.CV,
                   User = s.User,
                   Status = _context.Hirings.FirstOrDefault(h => h.SubmissionId == s.SubmissionId).CandidateSituation.ToString()
               }).FirstOrDefaultAsync();
        }

        public async Task<Submission> GetSubmissionByIdAsync(int id)
        {
            return await _context.Submissions.FirstOrDefaultAsync(su => su.SubmissionId == id);
        }

        public void RemoveSubmission(Submission submission)
        {
            _context.Submissions.Remove(submission);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void SubmissionUpdate(Submission submission)
        {
            //not returning anything
        }

        public async Task<IEnumerable<User>> GetAllCandidatesAsync()
        {
            var submissions = await _context.Submissions.ToListAsync();

            if (submissions == null)
            {
                throw new ArgumentNullException(nameof(submissions));
            }

            var retrievedUsers = new List<User>();

            foreach (var submission in submissions)
            {
                retrievedUsers.Add(submission.User);
            }

            if (retrievedUsers == null)
            {
                throw new ArgumentNullException(nameof(retrievedUsers));
            }

            return retrievedUsers;
        }

        public async Task<IEnumerable<SubmissionViewModel>> GetSubmissionsDetailsAsync()
        {
            return await _context.Submissions.Include(s => s.HiringProcess).Include(s => s.AppliedPosition)
                    .Include(s => s.User).Select(s => new SubmissionViewModel
                    {
                        User = _context.Users.FirstOrDefault(c => c.Id == s.CandidateId),
                        AppliedPosition = s.AppliedPosition,
                        Status = s.HiringProcess.CandidateSituation.ToString(),
                        DateOfSubmission = s.DateOfSubmission,
                        Mentions = s.Mentions,
                        SubmissionId = s.SubmissionId,
                        CandidateId = s.CandidateId,
                        PositionId = s.PositionId
                    }).ToListAsync();
        }

        public async Task<IEnumerable<Submission>> GetAllSubmissionsOfficialAsync()
        {
            return await _context.Submissions.ToListAsync();
        }

        public async Task<Submission> GetSubmissionByCandidateIdAsync(string id)
        {
            return await _context.Submissions.FirstOrDefaultAsync(s => s.CandidateId == id);
        }

        public async Task<Submission> GetLastSubmission()
        {
            var lastSubmissionId = await _context.Submissions.MaxAsync(s => s.SubmissionId);
            return await _context.Submissions.FirstOrDefaultAsync(s => s.SubmissionId == lastSubmissionId);
        }
    }
}
