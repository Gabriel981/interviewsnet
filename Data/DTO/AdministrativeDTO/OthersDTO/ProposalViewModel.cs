﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO.OthersDTO
{
    public class ProposalViewModel
    {
        public string EmailSender { get; set; }
        public string EmailCandidate { get; set; }
        public string Reason { get; set; }
    }
}
