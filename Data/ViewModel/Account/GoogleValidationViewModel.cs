﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Account
{
    public class GoogleValidationViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserKey { get; set; }
    }
}
