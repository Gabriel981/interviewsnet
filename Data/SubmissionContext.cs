﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class SubmissionContext : IdentityDbContext<User>
    {
        /*        public DbSet<Candidate> Candidates { get; set; } //done*/
        public DbSet<Position> Positions { get; set; } //done
        public DbSet<Interview> Interviews { get; set; } //done
        public DbSet<Quiz> Quizzes { get; set; } //done 
        public DbSet<HiringProcess> Hirings { get; set; } //done
        /*        public DbSet<Employee> Employees { get; set; } //done*/
        public DbSet<Submission> Submissions { get; set; } //done
        public DbSet<InterviewParticipant> InterviewParticipant { get; set; }
        public DbSet<AutoTask> Tasks { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Sequence> Sequences { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<RejectedAllocation> RejectedAllocation { get; set; }
        public DbSet<Audit> AuditLogs { get; set; }
        public DbSet<QuizQuestion> QuestionQuizzes { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        public SubmissionContext(DbContextOptions<SubmissionContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //User mapping
            modelBuilder.Entity<User>()
                .HasKey(u => u.Id)
                .HasName("user_id");

            modelBuilder.Entity<User>()
                .HasOne(u => u.Position)
                .WithMany(p => p.Users)
                .HasForeignKey(u => u.PositionId)
                .OnDelete(DeleteBehavior.Cascade);

            //Hiring process mapping
            modelBuilder.Entity<HiringProcess>()
                .HasKey(hp => hp.HiringId)
                .HasName("hiring_id");

            //Interview mapping
            modelBuilder.Entity<Interview>()
                .HasKey(i => i.InterviewId)
                .HasName("interview_id");

            modelBuilder.Entity<Interview>()
                .HasOne(q => q.Quiz)
                .WithMany(i => i.Interviews)
                .HasForeignKey(i => i.QuizId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Interview>()
                .HasOne(i => i.HiringProcess)
                .WithOne(h => h.Interview)
                .HasForeignKey<Interview>(i => i.HiringId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("fk_interview_hiring");

            //InterviewParticipant mapping
            modelBuilder.Entity<InterviewParticipant>()
                .HasKey(key => new { key.InterviewId, key.EmployeeId });

            modelBuilder.Entity<InterviewParticipant>()
                .HasOne(ip => ip.Interview)
                .WithMany(i => i.InterviewParticipants)
                .HasForeignKey(ip => ip.InterviewId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<InterviewParticipant>()
                .HasOne(ip => ip.User)
                .WithMany(p => p.InterviewParticipants)
                .HasForeignKey(ip => ip.EmployeeId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<QuizQuestion>()
                .HasKey(key => new { key.QuizId, key.QuestionId, key.AllocatedAt });

            modelBuilder.Entity<QuizQuestion>()
                .HasOne(qq => qq.Quiz)
                .WithMany(q => q.QuizQuestions)
                .HasForeignKey(qq => qq.QuizId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<QuizQuestion>()
                .HasOne(qq => qq.Question)
                .WithMany(q => q.QuizQuestions)
                .HasForeignKey(qq => qq.QuestionId)
                .OnDelete(DeleteBehavior.Restrict);

            //Position mapping
            modelBuilder.Entity<Position>()
                .HasKey(po => po.PositionId)
                .HasName("position_name");

            modelBuilder.Entity<Position>()
                .HasOne(p => p.Department)
                .WithMany(d => d.Positions)
                .HasForeignKey(p => p.DepartmentId)
                .OnDelete(DeleteBehavior.Restrict);

            //Quiz mapping
            modelBuilder.Entity<Quiz>()
                .HasKey(q => q.QuizId)
                .HasName("quiz_id");

            modelBuilder.Entity<Quiz>()
                .HasOne(q => q.Position)
                .WithMany(po => po.Quizzes)
                .HasForeignKey(q => q.PositionId);

            modelBuilder.Entity<Quiz>()
                .HasOne(q => q.CreatedBy)
                .WithMany(u => u.CreatedQuizes)
                .HasForeignKey(q => q.CreatedById);

            //Submission mapping
            modelBuilder.Entity<Submission>()
                .HasOne(s => s.AppliedPosition)
                .WithMany(po => po.Submissions)
                .HasForeignKey(s => s.PositionId)
                .HasPrincipalKey(po => po.PositionId);

            modelBuilder.Entity<Submission>()
                .HasOne(s => s.HiringProcess)
                .WithOne(hp => hp.Submission)
                .HasForeignKey<Submission>(s => s.HiringId)
                .HasPrincipalKey<HiringProcess>(hp => hp.HiringId);

            modelBuilder.Entity<Submission>()
                .HasKey(key => new { key.DateOfSubmission, key.PositionId, key.CandidateId });

            modelBuilder.Entity<Submission>()
                .Property(s => s.SubmissionId)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<AutoTask>()
                .HasKey(t => t.Id)
                .HasName("task_id");

            modelBuilder.Entity<State>()
                .HasKey(s => s.Id)
                .HasName("state_id");

            modelBuilder.Entity<Sequence>()
                .HasKey(se => se.Id)
                .HasName("sequence_id");

            modelBuilder.Entity<Sequence>()
                .HasOne(se => se.State)
                .WithMany(s => s.Sequences)
                .HasForeignKey(se => se.StateId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Question>()
                .HasKey(q => q.QuestionId)
                .HasName("question_id");

            modelBuilder.Entity<Answer>()
                .HasKey(a => a.AnswerId)
                .HasName("answer_id");

            modelBuilder.Entity<Answer>()
                .HasOne(a => a.Question)
                .WithMany(q => q.Answers)
                .HasForeignKey(a => a.QuestionId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Department>()
                .HasKey(d => d.DepartmentId)
                .HasName("department_id");

            modelBuilder.Entity<RejectedAllocation>()
                .HasKey(ra => ra.RejectionId)
                .HasName("RejectionId");
        }

        public virtual async Task<int> SaveChangesAsync(string userId = null)
        {
            OnBeforeSaveChanges(userId);
            var result = await base.SaveChangesAsync();
            this.DetachAllEntities();
            return result;
        }
        private void OnBeforeSaveChanges(string userId)
        {
            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditEntry>();
            var entries = ChangeTracker.Entries();
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is Audit || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                    continue;
                var auditEntry = new AuditEntry(entry);
                auditEntry.TableName = entry.Entity.GetType().Name;
                auditEntry.UserId = userId;
                auditEntries.Add(auditEntry);
                foreach (var property in entry.Properties)
                {
                    string propertyName = property.Metadata.Name;
                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[propertyName] = property.CurrentValue;
                        continue;
                    }
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            auditEntry.AuditType = AuditType.Create;
                            auditEntry.NewValues[propertyName] = property.CurrentValue;
                            break;
                        case EntityState.Deleted:
                            auditEntry.AuditType = AuditType.Delete;
                            auditEntry.OldValues[propertyName] = property.OriginalValue;
                            break;
                        case EntityState.Modified:
                            if (property.IsModified)
                            {
                                auditEntry.ChangedColumns.Add(propertyName);
                                auditEntry.AuditType = AuditType.Update;
                                auditEntry.OldValues[propertyName] = property.OriginalValue;
                                auditEntry.NewValues[propertyName] = property.CurrentValue;
                            }
                            break;
                    }
                }
            }
            foreach (var auditEntry in auditEntries)
            {
                AuditLogs.Add(auditEntry.ToAudit());
            }
        }

        public void DetachAllEntities()
        {
            var changedEntries = this.ChangeTracker.Entries()
              .Where(e => e.State == EntityState.Added ||
                     e.State == EntityState.Modified ||
                     e.State == EntityState.Deleted)
              .ToList();

            foreach(var entry in changedEntries)
            {
                entry.State = EntityState.Detached;
            }
        }

    }
}
