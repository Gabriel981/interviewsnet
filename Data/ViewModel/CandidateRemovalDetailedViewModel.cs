﻿using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class CandidateRemovalDetailedViewModel
    {
        public User Admin;
        public User Candidate;
        public string Message;
        public string Timestamp;
    }
}
