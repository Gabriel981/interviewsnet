﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Account
{
    public class AccountViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public bool IsTwoFactorEnabled { get; set; }
        [Required]
        public bool IsNotificationEnabled { get; set; }
        [Required]
        public bool IsTwoFactorGoogleEnabled { get; set; }

    }
}
