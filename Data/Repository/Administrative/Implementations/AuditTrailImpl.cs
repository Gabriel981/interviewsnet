﻿using InterviewsManagementNET.Data.Repository.Administrative.Interfaces;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.Repository.Administrative.Implementations
{
    public class AuditTrailImpl : IAuditTrailRepository
    {
        private readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuditTrailImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<Audit>> GetAllAuditTrailsAsync()
        {
            return await _context.AuditLogs.ToListAsync();
        }

        public async Task<IEnumerable<Audit>> GetAllAuditTrailsByUserIdAsync(string userId)
        {
            return await _context.AuditLogs.Where(at => at.UserId == userId).ToListAsync();
        }

        public async Task<Audit> GetAuditTrailByIdAsync(int id)
        {
            return await _context.AuditLogs.FirstOrDefaultAsync(at => at.Id == id);
        }

        public void RemoveAuditTrail(Audit audit)
        {
            _context.AuditLogs.Remove(audit);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }
    }
}
