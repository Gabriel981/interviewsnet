﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace InterviewsManagementNET.Data.Repository
{
    public class StateSeqImpl : IStateSeqRepository
    {
        private readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public StateSeqImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async System.Threading.Tasks.Task AddFieldsSequenceAsync(Sequence sequence)
        {
            await _context.Sequences.AddAsync(sequence);
        }

        public async System.Threading.Tasks.Task AddFieldStateAsync(State state)
        {
            await _context.States.AddAsync(state);
        }

        public async System.Threading.Tasks.Task<IEnumerable<Sequence>> GetAllFieldsSequencesAsync()
        {
            return await _context.Sequences.ToListAsync();
        }

        public async System.Threading.Tasks.Task<IEnumerable<State>> GetAllFieldsStateAsync()
        {
            return await _context.States.ToListAsync();
        }

        public async System.Threading.Tasks.Task<Sequence> GetFieldSequenceAsync(int sequenceId)
        {
            return await _context.Sequences.FirstOrDefaultAsync(seq => seq.Id == sequenceId);
        }

        public async System.Threading.Tasks.Task<State> GetFieldStateAsync(int stateId)
        {
            return await _context.States.FirstOrDefaultAsync(s => s.Id == stateId);
        }

        public void RemoveFieldsSequence(Sequence sequence)
        {
            _context.Sequences.Remove(sequence);
        }

        public void RemoveFieldState(State state)
        {
            _context.States.Remove(state);
        }

        public async System.Threading.Tasks.Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }
    }
}
