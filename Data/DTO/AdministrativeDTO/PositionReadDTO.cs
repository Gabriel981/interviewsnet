﻿using InterviewsManagementNET.Models;

namespace InterviewsManagementNET.Data
{
    public class PositionReadDTO
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public Department Department { get; set; }
        public int NoVacantPositions { get; set; }
        public bool AvailableForRecruting { get; set; }
    }
}
