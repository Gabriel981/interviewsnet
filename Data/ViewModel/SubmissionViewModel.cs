﻿using InterviewsManagementNET.Models;
using System;

namespace InterviewsManagementNET.Data
{
    public class SubmissionViewModel
    {
        public int SubmissionId { get; set; }
        public DateTime DateOfSubmission { get; set; }
        public string Mentions { get; set; }
        public string CandidateId { get; set; }
        public User User { get; set; }
        public int PositionId { get; set; }
        public Position AppliedPosition { get; set; }
        public string Status { get; set; }
        public string CV { get; set; }
    }
}
