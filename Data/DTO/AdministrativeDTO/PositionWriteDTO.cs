﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class PositionWriteDTO
    {
        [Required]
        [MinLength(2, ErrorMessage = "Position name cannot be less than 2 characters")]
        [MaxLength(50, ErrorMessage = "Position name cannot be more than 30 characters")]
        public string PositionName { get; set; }
        
        [Required]
        public int DepartmentId { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Description cannot be less than 8 characters")]
        [MaxLength(350, ErrorMessage = "Description cannot be more than 350 characters")]
        public string Description { get; set; }

        [Required]
        public int NoVacantPositions { get; set; }
    }
}
