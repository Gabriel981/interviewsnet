﻿using System.Collections.Generic;

namespace InterviewsManagementNET.Models
{
    public class Position
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        public string Description { get; set; }
        public int NoVacantPositions { get; set; }
        public bool AvailableForRecruting { get; set; }

        public ICollection<Submission> Submissions { get; set; }

        public ICollection<User> Users { get; set; }
        /*        public ICollection<Employee> Employees { get; set; }*/
        public ICollection<Quiz> Quizzes { get; set; }
    }
}
