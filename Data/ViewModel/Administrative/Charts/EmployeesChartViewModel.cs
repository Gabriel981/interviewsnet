﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative.Charts
{
    public class EmployeesChartViewModel
    {
        public string Department { get; set; }
        public int NoOfEmployees { get; set; }
    }
}
