﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data
{
    public class UserRegisterViewModel
    {
        [EmailAddress]
        [MinLength(6)]
        public string EmailAddress { get; set; }

        public bool EmailAddressAvailable { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool PasswordAvailable { get; set; }

        /*        [MinLength(5, ErrorMessage = "The employee first name cannot be less than 5 characters")]
                [MaxLength(40, ErrorMessage = "The employee first name cannot be more than 40 characters")]*/
        public string FirstName { get; set; }

        public bool FirstNameAvailable { get; set; }
        /*
                [MinLength(5, ErrorMessage = "The employee last name cannot be less than 5 characters")]
                [MaxLength(40, ErrorMessage = "The employee last name cannot be more than 40 characters")]*/
        public string LastName { get; set; }

        public bool LastNameAvailable { get; set; }

        [MinLength(8, ErrorMessage = "The address details cannot be less than 8 characters")]
        [MaxLength(50, ErrorMessage = "The address details cannot be more than 50 characters")]
        public string Address { get; set; }

        public bool AddressAvailable { get; set; }


        public string PhoneNumber { get; set; }

        public bool PhoneNumberAvailable { get; set; }

        public int PositionId { get; set; }

        public Position Position { get; set; }

        public bool PositionAvailable { get; set; }

        public string SecurityQuestion { get; set; }

        public bool SecurityQuestionAvailable { get; set; }

        public string SecurityQuestionAnswer { get; set; }

        public bool SecurityQuestionAnswerAvailable { get; set; }

        public IFormFile File { get; set; }

        public bool FileAvailable { get; set; }

        public string Role { get; set; }

    }
}
