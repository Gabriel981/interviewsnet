﻿namespace InterviewsManagementNET.Models
{
    public class AutoTask
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public Tasks Task { get; set; }
        public string Timestamp { get; set; }
        public string ScheduledOperation { get; set; }
    }

    public enum Tasks : int
    {
        Update = 1,
        Delete = 2
    }
}
