﻿using System;

namespace InterviewsManagementNET.Data
{
    public class SubmissionReadDTO
    {
        public int SubmissionId { get; set; }
        public DateTime DateOfSubmission { get; set; }
        public string Mentions { get; set; }
        public int PositionId { get; set; }
        public int CandidateId { get; set; }
    }
}
