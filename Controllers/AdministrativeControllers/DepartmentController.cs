﻿using AutoMapper;
using InterviewsManagementNET.Data;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO;
using InterviewsManagementNET.Data.DTO.AdministrativeDTO.DepartmentDTO;
using InterviewsManagementNET.Data.Repository;
using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Controllers.AdministrativeControllers
{
    [Route("api/departments")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentRepository _repository;
        private readonly IPositionRepo _positionRepository;
        private readonly IMapper _mapper;

        private const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


        public DepartmentController(IDepartmentRepository repository, IPositionRepo positionRepository, IMapper mapper)
        {
            _repository = repository;
            _positionRepository = positionRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult> GetDepartmentsAsync()
        {
            var departmentsList = await _repository.GetDepartmentsAsync();
            if (departmentsList == null) 
                return NotFound("No departments was found in the database");

            return Ok(_mapper.Map<IEnumerable<DepartmentReadDTO>>(departmentsList.Distinct()));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> GetDepartmentByIdAsync(int id)
        {
            var department = await _repository.GetDepartmentById(id);
            if (department == null)
                return NotFound("No department was found in the database");

            return Ok(_mapper.Map<DepartmentReadDTO>(department));
        }


        [HttpGet]
        [Route("{name}")]
        public async Task<ActionResult> GetDepartmentByName(string name)
        {
            var department = await _repository.GetDepartment(name);
            if (department == null)
                return NotFound("No department was found in the database");

            return Ok(_mapper.Map<DepartmentReadDTO>(department));
        }

        [HttpPost]
        [Route("add")]
        public async Task<ActionResult> AddDepartmentAsync([FromBody] DepartmentWriteDTO departmentWrite)
        {
            Department department = _mapper.Map<Department>(departmentWrite);
            if (department == null)
                throw new ArgumentNullException("Cannot convert department");

            _repository.AddDepartment(department);
            await _repository.SaveAllChangesAsync();

            return Ok("The department was succesfully inserted in the database");
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult> UpdateDepartmentAsync(int id, [FromBody] DepartmentUpdateDTO departmentUpdate)
        {
            Department department = await _repository.GetDepartmentById(id);
            if (department == null)
                return NotFound("No department was found in the database");

            department.DepartmentName = departmentUpdate.DepartmentName;
            department.Description = departmentUpdate.Description;
            department.Active = departmentUpdate.Active;

            _repository.UpdateDepartment(department);
            await _repository.SaveAllChangesAsync();

            return Ok("The department details was succesfully updated");
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult> RemoveDepartmentAsync(int id)
        {
            Department department = await _repository.GetDepartmentById(id);
            if (department == null)
                return NotFound("No department was found in the database");

            if (department.DepartmentName == "Administrative") return BadRequest("Cannot remove Administrative department");

            var positionsFound = await _positionRepository.GetAllPositionsAsync();
            if (positionsFound == null) return NotFound();

            foreach(var position in positionsFound)
            {
                if(position.DepartmentId == department.DepartmentId)
                {
                    _positionRepository.RemovePosition(position);
                    await _positionRepository.SaveChangesAsync();
                }
            }

            _repository.RemoveDepartment(department);
            await _repository.SaveAllChangesAsync();
            return Ok("The department was succesfully removed from the database");
        }

        [Authorize(Roles = "Administrator, Manager departament resurse umane, Recruiter")]
        [HttpGet("export")]
        public async Task<ActionResult> ExportDepartmentData()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var departmentsAvailable = await _repository.GetDepartmentsAsync();
            
            if (departmentsAvailable == null) return NotFound();

            MemoryStream memoryStream = new MemoryStream();
            using(ExcelPackage excelPackage = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Available departments");
                int index = 1;

                worksheet.Cells["B1"].Value = "Department id";
                worksheet.Cells["C1"].Value = "Department name";
                worksheet.Cells["D1"].Value = "Is active?";
                worksheet.Cells["E1"].Value = "Current no. of positions";

                worksheet.Cells["B1:E1"].Style.Font.Bold = true;
                worksheet.Cells["B1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                worksheet.Cells["B1:E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                worksheet.Cells["B1:E1"].Style.Font.Color.SetColor(System.Drawing.Color.White);

                foreach(var department in departmentsAvailable)
                {
                    worksheet.Cells["B" + (index + 1)].Value = department.DepartmentId;
                    worksheet.Cells["C" + (index + 1)].Value = department.DepartmentName;
                    worksheet.Cells["D" + (index + 1)].Value = department.Active;
                    worksheet.Cells["E" + (index + 1)].Value = department.CurrentNumberOfPositions;

                    index++;
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                await excelPackage.SaveAsync();
            }

            memoryStream.Position = 0;
            return File(memoryStream, contentType, "AvailableDepartments.xlsx");
        }
    }
}
