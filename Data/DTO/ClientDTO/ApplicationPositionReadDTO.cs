﻿namespace InterviewsManagementNET.Data.DTO.ClientDTO
{
    public class ApplicationPositionReadDTO
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public string Department { get; set; }
        public string Description { get; set; }
        public int NoVacantPositions { get; set; }
        public bool AvailableForRecruting { get; set; }
    }
}
