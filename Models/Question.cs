﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Models
{
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }
        public string QuestionBody { get; set; }
        public ICollection<QuizQuestion> QuizQuestions { get; set; }
        public string CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public string CreatedAt { get; set; }
        public int PositionId { get; set; }
        public Position Position { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public string LastModifiedById { get; set; }
        public User LastModifiedBy { get; set; }
        public string LastModifiedAt { get; set; }
    }
}
