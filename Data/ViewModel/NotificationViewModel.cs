﻿namespace InterviewsManagementNET.Data.ViewModel
{
    public class NotificationViewModel
    {
        public string FirstNameReceiver { get; set; }
        public string LastNameReceiver { get; set; }
        public string EmailReceiver { get; set; }
        public string FirstNameSender { get; set; }
        public string LastNameSender { get; set; }
        public string EmailSender { get; set; }
        public string Timestamp { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string PositionName { get; set; }
        public string NotificationId { get; set; }

    }
}
