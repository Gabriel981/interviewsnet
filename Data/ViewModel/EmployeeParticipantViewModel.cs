﻿using System;

namespace InterviewsManagementNET.Data.ViewModel
{
    public class EmployeeParticipantViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int PositionId { get; set; }
        public int AttendedInterviewId { get; set; }
    }
}
