﻿using System.ComponentModel.DataAnnotations;

namespace InterviewsManagementNET.Data.DTO.AdministrativeDTO
{
    public class QuizWriteDTO
    {
        public string Mentions { get; set; }

        [Required]
        public int InterviewId { get; set; }
        public int PositionId { get; set; }
        [Required]
        public string CreatedById { get; set; }
    }
}
