﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class UpdateParticipantViewModel
    {
        [Required]
        public string ParticipantId { get; set; }
        [Required]
        public int InterviewId { get; set; }
        [Required]
        public int OldInterviewId { get; set; }
    }
}
