﻿using InterviewsManagementNET.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public interface ICandidateRepository
    {
        Task<IEnumerable<User>> GetAllCandidatesAsync();
        Task<User> GetCandidateByIdAsync(string Id);
        Task<User> GetCandidateByEmailAddressAsync(string EmailAddress);
    }
}
