﻿using InterviewsManagementNET.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data
{
    public class HiringRepositoryImpl : IHiringRepo
    {
        private readonly SubmissionContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HiringRepositoryImpl(SubmissionContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<HiringViewModel>> GetAllHiringProcesses()
        {

            return await _context.Hirings.Include(s => s.Submission).Select(h => new HiringViewModel
            {
                HiringId = h.HiringId,
                CandidateSituation = h.CandidateSituation.ToString(),
                SubmissionId = h.SubmissionId,
                /*                Submission = _context.Submissions.Include(s => s.User)
                                    .Include(s => s.AppliedPosition)
                                    .Where(s => s.SubmissionId == h.SubmissionId)
                                    .FirstOrDefault(),*/
                Submission = h.Submission,
                Candidate = _context.Users.FirstOrDefault(c => c.Id == h.Submission.CandidateId),
                AppliedPosition = _context.Positions.FirstOrDefault(p => p.PositionId == h.Submission.PositionId),
                Interview = _context.Interviews.FirstOrDefault(i => i.HiringId == h.HiringId)
            }).ToListAsync();

            /*return await _context.Hirings.Include(s => s.Submission).ToList().Select(h => new HiringViewModel
            {
                HiringId = h.HiringId,
                CandidateSituation = h.CandidateSituation.ToString(),
                SubmissionId = h.SubmissionId,
                *//*                Submission = _context.Submissions.Include(s => s.User)
                                    .Include(s => s.AppliedPosition)
                                    .Where(s => s.SubmissionId == h.SubmissionId)
                                    .FirstOrDefault(),*//*
                Submission = h.Submission,
                Candidate = _context.Users.FirstOrDefault(c => c.Id == h.Submission.CandidateId),
                AppliedPosition = _context.Positions.FirstOrDefault(p => p.PositionId == h.Submission.PositionId),
                Interview = _context.Interviews.FirstOrDefault(i => i.HiringId == h.HiringId)
            });*/
        }

        public async Task<HiringViewModel> GetHiringProcessByIdDetailed(int id)
        {
            return await _context.Hirings.Select(h => new HiringViewModel
            {
                HiringId = h.HiringId,
                SubmissionId = h.Submission.SubmissionId,
                CandidateSituation = h.CandidateSituation.ToString(),
                Submission = h.Submission
            })
            .FirstOrDefaultAsync(h => h.HiringId == id);
        }

        public async Task<HiringProcess> GetHiringProcessById(int? id)
        {
            return await _context.Hirings.FirstOrDefaultAsync(h => h.HiringId == id);
        }

        public async Task<HiringProcess> GetHiringProcessBySubmissionId(int id)
        {
            return await _context.Hirings.FirstOrDefaultAsync(h => h.SubmissionId == id);
        }

        public async Task addHiringProcess(HiringProcess hiringProcess)
        {
            await _context.Hirings.AddAsync(hiringProcess);
        }

        public void UpdateHiringProcess(HiringProcess hiringProcess)
        {
            // is not necessary
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value) > 0;
        }

        public void RemoveHiringProcess(HiringProcess hiringProcess)
        {
            _context.Hirings.Remove(hiringProcess);
        }

        public async Task<HiringProcess> GetHiringProcessByInterviewId(int id)
        {
            return await _context.Hirings.FirstOrDefaultAsync(h => h.InterviewId == id);
        }

        public async Task<bool> SaveChangesRegisterAsync()
        {
            return await _context.SaveChangesAsync(null) > 0;
        }
    }
}
