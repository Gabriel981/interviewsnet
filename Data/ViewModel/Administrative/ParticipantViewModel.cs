﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative
{
    public class ParticipantViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PositionName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime InterviewScheduledOn { get; set; }
        public string InterviewScheduledFor { get; set; }
        public DateTime AttendedOn { get; set; }
        public int InterviewId { get; set; }
    }
}
