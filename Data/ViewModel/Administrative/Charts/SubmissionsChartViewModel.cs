﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewsManagementNET.Data.ViewModel.Administrative.Charts
{
    public class SubmissionsChartViewModel
    {
        public string Date { get; set; }
        public int NoOfSubmissions { get; set; }
    }
}
